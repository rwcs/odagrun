//
//  test_utils.c
//  odagrun
//
//  Created by Danny Goossen on 16/9/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

//LCOV_EXCL_START
#include <check.h>
//LCOV_EXCL_STOP

#include "../src/utils.h"
#include "../src/cJSON_deploy.h"
#include "../src/error.h"

START_TEST(check_dirops)
{
	vrmdir(NULL);
	vrmdir_s(NULL);
	vmkdir(NULL);
	xis_dir(NULL);
	ck_assert_int_ne(1,  v_exist_dir(NULL));
	vmkdir("./test/a/b");
	ck_assert_int_eq(1,  v_exist_dir("./test/a/b"));
	ck_assert_int_ne(1,  v_exist_dir("./test/a/b/%s","c"));
	char * fpath=vcmkdir("%s/%s","./testlink","al/bl");
	ck_assert_ptr_nonnull(fpath);
	ck_assert_str_eq("./testlink/al/bl", fpath);
	if (fpath) free(fpath);
	symlink("../testlink/al", "./test/al");
	ck_assert_int_eq(0,  xis_dir("./test/a/b"));
	ck_assert_int_eq(0,  xis_dir("./test/al"));
	vrmdir("./test");
	ck_assert_int_eq(-1,  xis_dir("./test/al"));
	ck_assert_int_eq(0,  xis_dir("./testlink/al/bl"));
	vmkdir("./test/");
	symlink("../testlink/al", "./test/al");
	vrmdir_s("./test");
	ck_assert_int_eq(0,  xis_dir("./test/al"));
	ck_assert_int_eq(-1,  xis_dir("./testlink/al/bl"));
	vrmdir("./testlink");
	ck_assert_int_eq(-1,  xis_dir("./testlink"));
	vrmdir("./test");
	char *dir2=vcmkdir("./test_dir/test/");
	ck_assert_str_eq(dir2, "./test_dir/test");
	vrmdir("./test_dir");
	if (dir2) free(dir2);
}
END_TEST

START_TEST(check_fileops)
{
	char * dir =vcmkdir("./%s","test_fileops");
	ck_assert_int_eq(v_exist_dir("./%s","test_fileops"),1);
	ck_assert_int_eq(xis_dir(dir),0);
	ck_assert_int_eq(xis_dir("test_fileopts/don'texist"),-1);
	ck_assert_ptr_nonnull(dir);
	ck_assert_int_eq(write_file_v("1234567890",10,"%s/%s",dir,"ten.txt"),0);
	ck_assert_int_eq(xis_dir("test_fileopts/ten.txt"),-1);
	char * r1=read_a_file("./test_fileops/ten.txt");
	char * r2=read_a_file_v("./test_fileops/%s","ten.txt");
	ck_assert_ptr_nonnull(r1);
	ck_assert_ptr_nonnull(r2);
	ck_assert_str_eq(r1, "1234567890");
	ck_assert_str_eq(r2, "1234567890");
	
	ck_assert_int_eq(cp_file("./test_fileops/ten.txt","./test_fileops/cpten.txt"),10);
	ck_assert_int_eq(cp_file("./test_fileops/ten.txt","./test_fileops3/cpten.txt"),-1);
	ck_assert_int_eq(cp_file("./test_fileops/ten.txt",NULL),-1);
	ck_assert_int_eq(cp_file("./test_fileops23/ten.txt","./test_fileops/cpten.txt"),-1);
	ck_assert_int_eq(cp_file(NULL,"./test_fileops/cpten.txt"),-1);
	
	ck_assert_int_eq(append_file_2_file("test_fileops/ten.txt","test_fileops/cpten.txt"),10);
	ck_assert_int_eq(append_file_2_file("test_fileops22/ten.txt","test_fileops/cpten.txt"),-1);
	ck_assert_int_eq(append_file_2_file(NULL,"test_fileops/cpten.txt"),-1);
	ck_assert_int_eq(append_file_2_file("test_fileops/ten.txt","test_fileops234/cpten.txt"),-1);
	ck_assert_int_eq(append_file_2_file("test_fileops/ten.txt",NULL),-1);
	ck_assert_int_eq(append_data_2_file("01234","test_fileops/cpten.txt"),5);
	ck_assert_int_eq(append_data_2_file("01234","test_fileops/aps5.txt"),5);
	ck_assert_int_eq(append_data_2_file("01234","test_fileops3/aps5.txt"),-1);
	char * r3=read_a_file("./test_fileops/cpten.txt");
	char * r4=read_a_file("./test_fileops/aps5.txt");
	ck_assert_ptr_nonnull(r3);
	ck_assert_ptr_nonnull(r4);
	ck_assert_str_eq(r3, "1234567890123456789001234");
	ck_assert_str_eq(r4, "01234");
	if (r1) free(r1);
	if (r2) free(r2);
	if (r3) free(r3);
	if (r4) free(r4);
	vrmdir("%s",dir);
	ck_assert_int_eq(v_exist_dir("./%s","test_fileops"),0);
	ck_assert_int_eq(write_file_v("1234567890",10,NULL),-1);
	r1=read_a_file(NULL);
	ck_assert_ptr_null(r1);
	r1=read_a_file_v(NULL);
	ck_assert_ptr_null(r1);
	r1=read_a_file("test_fileopts/don'texist");
	ck_assert_ptr_null(r1);
	r1=read_a_file_v("test_fileopts/dontexist");
	ck_assert_ptr_null(r1);
	ck_assert_int_eq(write_file_v("1234567890",10,"./nonexistdeirectory/test.txt"),-1);
	if (dir)free(dir);
	dir =vcmkdir(NULL);
	ck_assert_ptr_null(dir);
}
END_TEST

START_TEST(check_stringops)
{
	upper_string(NULL);
	lower_string(NULL);
	upper_string_n(NULL, 0);
	upper_string_n(NULL, 5);
	char test[5]="AaBb\0";
	upper_string(test);
	ck_assert_str_eq(test, "AABB\0");
	lower_string(test);
	ck_assert_str_eq(test, "aabb\0");
	upper_string_n(test, 2);
	ck_assert_str_eq(test, "AAbb\0");
	char test2[5]="\na\n\n\0";
	strip_nl(test2);
	ck_assert_str_eq(test2, "\na\0");
}
END_TEST

START_TEST(check_filelist)
{
	vmkdir("./test_filelist/d1/d11");
	vmkdir("./test_filelist/d2/d22/d222");
	vmkdir("./otest_filelist_out/d3");
	write_file_v("1234567890",10,"%s/%s","./test_filelist/d1/d11/","ten1.txt");
	write_file_v("1234567890",10,"%s/%s","./test_filelist/d1/d11/","ten2.txt");
	write_file_v("1234567890",10,"%s/%s","./test_filelist/d1","ten3.txt");
	write_file_v("1234567890",10,"%s/%s","./test_filelist/d1/","ten4.txt");
	write_file_v("1234567890",0,"%s/%s","./test_filelist/d1","ten5.txt");
	write_file_v("1234567890",10,"%s/%s","./test_filelist/d2/d22/","ten6.txt");
	
	write_file_v("1234567890",10,"%s/%s","./test_filelist_out/d3/","ten7.txt");
	char pwdb[PATH_MAX];
	char * pwd=getcwd(pwdb, PATH_MAX);
	char * projectdir=NULL;
	asprintf(&projectdir, "%s/test_filelist",pwd);
	cJSON*paths=cJSON_CreateArray();
	cJSON_AddItemToArray(paths, cJSON_CreateString("../otest_filelist_out/*"));
	cJSON_AddItemToArray(paths, cJSON_CreateString("../d1/*"));
	cJSON_AddItemToArray(paths, cJSON_CreateString("d1/*"));
	cJSON_AddItemToArray(paths, cJSON_CreateString("d1/;"));
	cJSON_AddItemToArray(paths, cJSON_CreateString("./d2/../d2"));
	cJSON * filelist=NULL;

	ck_assert_int_eq( getfilelist(projectdir,paths,&filelist),0);
	ck_assert_ptr_nonnull(filelist);
	ck_assert_int_eq(cJSON_GetArraySize(filelist), 10);
	if (filelist) cJSON_Delete(filelist);
	if (paths) cJSON_Delete(paths);
	if (projectdir) free(projectdir);
	vrmdir("./test_filelist");
	vrmdir("./otest_filelist_out");
}
END_TEST


struct test_data_optvalue_t
{
	char * optv;
	char * defaultv;
	char * result;
} ;
static const struct test_data_optvalue_t testlist_optvalue[4] = {
	{ NULL,NULL,NULL},
	{ NULL,"AB","AB"},
	{ "AB","CD","AB"},
	{ "EF",NULL,"EF"}
};


START_TEST(check_optvalue)
{
	char * test=get_opt_value(testlist_optvalue[_i].optv,testlist_optvalue[_i].defaultv,NULL);
	if (testlist_optvalue[_i].result)
	{ck_assert_ptr_nonnull(test);
	ck_assert_str_eq(test, testlist_optvalue[_i].result);
	}
	else ck_assert_ptr_null(test);
	if (test) free(test);
	test=NULL;
}
END_TEST

struct test_data_format_size_t
{
	uint64_t sz;
	char * format;
} ;
static const struct test_data_format_size_t testlist_format_size[10] = {
	{ 0,"0 B"},
	{ 1,"1 B"},
	{ 9999000,"10 MB"},
	{ 999900,"1.0 MB"},
	{ 25000,"25 kB"},
	{ 1024,"1.0 kB"},
	{ 8096,"8.1 kB"},
	{ 16192,"16 kB"},
	{ 100000000,"100 MB"},
	{1230000, "1.2 MB"}
};

START_TEST(check_format_size)
{
	char buff[FORMAT_SIZE_BUF];
   ck_assert_str_eq(testlist_format_size[_i].format, format_size(buff, testlist_format_size[_i].sz));
}
END_TEST

START_TEST(check_stderr_redirect)
{
	int my_pipe[2];
	my_pipe[0]=-1;
	my_pipe[1]=-1;
	char * redirect=NULL;
	int saved_stderr=0;
	saved_stderr=pipe_redirect_stderr(my_pipe);
	ck_assert_int_ne(0, saved_stderr);
	fprintf(stderr, "redirectedfromstderr\n");
	pipe_redirect_undo(my_pipe, saved_stderr, &redirect);
	ck_assert_ptr_nonnull(redirect);
	ck_assert_str_eq(redirect, "redirectedfromstderr\n");
	if(redirect) free(redirect);
	redirect=NULL;
// if we do 2 times, for sure we got the undo correct.
	saved_stderr=pipe_redirect_stderr(my_pipe);
	ck_assert_int_ne(0, saved_stderr);
	fprintf(stderr, "redirectedfromstderr\n");
	pipe_redirect_undo(my_pipe, saved_stderr, &redirect);
	ck_assert_ptr_nonnull(redirect);
	ck_assert_str_eq(redirect, "redirectedfromstderr\n");
	if(redirect) free(redirect);
	redirect=NULL;
}
END_TEST

START_TEST(check_split_path_file_2_path)
{
	struct test_data_t
	{
		char * in;
		char * out;
	} ;
	static const struct test_data_t testlist[6] = {
		{ "/dir/file","/dir/"},
		{ "dir/dir1/","dir/dir1/"},
		{ "/dir/dir1/","/dir/dir1/"},
		{ "dir/dir1/file","dir/dir1/"},
		{ "file",""},
		{ NULL, NULL},
	};
	char * result=NULL;
	
	split_path_file_2_path(&result,testlist[_i].in);
	if(testlist[_i].out)
	{
	ck_assert_ptr_nonnull(result);
	ck_assert_str_eq(result,testlist[_i].out );
	}
	else
		ck_assert_ptr_null(result);
}
END_TEST


char * tescs(const char *x,...)
{
	int i=0;
	char *a=CSPRINTF_L(x,&i);
	return(a);
}

START_TEST(check_CSPRINTh)
{
	char* r=tescs("t %s,%d","sd",5);
	printf("CSNPRINTF: %s\n",r);
	ck_assert_ptr_nonnull(r);
	ck_assert_str_eq(r,"t sd,5" );
}
END_TEST

/*

covered by other tests:
void split_path_file_2_path(char** p,const char *pf);
char * normalizePath(const char* pwd, const char * src, char* res);
void * memrchr(const void *s,int c, size_t n);

// no idea yet how to test yet:
int pipe_redirect_stderr(int my_pipe[2]);
void pipe_redirect_undo(int my_pipe[2],int saved_stderr,char**errormsg);
*/

Suite * utils_suite(void)
{
	Suite *s;
	TCase *tc_core;
	//TCase *tc_progress;
	s = suite_create("test_error");
	/* Core test case */
	tc_core = tcase_create("Core");
	//tcase_add_checked_fixture(tc_core, setup, teardown);
	
	tcase_add_test (tc_core, check_dirops);
	tcase_add_test (tc_core, check_stringops);
	tcase_add_test (tc_core,check_fileops);
	tcase_add_test (tc_core,check_filelist);
	tcase_add_loop_test (tc_core,check_format_size,0,10);
	tcase_add_loop_test (tc_core,check_optvalue,0,4);
	tcase_add_loop_test (tc_core,check_split_path_file_2_path,0,4);
	tcase_add_test (tc_core,check_stderr_redirect);
	tcase_add_test (tc_core,check_CSPRINTh);
	tcase_add_unchecked_fixture(tc_core, NULL, NULL);
	tcase_set_timeout(tc_core,15);
	suite_add_tcase(s, tc_core);
	return s;
}


int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;
	
	s = utils_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

