//
//  test_specials.c
//  odagrun
//
//  Created by Danny Goossen on 4/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

//LCOV_EXCL_START
#include <check.h>
//LCOV_EXCL_STOP

#include "../src/specials.h"
#include "../src/cJSON_deploy.h"

#define DOCKER_REGISTRY "registry.hub.docker.com"

//LCOV_EXCL_START
//Dummy for compling
size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...)
{
	return 0;
}
//LCOV_EXCL_STOP
// the command structure


static Suite * specials_suite(void);


Suite * specials_suite(void)
{
	Suite *s;
	TCase *tc_core;
	//TCase *tc_progress;
	s = suite_create("test_error");
	/* Core test case */
	tc_core = tcase_create("Core");
	//tcase_add_checked_fixture(tc_core, setup, teardown);
	
	tcase_add_unchecked_fixture(tc_core, NULL, NULL);
	tcase_set_timeout(tc_core,15);
	suite_add_tcase(s, tc_core);
	return s;
}


int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;
	
	s = specials_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
