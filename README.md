
# odagrun

[![](https://downloads.odagrun.com/latest/files/manual/logo.svg)](https://www.odagrun.com)

a custom gitlab-ci-multi-runner for building on openshift cluster with some bells and whistles.

## Purpose

With this project we hope to add easier automated creation and releasing of (docker-)images on with limited resources, while saving resourses with features like WORK_SPACES, GIT_CACHE and Depencies, all in a root-less environment without any priviledges.

## releases

[latest](https://downloads.odagrun.com/latest/) with the help of [deployctl](https://www.deployctl.com)

## manual

see [manual](http://downloads.odagrun.com/latest/files/manual/manual.html)

## Features of the odagrun:

1. can run and spawn multiple executors non privileged.

2. Parallel downloads of artifacts.

3. dynamic choice of dependencies with variable: `dependencies`

3. Use of the `CI_TOKEN` for pulling images from same `Gitlab server`

3. all CI functions integrated in the executable, Meaning the image does not need to contain linux-coreutils, shell nor git for an executer to run a Job!

4. integrated commands for registry operations/image creation (no docker-daemon is involved):
    - `registry_push`
        - create from rootfs an image and push to registry
        - create from an rootfs archive an image and push to registry
        - merge a rootfs FROM an existing image and a new image to the registry.
        - add a config file
        - all non root and no Docker Daemon is involved!
        - support for schemaVersion 1 and 2
    - `registry_tag_image` Remote tag an image reference

5. reduced disk usage to create images: `rootfs` and `archive` are streamed to registry with "HTTP PATCH", while calculating the content and blob SHA256SUM, and transfering a FROM image happens with up to 
6 parallel threads in chunks of 4MB, again no files are saved on the file system, allowing to create a base CentOS image on `openshift-online-starter`.

6. integrated ImageStream commands:
    - `ImageStream_delete_tag` delete a tag on the integrated `openshift` registry.
    - `ImageStream_delete_image` delete an image on the integrated `openshift` registry.

9. integrated `DockerHub` commands
    - `DockerHub_set_description` on DockerHub for a given image:
        - Short Description
        - Full Description
        - Is_Private: set repository private
    - `DockerHub_delete_tag`: delete an image tag
    - `DockerHub_delete_repository`: delete a DockerHub repository

10. integrated `QUAY` registry commands
    - `QUAY_set_description` on QUAY for a given image:
        - set image description
        - set image visibility
    - `QUAY_delete_tag`: delete an image tag
    - `QUAY_delete_repository`: delete an image

11. integrated command `PostHook` creates a POST request for a given url.

12. integrated `copy` command:
    - copy file to file/var/terminal
    - copy text to file/variable/terminal
    - option `--subtitute`: and substitute all `$xxx` or `${yyy}` environment variables
    - option `--append` : merge file/variables

13. integrated `MicroBadger_Update`: update MicroBadger


## install
 
see odagrun [User Guide install section](https://downloads.odagrun.com/latest/files/manual/manual.html#install)

## User Guide

see odagrun [User Guide](http://downloads.odagrun.com/latest/files/manual/manual.html#install)

## DockerHub

[DockerHub](https://microbadger.com/images/gioxa/odagrun)

## contributing

All kinds of contributions are welcome:
- submit an issue,
- merge requests,
- Buy me a coffee [![](https://www.buymeacoffee.com/assets/img/BMC-btn-logo.svg)](https://www.buymeacoffee.com/ryUMq1sSa) for some extra inspiration,
- some money to keep the cogs turning [![](https://img.shields.io/badge/donate-PayPal-blue.svg)](https://www.paypal.me/gioxa).

## Disclaimer

*The project odagrun, the Author and Gioxa Ltd. is in no way affiliated with Gitlab nor with Openshift, okd*

