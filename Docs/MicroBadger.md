
## MicroBadger API

Update [Micro Badger](https://microbadger.com/) with latest Tag on the [DockerHub](https://hub.docker.com/) for a given image.

```bash
MicroBadger_Update \
[--allow-fail] \
--image=repo-name
```

<br>
<div class="alert alert-warning"><strong>Alert! </strong>  <code> --image=repo-name</code> need to represent a valid docker image!</div>
<br>

*valid repo-names, e.g.:*

- `centos`
- `library/centos[:whatevertag]`
- `register.hub.docker.com/library/centos`

<br>
<div class="alert alert-info"><strong>Info:</strong> The image tag is stripped off and completly ignored.</div>
<br>


[top](#odagrun)

