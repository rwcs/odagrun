
## copy

<br>

### purpose


copy, substitute or append data from a file or text to a variable or to a file or to the terminal output.


```bash
copy \
  (--from_text="text"|--from_file=file) \
  (--to_var=varname|--to_file=filename|--to_term) \
  [--substitute] \
  [--append] \
  [--quiet] \
  [--if_value=${var}] \
  [--if_zero="${var}"]
```


<br>


<div class="alert alert-warning"><strong>Alert! </strong> when using <code>--from_text</code>, enclose option in double quotes <code>"</code>, especially when an variable consist out of multiple words.</div>

<br>

### Copy Options:



| Options | Description |
|------------:|----------------------------|
| [substitute](#substitute) | all variables in the text or file are resolved.|
| append | the `from_xxx` will be appended to the defined output, exept for the `--to_term` |
| quiet | no info will be outputed, and with `--to_term` only the data is displayed. |
| `if_value="${var}"` | command will only execute on the `var` not empty, simple substitution is supported.|
| `if_zero="${var}"` | command will only be executed if `var` is not defined or empty |


<br>

<div class="alert alert-warning"><strong>Alert! </strong> when using <code>--from_text</code>, enclose option in double quotes <code>"</code>, especially when an variable consist out of multiple words.</div>
<br>

### substitute


Using `--substitute` will substitude all `${var}` or `$var` with the environment variables from the source text.

If an variable is not defined, the original text `${var}` or `$var` will be used and thus not substituted, while if the `var` is empty, the result will be an empty string.

Simple Variable-Substitution is posible as per [wordexp()](https://www.gnu.org/software/libc/manual/html_node/Variable-Substitution.html#Variable-Substitution) from `glibc-2.17`, and escaping this can be done with a double `$` :

e.g.: `$${var}` will result in `${var}` without substitution of the variable.

<div class="alert alert-info"><strong>Good Practice:</strong><br> Always use <pre>${var}</pre> for substitution in stead of <pre>$var</pre>.</div>

<br>


[top](#odagrun)

