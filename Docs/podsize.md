# Features

## Pod-size

### Purpose:

Define the size of the buildcontainer for the job.

Setting Pod sizes smaller allows to run more parallel jobs, by setting a variable:


```yaml
variables:
  ODAGRUN_POD_SIZE: [nano|micro|small|medium|large|xlarge|xxlarge|xxxlarge]
```


### Sizes:


|type | memory | cpu |
|---:|:---:|:---:|
| nano | 128Mi | 250m |
| micro | 256Mi | 500 |
| small | 512Mi | 1 |
| medium | 768Mi | 1500m |
| large | 1Gi | 2 |
| xlarge | 2Gi | 4 |
| xxlarge | 4Gi | 8 |
| xxxlarge | 8Gi | 8 |


<br>


<br>
<div class="alert alert-info"><strong>Info:</strong> If <code>ODAGRUN_POD_SIZE</code> is not set, it silently defaults to <strong>micro</strong>.</div>



### Tip:

when building e.g. with automake and gcc, If you get a message like:

```bash
mv -f $depbase.Tpo $depbase.Po
gcc: internal compiler error: Killed (program cc1)
Please submit a full bug report,
with preprocessed source if appropriate.
See <http://bugzilla.redhat.com/bugzilla> for instructions.

```

One might want to increase the size of pod, or reduce the number of Parallel jobs as the process was probaly killed due to insufficiant memory.

Reduce number of parallel jobs with `make` e.g.:

```bash
make $(($(nproc)/3+1))
```

Secondly, a good practice with `make` would be:

```bash
make -j$(nproc)> /dev/null
```

Directing `stdout` to `/dev/null` for builds does reduce the uneeded output, thus increasing readability and preserving resources!

<br>
<div class="alert alert-info"><strong>Info:</strong> Define <code>ODAGRUN_POD_SIZE</code> as a global variable for all jobs the default value.</div>
<br>


[top](#odagrun)


