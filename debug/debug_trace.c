//
//  debug_trace.c
//  oc-runner
//
//  Created by Danny Goossen on 19/12/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "../src/deployd.h"

int main (void)
{
	void * trace=NULL;
	init_dynamic_trace(&trace, "", "", 0, NULL);
	Write_dyn_trace_pad(trace, blue,5, "1234567890");
	
	hex_dump_msg("test_buff", 0, "%s",get_dynamic_trace(trace));
	
	Write_dyn_trace(trace, none, "<<\n");
	hex_dump_msg("test_buff", 0, "%s",get_dynamic_trace(trace));
	
	printf("          1111111112222222222333333333344444444445555555555666666666677777777778\n");
	printf("12345678901234567890123456789012345678901234567890123456789012345678901234567890\n");
	printf("%s",get_dynamic_trace(trace));
}
