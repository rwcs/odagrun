//
//  debug_specials.c
//  odagrun
//
//  Created by Danny Goossen on 5/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include <stdio.h>
//
//  test_specials.c
//  odagrun
//
//  Created by Danny Goossen on 4/5/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "../src/specials.h"
#include "../src/cJSON_deploy.h"
#include "../src/error.h"
#include "../src/yaml2cjson.h"

size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...);
static int debug_process_nick(int _i);
#define DOCKER_REGISTRY "registry.hub.docker.com"




struct test_data_chop_ns_t
{
	char * name_space;
	char * name_ref;
} ;
static const struct test_data_chop_ns_t testlist_chop_ns[] = {
	{ "library/centos","centos"},
	{ "gioxa/odagrun/odagrun","odagrun"},
	{ "gioxa/odagrun/build-image","build-image-odagrun"},
	{ "centos/redis","redis-centos"},
	{ "dgoo2308/fpm-image","fpm-image-dgoo2308"},
	{ "deployctl/test_docker_config", "test-docker-config-deployctl"},
	{NULL}
	};


int get_size_chop_ns_data(void)
{
	int size;
	size=sizeof(testlist_chop_ns)/sizeof(struct test_data_chop_ns_t);
	return size-1;
}

int debug_chop_ns(int _i)
{
	
	char * name_ref=NULL;
	
	printf(" %d: %s =>",_i,testlist_chop_ns[_i].name_space);
	
	name_ref= name_space_rev_slug(testlist_chop_ns[_i].name_space);
	
	int res=0;
	if (name_ref && testlist_chop_ns[_i].name_ref && strcmp(name_ref,testlist_chop_ns[_i].name_ref)==0) ;
	else
		res=1;
	if (name_ref) free(name_ref);
	
	if (res) printf("FAIL\n");
	else printf("OK\n");
 return res;
}


struct test_data_chop_image_t
{
	char * image;
	char * registry;
	char * name_space;
	char * reference;
} ;
static const struct test_data_chop_image_t testlist_chop_image[] = {
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build","172.30.208.107:5000","odagrun/is-deployctl-test-oc-build",NULL},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:1","172.30.208.107:5000","odagrun/is-deployctl-test-oc-build","1"},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:1.1","172.30.208.107:5000","odagrun/is-deployctl-test-oc-build","1.1"},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:1:1","172.30.208.107:5000","odagrun/is-deployctl-test-oc-build","1:1"},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:1-1.2","172.30.208.107:5000","odagrun/is-deployctl-test-oc-build","1-1.2"},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107:5000","odagrun/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	
	{ "172.30.208.107/odagrun/is-deployctl-test-oc-build","172.30.208.107","odagrun/is-deployctl-test-oc-build",NULL},
	{ "172.30.208.107/odagrun/is-deployctl-test-oc-build:1","172.30.208.107","odagrun/is-deployctl-test-oc-build","1"},
	{ "172.30.208.107/odagrun/is-deployctl-test-oc-build:1.1","172.30.208.107","odagrun/is-deployctl-test-oc-build","1.1"},
	{ "172.30.208.107/odagrun/is-deployctl-test-oc-build:1:1","172.30.208.107","odagrun/is-deployctl-test-oc-build","1:1"},
	{ "172.30.208.107/odagrun/is-deployctl-test-oc-build:1-1.2","172.30.208.107","odagrun/is-deployctl-test-oc-build","1-1.2"},
	{ "172.30.208.107/odagrun/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107","odagrun/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	
	{ "a.b:5000/odagrun/is-deployctl-test-oc-build","a.b:5000","odagrun/is-deployctl-test-oc-build",NULL},
	{ "a.b:5000/odagrun/is-deployctl-test-oc-build:1","a.b:5000","odagrun/is-deployctl-test-oc-build","1"},
	{ "a.b:5000/odagrun/is-deployctl-test-oc-build:1.1","a.b:5000","odagrun/is-deployctl-test-oc-build","1.1"},
	{ "a.b:5000/odagrun/is-deployctl-test-oc-build:1:1","a.b:5000","odagrun/is-deployctl-test-oc-build","1:1"},
	{ "a.b:5000/odagrun/is-deployctl-test-oc-build:1-1.2","a.b:5000","odagrun/is-deployctl-test-oc-build","1-1.2"},
	{ "a.b:5000/odagrun/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","a.b:5000","odagrun/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	
	{ "a.b/odagrun/is-deployctl-test-oc-build","a.b","odagrun/is-deployctl-test-oc-build",NULL},
	{ "a.b/odagrun/is-deployctl-test-oc-build:1","a.b","odagrun/is-deployctl-test-oc-build","1"},
	{ "a.b/odagrun/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","a.b","odagrun/is-deployctl-test-oc-build","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	{ "a.b/odagrun/is-deployctl-test-oc-build:1.1","a.b","odagrun/is-deployctl-test-oc-build","1.1"},
	{ "a.b/odagrun/is-deployctl-test-oc-build:1:1","a.b","odagrun/is-deployctl-test-oc-build","1:1"},
	{ "a.b/odagrun/is-deployctl-test-oc-build:1-1.2","a.b","odagrun/is-deployctl-test-oc-build","1-1.2"},
	
	
	{ "centos",DOCKER_REGISTRY,"library/centos",NULL},
	{ "centos:1",DOCKER_REGISTRY,"library/centos","1"},
	{ "centos:1.1",DOCKER_REGISTRY,"library/centos","1.1"},
	{ "centos:1:1",DOCKER_REGISTRY,"library/centos","1:1"},
	{ "centos:1-1.2",DOCKER_REGISTRY,"library/centos","1-1.2"},
	{ "centos@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d",DOCKER_REGISTRY,"library/centos","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	
	{ "jekyll/builder",DOCKER_REGISTRY,"jekyll/builder",NULL},
	{ "jekyll/builder:1",DOCKER_REGISTRY,"jekyll/builder","1"},
	{ "jekyll/builder:1.1",DOCKER_REGISTRY,"jekyll/builder","1.1"},
	{ "jekyll/builder:1:1",DOCKER_REGISTRY,"jekyll/builder","1:1"},
	{ "jekyll/builder:1-1.2",DOCKER_REGISTRY,"jekyll/builder","1-1.2"},
	{ "jekyll/builder@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d",DOCKER_REGISTRY,"jekyll/builder","sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	
	{ "dockregi.gioxa.com/docker/centos7-coreutils","dockregi.gioxa.com","docker/centos7-coreutils",NULL},
	{ "dockregi.gioxa.com/docker/centos7-coreutils:1","dockregi.gioxa.com","docker/centos7-coreutils","1"},
	{ "dockregi.gioxa.com/docker/centos7-coreutils:latest","dockregi.gioxa.com","docker/centos7-coreutils","latest"},
	NULL
};


int get_size_chop_image_data(void)
{
	int size;
	size=sizeof(testlist_chop_image)/sizeof(struct test_data_chop_image_t);
	return size-1;
}

int debug_chop_image(int _i)
{

char * reference=NULL;
char * registry=NULL;
char * namespace=NULL;
	
	printf(" %d: %s =>",_i,testlist_chop_image[_i].image);
chop_image(testlist_chop_image[_i].image, &registry, &namespace,&reference );
	
	int res=0;
	if (registry && testlist_chop_image[_i].registry && strcmp(registry,testlist_chop_image[_i].registry)==0) ;
	else
		res=1;
	if ((namespace && testlist_chop_image[_i].name_space && strcmp(namespace,testlist_chop_image[_i].name_space)==0 )|| namespace==testlist_chop_image[_i].name_space);
	else
		res=1;
	if ((reference && testlist_chop_image[_i].reference && strcmp(reference,testlist_chop_image[_i].reference)==0 )|| reference==testlist_chop_image[_i].reference) ;
	else
		res=1;
	if (res) printf("FAIL\n");
	else printf("OK\n");
 return res;
}

// the command structure
struct test_data_process_nick_t
{
	char * nick;
	char * nick_result;
	char * image;
} ;


static const struct test_data_process_nick_t testlist_process_nick[] = {
	{ "ImageStream", "ImageStream","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build"},
	{ "ImageStream:1.1-2", "ImageStream:1.1-2","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:1.1-2"},
	{ "ImageStream/lev1", "ImageStream/lev1","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build-lev1"},
	{ "ImageStream/$TEST_VAR", "ImageStream/testvar","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build-testvar"},
	{ "ImageStream@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "ImageStream@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	{ "ImageStream/$TEST_VAR@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "ImageStream/testvar@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build-testvar@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	{ "Gitlab", "Gitlab","dockregi.gioxa.com/deployctl/test_oc_build"},
	{ "Gitlab:1-2.3", "Gitlab:1-2.3","dockregi.gioxa.com/deployctl/test_oc_build:1-2.3"},
	{ "Gitlab/lev2:1-2.3", "Gitlab/lev2:1-2.3","dockregi.gioxa.com/deployctl/test_oc_build/lev2:1-2.3"},
	{ "Gitlab/lev2@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "Gitlab/lev2@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","dockregi.gioxa.com/deployctl/test_oc_build/lev2@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	{ "Gitlab@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "Gitlab@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d","dockregi.gioxa.com/deployctl/test_oc_build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d"},
	{ "dockregi.gioxa.com/docker/centos7-coreutils","dockregi.gioxa.com/docker/centos7-coreutils","dockregi.gioxa.com/docker/centos7-coreutils"},
	{ "dockregi.gioxa.com/docker/centos7-coreutils:1","dockregi.gioxa.com/docker/centos7-coreutils:1","dockregi.gioxa.com/docker/centos7-coreutils:1"},
	{ "$CI_REGISTRY/docker/centos7-coreutils:latest","dockregi.gioxa.com/docker/centos7-coreutils:latest","dockregi.gioxa.com/docker/centos7-coreutils:latest"},
	NULL
};

int get_size_process_nick_data(void)
{
	int size;
	size=sizeof(testlist_process_nick)/sizeof(struct test_data_process_nick_t);
	return size-1;
}
int debug_process_nick(int _i)
{

	
	cJSON * env_vars=cJSON_CreateObject();
	cJSON_AddStringToObject(env_vars,"CI_PROJECT_PATH_SLUG" , "deployctl-test-oc-build");
	cJSON_AddStringToObject(env_vars,"CI_REGISTRY_IMAGE" , "dockregi.gioxa.com/deployctl/test_oc_build");
	cJSON_AddStringToObject(env_vars,"CI_REGISTRY" , "dockregi.gioxa.com");
	cJSON_AddStringToObject(env_vars,"TEST_VAR" , "testvar");
	cJSON_AddStringToObject(env_vars,"" , "");
	
	const struct test_data_process_nick_t * test_item=&testlist_process_nick[_i];
	
	char *image_nick=strdup(test_item->nick);
	char * image=NULL;
	
	printf("%d: %s ",_i,image_nick);
	
	image=process_image_nick(&image_nick, env_vars, "172.30.208.107:5000", "odagrun", "registry.hub.docker.com");
	int res=0;
	if ((image && test_item->image && strcmp(image,test_item->image)==0 )|| image==test_item->image )
		;
	else
	{
	printf("img =>'%s' != '%s'\n",image,test_item->image);
		res=1;
	}
	if ((image_nick && test_item->nick_result && strcmp(image_nick,test_item->nick_result)==0 )|| image_nick==test_item->nick_result )
		;
	else
	{
		printf("\nnick=>'%s' != '%s'\n",image_nick,test_item->nick_result);
		
		res=1;
	}
	if (res)
	{
		printf("=>FAIL\n");
		
		
	}
	else
		printf("=>OK\n");
	free(image_nick);
	free(image);
	return (res);
}

struct test_data_change_image_tag_t
{
	char * image;
	char * tag;
	char * image_result;
	int result;
} ;
static const struct test_data_change_image_tag_t testlist_change_image_tag[] = {
	{ "ImageStream", "1","ImageStream:1",0},
	{ "ImageStream:latest", "1","ImageStream:1",0},
	{ "ImageStream:1", NULL,"ImageStream",0},
	{ "ImageStream@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", NULL,"ImageStream",0},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "1.1-2","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:1.1-2",0},
	{ "ImageStream/lev1@sha256:202ecb065b0ca74e07147606d1114a9cf2057efce4f210f7a17fc7b57da6c31d", "1","ImageStream/lev1:1",0},
	{ "", "latest","",(int) (-1)},
	{ NULL, "latest",NULL,(int)(-1)},
	{ NULL, NULL,NULL,(int)(-1)},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build", "1","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:1",0},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:2", "3","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:3",0},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:latest", "","172.30.208.107:5000/odagrun/is-deployctl-test-oc-build",0},
	{ "172.30.208.107:5000/odagrun/is-deployctl-test-oc-build:latest", NULL,"172.30.208.107:5000/odagrun/is-deployctl-test-oc-build",0},
	NULL
};

int get_size_change_image_tag_data(void)
{
	int size;
	size=sizeof(testlist_change_image_tag)/sizeof(struct test_data_change_image_tag_t);
	return size-1;
}

int debug_change_image_tag(int _i)
{
	int res=0;
	
	char *image=NULL;
	if (testlist_change_image_tag[_i].image) image=strdup(testlist_change_image_tag[_i].image);
	printf("%d: %s newtag: %s",_i,image,testlist_change_image_tag[_i].tag);
	int result=change_image_tag(&image,testlist_change_image_tag[_i].tag);
	
	if (result!=testlist_change_image_tag[_i].result)
	{
		printf("result %d != %d\n",result,testlist_change_image_tag[_i].result);
		res=1;
	}
	if ((image && testlist_change_image_tag[_i].image_result && strcmp(image,testlist_change_image_tag[_i].image_result)==0 )|| image==testlist_change_image_tag[_i].image_result);
	else
	{
		printf("result %s != %s\n",image,testlist_change_image_tag[_i].image_result);
		res=1;
	}
	if (res)
	{
		printf("=>FAIL\n");
	}
	else
		printf("=>OK\n");
	if (image)	free(image);
	return res;
}
void cb(void*a,char*path);

void cb(void*a,char*path)
{
	if (a || !a)
	printf("path=%s\n",path);
}




int debug_recursive_dir(int _i)
{
	setdebug();
	cJSON* list=cJSON_CreateArray();
	const char test[]="/build/test";
	size_t len=strlen(test);
	recursive_dir_extract(test, len, list, cb, NULL);
	print_json(list);
	const char test2[]="/build/test2/";
	size_t len2=strlen(test2);
	recursive_dir_extract(test2, len2, list, cb, NULL);
	print_json(list);
	cJSON_Delete(list);
	return 0;
}

struct test_data_comma_t
{
	char * data;
	char *result;
} ;

static const struct test_data_comma_t test_data_comma[] = {
	{"test,hallo", "- test\n- hallo\n" },
	{"test", "- test\n" },
	{"test,hallo,", "- test\n- hallo\n" },
	{"test   ,   hallo  ", "- test\n- hallo\n" },
	NULL
};

int get_size_ctest_data_comma(void)
{
	int size;
	size=sizeof(test_data_comma)/sizeof(struct test_data_comma_t);
	return size-1;
}

int debug_comma_sep_list(int _i)
{
	printf("%d: %s",_i,test_data_comma[_i].data);
	cJSON * test=comma_sep_list_2_json_array(test_data_comma[_i].data);
	cJSON* result=yaml_sting_2_cJSON (NULL ,test_data_comma[_i].result);
	int res=0;
	cJSON * wish=cJSON_GetArrayItem(result,0);
	int len_test=cJSON_GetArraySize(test);
	int len_result=cJSON_GetArraySize(wish);
	if (len_test!=len_result)
	{
		
		res=-1;
	}
	else
	{
		int j=0;
		for (j=0;j<len_result;j++)
		{
			const char * a=cJSON_GetArrayItem(wish, j)->valuestring;
			const char * b=cJSON_GetArrayItem(test, j)->valuestring;
			if(strcmp(a,b)!=0){
				res=-1;
				break;
			}
		}
	}
	cJSON_Delete(test);
	cJSON_Delete(result);
	if (res)
	{
		printf("=>FAIL\n");
	}
	else
		printf("=>OK\n");
	return res;
}


int main(void)
{
	
	setdebug();
	debug("recdir\n");
	
	
	
	int nr=0;
	debug_recursive_dir(0);
	
	debug("test comma\n");
	
	printf("\n Comma separated list\n");
	nr=get_size_ctest_data_comma();
	for (int i=0;i<nr;i++)
		if (debug_comma_sep_list(i))
			exit;
	printf("\n CHOP NAMESAPEC\n");
	nr= get_size_chop_ns_data();
	//debug_process_nick(1);
	for (int i=0;i<nr;i++)
		if (debug_chop_ns(i))
			exit;
	
	printf("\n CHOP IMAGE\n");
	nr= get_size_chop_image_data();
	//debug_process_nick(1);
	for (int i=0;i<nr;i++)
		if (debug_chop_image(i))
			break;
	printf("\n Process NICK\n");
	nr=get_size_process_nick_data();
	for (int i=0;i<nr;i++)
		if (debug_process_nick(i))
			break;
	printf("\n CHANGE IMAGE TAG\n");
	nr=get_size_change_image_tag_data();
	for (int i=0;i<nr;i++)
	    if(debug_change_image_tag(i))
			break;
		
}

size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...)
{
	return 0;
}
