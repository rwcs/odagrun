//
//  main.c
//  dockerlogin
//
//  Created by Danny Goossen on 18/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

/*
 dispatcher.c
 Created by Danny Goossen, Gioxa Ltd on 9/10/17.
 
 MIT License
 
 Copyright (c) 2017 deployctl, Gioxa Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */


//#include "dispatcher.h"
#include "deployd.h"
#include "dkr_api.h"
#include "manifest.h"
#include "image_parameters.h"

static parameter_t * parameters=NULL;

enum IS_name{
	IS_name_useast,
	IS_name_gioxa
};

// choice to keep parameter global, so signal handler can useit too.
//static parameter_t * parameters=NULL;

void docker_test(const char * image_in, enum IS_name IS)
{
	
	char *ns=NULL;
	char *rg=NULL;
	chop_image(image_in, &rg, &ns, NULL);
	
   // char * image=strdup("dockregi.gioxa.com/c-build-tools/cent7build");
    char * token_docker = strdup("ItioqvbhoXmcstfFHqE78qIfunPZXX8d_Xp2AsyT8Us");
	
	char * token_gioxa=read_a_file( "/Users/dgoo2308/oc_service_gioxa/token");
	char *namespace_gioxa=read_a_file("/Users/dgoo2308/oc_service_gioxa/namespace");
	
	char *token_useast1=strdup("GfCLLzZL_OoYxYsHCAIgZiUAWLsUnOZZ3opN5eF0rv8"); //"read_a_file("/Users/dgoo2308/oc_service_useast1/token");
	char *namespace_useast1=strdup("odagrun-starter"); //read_a_file("/Users/dgoo2308/oc_service_useast1/namespace");
	
	
   //char * image=strdup("dockregi.gioxa.com/dgoo2308/dockertest:latest");
     //char * image=strdup("dgoo2308/test-docker-config:latest");
	//char * image=strdup("centos");
	//char * image=strdup("dgoo2308/test-centos-add-config:9150");
	//char * image=strdup("jekyll/builder");
	
   //const char registry_domain[]="172.30.1.1:5000";
	
	//const char registry_domain[]="52.73.240.105";
	
   const char registry_domain_useast1[]="registry.starter-us-east-1.openshift.com";
	const char registry_domain_gioxa[]="dr.apps.gioxa.com:80";
	
//	char * image=strdup("dr.apps.gioxa.com:80/odagrun/is-dgoo2308-fpm-image-9503:fpm");
	
	//const char registry_domain[]="dr.apps.gioxa.com:80";
	
   const char docker_domain[]=DOCKER_REGISTRY;
   const char gitlab_registry_domain[]="dockregi.gioxa.com";
	
	const char* registry_domain=NULL;
	const char* registry_token=NULL;
	const char* registry_namespace=NULL;
	int registry_insecure=0;
	if (IS==IS_name_gioxa)
	{
		registry_domain=registry_domain_gioxa;
		registry_namespace=namespace_gioxa;
		registry_token=token_gioxa;
		registry_insecure=1;
	}
	else if (IS==IS_name_useast)
	{
		registry_domain=registry_domain_useast1;
		registry_token=token_useast1;
		registry_namespace=namespace_useast1;
	}
	char *image=strdup(image_in);
	
   //const char token[]="token";
  //  char * namespace=strdup("odagrun");
   char image_pod[1024];
   
   int res=0;
   cJSON * env_vars=cJSON_CreateObject();
   cJSON_AddStringToObject(env_vars, "CI_REGISTRY", gitlab_registry_domain);
   cJSON_AddStringToObject(env_vars, "CI_PROJECT_NAMESPACE", "c-build-tools/cent7build");
   //cJSON_AddStringToObject(env_vars, "CI_PROJECT_NAMESPACE", "c-build-tools");
   cJSON_AddStringToObject(env_vars, "CI_REGISTRY_USER", getenv("GITLABUSR"));
   cJSON_AddStringToObject(env_vars, "CI_REGISTRY_PASSWORD", getenv("GITLABPWD"));
   
   //int check=dkr_api_check_registry(api_data, name);
   if(image)
   {
	   setdebug();
      debug ("start");
      struct dkr_api_data_s * apidata=NULL;
      dkr_api_init(&apidata, DEFAULT_CA_BUNDLE);
   
      int add_reg_res=0;
	   
	   char * t_dir=name_space_rev_slug(image);
      // add ImageStream registry
	   t_dir=vcmkdir("%s/dl_images/%s",getenv("HOME"),t_dir);
	
      add_reg_res=dkr_api_add_registry(apidata, registry_domain,registry_namespace,"openshift",registry_token, NULL,registry_insecure);
      alert("added ImageStream registry: %d\n",add_reg_res);
      
      add_reg_res=dkr_api_add_registry(apidata, "registry.hub.docker.com", NULL, NULL, NULL, NULL,0);
      alert("added docker hub: %d\n",add_reg_res);
	   
	   add_reg_res=dkr_api_add_registry(apidata, cJSON_get_key(env_vars,"CI_REGISTRY" ), NULL, NULL, NULL, NULL,0);
	   alert("added global gitlab registry: %d\n",add_reg_res);
      // add CI registry (one for project name space, one for other name spaces due to bug in gitlab, whichj returns http 500 on public project w ci_usr/passwd for other name spaces)
   
      if (!res)
      {
         if (cJSON_GetObjectItem(env_vars,"CI_REGISTRY" ))
         {
            // need to provide login in case this project is not public
            dkr_api_add_registry(apidata, cJSON_get_key(env_vars,"CI_REGISTRY" ),cJSON_get_key(env_vars,"CI_PROJECT_NAMESPACE") , cJSON_get_key(env_vars,"CI_REGISTRY_USER" ), cJSON_get_key(env_vars,"CI_REGISTRY_PASSWORD" ), NULL,0);
            alert("\nadded gitlab registry for %s: %d\n",cJSON_get_key(env_vars,"CI_PROJECT_NAMESPACE"), add_reg_res);
            
            // no login as CI_JOB_TOKEN has no access if project not public (http500 on Realm)
            add_reg_res=dkr_api_add_registry(apidata, cJSON_get_key(env_vars,"CI_REGISTRY" ), "c-build-tools",NULL,NULL, NULL,0);
            
            //alert("\nadded gitlab registry: %d\n", add_reg_res);
         }
      }
      print_json(apidata->registries);
      int image_check_result=0;
    
      // need to do this for all images, not only job, "image" but also for services!!!!
	   char * image_nick=image;
	  
	   image=process_image_nick(&image_nick, NULL, registry_domain, registry_namespace, DOCKER_REGISTRY);
	   setdebug();
      if (image)
      {
	
		 // Check image
         cJSON * result_data=NULL;
         if (image && strlen(image)>0)
         {
            char * manifest=NULL;
            res=dkr_api_get_manifest_raw(apidata, "application/vnd.docker.distribution.manifest.v2+json", image, &manifest);
			// res=dkr_api_get_manifest_raw(apidata, "application/vnd.docker.container.image.v1+json", image, &manifest);
			 
			 //res=dkr_api_get_manifest(apidata,"application/vnd.docker.container.image.v1+json", image, &manifest);
			 
            if (res==200 && manifest)
            {
               debug("manifest:\n%s\n",manifest);
              

                  //Write_dyn_trace_pad(trace, none, 60, "      + get config.json ...");
                  cJSON * manifest_json=cJSON_Parse(manifest);
                  cJSON * manifest_config=NULL;
                  const char * blob=NULL;
                  char * config=NULL;
                  if (manifest_json)  manifest_config=cJSON_GetObjectItem(manifest_json, "config");
                  if(manifest_config) blob=cJSON_get_key(manifest_config, "digest");
                  if (blob) res=dkr_api_blob_get(apidata, image, blob, &config);
                  if ((res==200 || res==201) && blob)
                  {
                     printf( "                [ok]\n");
                     printf( "      + pull config.json blob...\n");
                  }
                  else
                  {
                    printf ("	          [FAIL]\n"); // fail layer
                     printf ( "\n pull config.json error: %d\n",res);
                     
                  }
               printf("CONFIG:\n");
               if (config)
               {
                  cJSON * config_json=cJSON_Parse(config);
				  // cJSON_DeleteItemFromObject(config_json, "container");
				  // cJSON_DeleteItemFromObject(config_json, "container_config");
				   
                  print_json(config_json);
                  
               }
				

				
               cJSON * manifest_layers=NULL;
				int schemaVersion=0;
				
				// sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4
				char empty[32]={ 0x1F, 0x8B, 0x08, 0x00, 0x00, 0x09, 0x6E, 0x88,
					0x00, 0xFF, 0x62, 0x18, 0x05, 0xA3, 0x60, 0x14,
					0x8C, 0x58, 0x00, 0x08, 0x00, 0x00, 0xFF, 0xFF,
					0x2E, 0xAF, 0xB5, 0xEF, 0x00, 0x04, 0x00, 0x00};
				if (manifest_json)
					schemaVersion=cJSON_safe_GetNumber(manifest_json, "schemaVersion");
				
				if (schemaVersion==1)
				{
					manifest_layers=cJSON_GetObjectItem(manifest_json, "fsLayers");
				}
				else if (schemaVersion==2)
				{
                  manifest_layers=cJSON_GetObjectItem(manifest_json, "layers");
				}
               cJSON * layer;
				if (manifest_layers)
               cJSON_ArrayForEach(layer, manifest_layers)
               {
				   if (schemaVersion==2)
				   		dkr_api_blob_get_file(apidata, image, cJSON_get_key(layer, "digest"), "%s/%s.tar.gz",t_dir,cJSON_get_key(layer, "digest")+7);
				   else if (schemaVersion==1)
					   dkr_api_blob_get_file(apidata, image, cJSON_get_key(layer, "blobSum"), "%s/%s.tar.gz",t_dir,cJSON_get_key(layer, "blobSum")+7);
               }
				
				write_file_v(manifest, 0, "%s/manifestv2.json",t_dir);
				if (config)
				write_file_v(config, 0, "%s/config.json",t_dir);
               
                /*
                  res=dkr_api_blob_push_raw (apidata, image_check, blob, config);
                  if (res==200 || res==201)
                  {
                     printf ( "                [ok]\n");
                     printf ("         %s\n",blob);
                  }
                  else
                  {
                     printf ( "	          [FAIL]\n"); // fail layer
                     printf ("\n push config.json error: %d\n",res);
                    // print_api_error(trace, res);
                  }
                  if (config) free(config);
                  if (manifest_json) cJSON_Delete(manifest_json);
              
                printf("\ndebug: manifest v2+json:\n%s\n",manifest);
               
            */
            }
         
         
         }}
	   /*
      char *v1_manifest=NULL;
      res=dkr_api_get_manifest_raw(apidata, "application/vnd.docker.distribution.manifest.v1+json", image, &v1_manifest);
      
      
      if (image_check_result!=200)
      {
         debug( "[FAIL]\n");
         res=-99;
         if (image_check_result<0)
         {
           debug( "\n\tError image check: %s\n",curl_easy_strerror(image_check_result));
         }
         else
         {
            switch (image_check_result) {
               case 404:
                  debug( "\n\tImage Not Found (HTTP %d)\n",image_check_result);
                  break;
               case 500:
                  debug( "\n\tRegistry server error (HTTP %d)\n",image_check_result);
                  break;
               case 403:
                  debug( "\n\tNot Authorised (HTTP %d)\n",image_check_result);
                  break;
               default:
                  debug( "\n\tHttp-Error : HTTP %d\n",image_check_result);
                  break;
            }
         }
         debug( "\nImage pre check failed, exit!\n");
      }
      else
      {
        debug("[OK]\n\n");
     
         char * strippedV1= strip_manifest_v1(v1_manifest);
         cJSON * manifest_json=cJSON_Parse(strippedV1);
         //sign V1 manifest
         char * Signed_manifest=SignManifest(manifest_json);
         printf("signedManifest:\n%s\n",Signed_manifest);
         res=dkr_api_manifest_push_raw(apidata,image, NULL, Signed_manifest,0);
         if (res==200||res==201||res==204)
         {
            printf( "                [ok]\n");
            res=0;
         }
         else
            printf( "              [FAIL]\n");

      }
		*/
      dkr_api_cleanup(&apidata);
      
   }
   
}
void manifest()
{
   
   return;
}

#include "registry.h"


// ********************************************************************
//          MAIN
// ********************************************************************

int main(int argc, char ** argv)
{
	
   thread_setup(); //TODO move this to main
   curl_global_init(CURL_GLOBAL_ALL); //TODO move this to main
#ifdef run_local
   git_libgit2_init();
#endif
   //git_threads_init();
   const char image[]="registry.starter-us-east-1.openshift.com/odagrun-starter/is-gioxa-build-images-centos7-ruby-node-31848231:base";
 //	const char image[]="dr.apps.gioxa.com:80/odagrun/is-executer-image:55576";
//	const char image[]="dr.apps.gioxa.com:80/odagrun/is-executer-image:55954";
	
	
//	const char image[]="gioxa/fpm-centos";
	//sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4
   docker_test(image,IS_name_useast);
   curl_global_cleanup(); //TODO move this to main
   thread_cleanup(); //TODO move this to main
#ifdef run_local
   git_libgit2_shutdown();
#endif
     alert("exit deployctl\n");
   // TODO free parameters!!!
   
   return 0;
}
