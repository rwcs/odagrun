
/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/* \file MicroBadgerUpdate.c
 *  \brief custom internal MicroBadgerUpdate command
 *  \author Created by Danny Goossen on 22/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 */

#include "deployd.h"
#include "MicroBadgerUpdate.h"

#include "curl_commands.h"
#include <curl/curl.h>

#include "dyn_buffer.h"
#include "dyn_trace.h"
#include "error.h"
#include "simple_http.h"

/**
 \brief typedef parameter structure, struct microbadger_parameter_s
 */
typedef  struct microbadger_parameter_s microbadger_parameter_t;

/**
 \brief parameter structure, general sub-program settings
 Parameter settings for the curl API sub programs
 */
struct microbadger_parameter_s
{
	u_int8_t verbose; /**< verbose output */
	u_int8_t quiet;   /**< no output */
	u_int8_t allow_fail; /**< allow failure */
	char * image; /**< image we want update for */
};

/**
 \brief internal odagrun curl_post command
 */
int process_options_microbadger(size_t argc, char * const *argv, microbadger_parameter_t * parameters);

/**
 \brief internal free parameters
 */
void clear_microbadger_parameters(microbadger_parameter_t **parameter);

/**
 \brief internal get Microbadger WebHookUrl
 */
int microbadger_getwebhook(const char * image, CURL *curl,struct trace_Struct * trace,char**webhook);

/**
 \brief internal do MicroBadger update
 */
int post_update(const char *url,CURL *curl ,struct trace_Struct * trace);

int microbadgerupdate(const cJSON * job,struct trace_Struct * trace,size_t argc,char * const *argv)
{
	
	int res=-1;
	
	microbadger_parameter_t * parameters=calloc(1, sizeof(struct microbadger_parameter_s));
	
	int argscount=0;
	
	if ((argscount=process_options_microbadger(argc, argv,parameters)) < 1 || !parameters->image)
	{
		error("\n *** ERRORin command line options, exit \n");
		Write_dyn_trace(trace, red, "\nError command line options\n");
		update_details(trace);
		return res;
	}
	char * webhook=NULL;
	
	// todo chop up image !!!
	if (argscount!=(int)argc)
		Write_dyn_trace(trace, yellow, "\nIgnoring non option commandline arguments\n");
	
	cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	char * image=process_image_nick(&parameters->image, env_vars, "", "", DOCKER_HUB);
	
	
	char * registry_name=NULL;
	char * namespace=NULL;
	char * reference=NULL;
	
	chop_image(image, &registry_name, &namespace, &reference);
	
	if (!registry_name || strcmp(registry_name,DOCKER_HUB)!=0 || ! namespace)
	{
		Write_dyn_trace(trace, red, "\nNot a DockerHub image\n");
		update_details(trace);
		clear_microbadger_parameters(&parameters);
		if (registry_name) free(registry_name);
		if (namespace) free(namespace);
		if (reference) free(reference);
		return -1;
	}
	Write_dyn_trace(trace, none, "\n   For Image: ");
	Write_dyn_trace(trace, cyan, "%s\n",namespace);
	CURL *curl =simple_http_init(DEFAULT_CA_BUNDLE,easy_http_follow_location, easy_http_quiet, easy_http_SSL_VERIFYPEERL, PACKAGE_STRING);
	if (!curl)
	{
		Write_dyn_trace(trace, red, "System: failed to initialize curl\n");
		clear_microbadger_parameters(&parameters);
		update_details(trace);
		return -1;
	}
	
	Write_dyn_trace(trace, none, "\n");
	Write_dyn_trace_pad(trace, yellow, 60,"   * Get WebHook URL ");
	update_details(trace);
	res=microbadger_getwebhook(namespace, curl,trace,&webhook);
	
	if (!res && webhook)
	{
		Write_dyn_trace_pad(trace, yellow, 60, "   * Post Update");
		update_details(trace);
		res=post_update(webhook, curl,trace);
	}
	if(!res)
	{
		Write_dyn_trace(trace, none, "\n   url: ");
		Write_dyn_trace(trace, blue, "https://microbadger.com/images/%s\n",namespace);
	}
	if (webhook) free(webhook);
	if(res && parameters->allow_fail)
	{
		Write_dyn_trace(trace, yellow, " Warning: allowed to fail, exit success\n");
		res=0;
	}
	clear_microbadger_parameters(&parameters);
	if (registry_name) free(registry_name);
	if (namespace) free(namespace);
	if (reference) free(reference);
	update_details(trace);
	simple_http_clean(&curl);
	return res;
}





int microbadger_getwebhook(const char * image, CURL *curl,struct trace_Struct * trace,char**webhook)
{
	int res=-1;
	
	if(curl)
	{
		char * result=NULL;
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res= simple_httpget_v(curl, &result,NULL,"https://api.microbadger.com/v1/images/%s",image);
		} while (((res>=500 && res<600) || res<0) && count<5);
		
		if (res >= 200 && res <300)
		{
			res=-1;
			cJSON * reply=NULL;
			if (result && strlen(result)>0) reply=cJSON_Parse(result);
			const char * WebhookURL=NULL;
			if (reply)  WebhookURL=cJSON_get_key(reply, "WebhookURL");
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				Write_dyn_trace(trace, magenta, "\nMicroBadger get WebhookURL, Failed to parse Result!\n");
			}
			if (WebhookURL && webhook)
			{
				*webhook=strdup(WebhookURL);
				Write_dyn_trace(trace, green, "  [OK]\n");
				res=0;
			}
			else if(reply)
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				Write_dyn_trace(trace, magenta, "\nMicroBadger get WebhookURL, Failed to extract WebhookURL!\n");
			}
			if (reply) cJSON_Delete(reply);
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n\n\tError: ");
			Write_dyn_trace(trace, none, "GET webhook for %s:\n",image);
			print_api_error(trace, res);
			if (result && strlen(result)>0)
			{
				Write_dyn_trace(trace, magenta, "\n\tResponse: ");
				Write_dyn_trace(trace, none, "%s\n",result);
				debug("curl_post reply:\n>>>\n%s\n<<<\n",result);
			}
		}
		if (result) free(result);
	}
	return res;
}





int post_update(const char *request,CURL *curl,struct trace_Struct * trace )
{
	int res=0;
	if(curl)
	{
		
		
		char * result=NULL;
		int count=0;
		do{
			if (count) usleep(2000000LL * count*count);
			count++;
			res= simple_httppost_v(curl, &result,NULL,"%s",request);
		} while (((res>=500 && res<600) || res<0) && count<5);

		if (res== 200 )
		{
			//result in chunk
			debug("curl_post reply:\n>>>\n%s\n<<<\n",result);
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
			print_api_error(trace, res);
			if (!res && result && strlen(result)>0)
			{
				Write_dyn_trace(trace, magenta, "\n Response: ");
				Write_dyn_trace(trace, none, "%s\n",result);
				debug("curl_post reply:\n>>>\n%s\n<<<\n",result);
			}
		}

		if (result) free(result);
	}
	else
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace, magenta, "\nSystem: failed to initialize curl\n");
	}
	return res;
}


// helper functions

void clear_microbadger_parameters(microbadger_parameter_t **parameter)
{
	if (parameter && *parameter)
	{
		//	if((*parameter)->rootfs!=NULL) free((*parameter)->rootfs);
		
		free(*parameter);
		(*parameter)=NULL;
	}
	return;
}

int process_options_microbadger(size_t argc, char * const *argv, microbadger_parameter_t * parameters)
{
	
	struct option long_options[] = {
		
		{ "verbose",       0, NULL, 'v' },
		{ "quiet",         0, NULL, 'q' },
		{ "allow-fail",    0, NULL, 'f' },
		{ "allow_fail",    0, NULL, 'f' },
		{ "allow-failure", 0, NULL, 'f' },
		{ "allow_failure", 0, NULL, 'f' },
		{ "image",         1, NULL, 'i' },
		{ NULL,            0, NULL,  0  }
	};
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameters->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameters->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameters->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameters->quiet = 1;
				}
					break;
				case 'i': {
					parameters->image=get_opt_value( optarg,NULL,NULL);
				}break;
				case 'f': {
					parameters->allow_fail=1;
				}
					break;
					
					
					/* otherwise    : display usage information */
				default:
					return -1;
					break;
			}
		}
	}
	return optind;
}
