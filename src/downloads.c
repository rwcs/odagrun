/*! \file downloads.c
    \author Created by Danny Goossen on 23/7/17.
    \copyright Copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#include "deployd.h"
#include <ftw.h>

// Forward declarations

/**
 *  @brief Start an artifacst download
 *  @param artifacts cJSON list of the artifacts
 *  @param dl_artifacts struct with all relevant info to do the download
 *  @param thread_number the number of this thread
 *  @return 0 on success
 */
static int start_an_artifacts_download(cJSON * artifacts,struct dl_artifacts_s * dl_artifacts,int thread_number);

/**
 *  @brief Unzips the item in \a tmp
 *  @param tmp cJSON list of the artifacts
 *  @param dl_artifacts struct with all relevant info to do the download
 *  @return 0 on success
 */
static int unzip_one(cJSON* tmp,struct dl_artifacts_s * dl_artifacts);

/**
 \brief add artifacts download item from a dependency to array
 \param [in,out] artifacts pointer to array to add or create
 \param dependency the item to transform
 */
static void make_artifacts(cJSON ** artifacts,cJSON * job,cJSON * dependency);

/**
 \brief find the depencency object with the given name
 \param dependencies object to look in
 \param name to find in dependencies
 \return dependency object
 */
static cJSON * find_dependency_obj(cJSON *dependencies,const char * name);


void init_download_artifacts(struct dl_artifacts_s * dl_artifacts, int count,const char * proj_dir, char* zip_dir, const char * ca_bundle)
{
   //setdebug();
   vmkdir("%s",proj_dir );
   vmkdir("%s",zip_dir );
   int i=0;
   dl_artifacts->free_count=NUMT;
   for(i=0; i< NUMT; i++) {
      dl_artifacts->data[i].finish=0;
      dl_artifacts->data[i].free=1;
      dl_artifacts->data[i].path=zip_dir;
      dl_artifacts->data[i].http_code=-1;
      dl_artifacts->data[i].artifact=NULL;
      dl_artifacts->data[i].progress=0;
	   dl_artifacts->data[i].ca_bundle=ca_bundle;

   }
   dl_artifacts->todo=count;
   dl_artifacts->unzip_todo=count;
   dl_artifacts->next_unzip=0;
   dl_artifacts->next=0;
   dl_artifacts->proj_dir=proj_dir;
   dl_artifacts->zip_dir=zip_dir;

   return;
}

void cancel_download_artifacts(struct dl_artifacts_s * dl_artifacts)
{
   debug("Canceling download\n");
   int i=0;
   for(i=0; i< NUMT; i++) {
      if (!dl_artifacts->data[i].free) pthread_cancel(dl_artifacts->tid[i]);
   }
   return;
}

void close_download_artifacts(struct dl_artifacts_s * dl_artifacts)
{
   debug("waiting for unfinished threads to end\n");
   int i=0;
   for(i=0; i< NUMT; i++) {
      if (!dl_artifacts->data[i].free) pthread_join(dl_artifacts->tid[i], NULL);
      dl_artifacts->data[i].finish=0;
      dl_artifacts->data[i].path=NULL;
      dl_artifacts->data[i].free=1;
      dl_artifacts->data[i].path=NULL;
      dl_artifacts->data[i].http_code=-1;
      dl_artifacts->data[i].artifact=NULL;
   }
   dl_artifacts->free_count=NUMT;

   return;
}


int start_an_artifacts_download(cJSON * artifacts,struct dl_artifacts_s * dl_artifacts,int thread_number)
{
   int error_1=0;
   dl_artifacts->data[thread_number].free=0;
   dl_artifacts->data[thread_number].error=0;
   dl_artifacts->data[thread_number].artifact=cJSON_GetArrayItem(artifacts, dl_artifacts->next);
   cJSON*error_count_json=cJSON_GetObjectItem(dl_artifacts->data[thread_number].artifact, "error_count");
   cJSON_ReplaceItemInObject(dl_artifacts->data[thread_number].artifact, "status", cJSON_CreateString("download"));
   cJSON_GetObjectItem(dl_artifacts->data[thread_number].artifact, "progress")->valueint=0;
   if (error_count_json)
      dl_artifacts->data[thread_number].error_count=error_count_json->valueint;
   else {
      dl_artifacts->data[thread_number].error_count=0;
      cJSON_AddNumberToObject(dl_artifacts->data[thread_number].artifact, "error_count", 0);
   }
   dl_artifacts->data[thread_number].progress=0;
   error_1=pthread_create(&dl_artifacts->tid[thread_number],
                        NULL, /* default attributes please */
                        get_artifact,
                        (void *)&dl_artifacts->data[thread_number]);
   if(0 != error_1)
   {
      error( "Couldn't run thread number %d, errno %d\n", thread_number, error_1);
      return -1;
   }
   else
   {
      dl_artifacts->free_count--;
      return 0;
   }

}

static int unzip_one(cJSON* tmp,struct dl_artifacts_s * dl_artifacts)
{
  if (!tmp)
  {
    error("unzip one no JSON data\n");
  return -1;

}
    char filename[1024];
    snprintf(filename,1024, "%s/%s.zip",dl_artifacts->zip_dir, cJSON_get_key(tmp, "name"));
    char unzipdest[1024];
    snprintf(unzipdest, 1024,"%s/project_dir",dl_artifacts->proj_dir);
    debug("* unzip_one: \n ->file: %s \n ->dest: %s\n",filename,unzipdest);
    if (unzip(filename, unzipdest)==0)
    { //success
       dl_artifacts->next_unzip++;
       dl_artifacts->unzip_todo--;
       cJSON_ReplaceItemInObject(tmp, "unzip_status", cJSON_CreateString("done"));
       debug("Unzip status to done\n");
       return 0;
    }
    else
    { // fail
       cJSON_ReplaceItemInObject(tmp, "unzip_status", cJSON_CreateString("error"));
        error("Unzip status to ERROR\n");
       return -1;
    }
}

// Prepare artifacts create a JSON structure containing everything needed to download artifact
// from JOB::dependencies
// artifacts[]:
//     :api (url request)

void make_artifacts(cJSON ** artifacts,cJSON * job,cJSON * dependency)
{
   cJSON * file=cJSON_GetObjectItem(dependency, "artifacts_file");
   if (file)
   {
      cJSON * tmp=cJSON_CreateObject();
      cJSON_add_string_v(tmp, "api", "%s/api/v4/jobs/%d/artifacts",cJSON_get_key(job, "ci_url"),cJSON_GetObjectItem(dependency, "id")->valueint);
      cJSON_AddStringToObject(tmp, "name",cJSON_get_key(dependency, "name" ) );
      cJSON_AddStringToObject(tmp, "status","pending");
      cJSON_AddStringToObject(tmp, "unzip_status","");
      cJSON_AddStringToObject(tmp, "filename",cJSON_get_key(file, "filename" ));
      // JOB-TOKEN is token as per dependency token
      cJSON_add_string_v(tmp, "header_auth","JOB-TOKEN: %s",cJSON_get_key(dependency, "token" ));
      cJSON_AddNumberToObject(tmp, "size",cJSON_GetObjectItem(file, "size")->valueint);
      cJSON_AddNumberToObject(tmp, "progress",0);
      if(!(*artifacts)) *artifacts=cJSON_CreateArray();
      cJSON_AddItemToArray(*artifacts, tmp);
   }
   else
      debug("artifacts %s has no file \n",cJSON_get_key(dependency, "name" ));
}

cJSON * find_dependency_obj(cJSON *dependencies,const char * name)
{
   cJSON * dependency=NULL;
   cJSON * result=NULL;
   cJSON_ArrayForEach(dependency,dependencies)
   	if (validate_key(dependency, "name", name)==0)
         result=dependency;
   return result;
}

cJSON * prepare_artifacts(cJSON * job)
{
   cJSON * artifacts=NULL;
   cJSON * dependencies=cJSON_GetObjectItem(job, "dependencies");
   debug("Getting %d dependencie(s)\n",cJSON_GetArraySize(dependencies));
   cJSON * env_vars=cJSON_GetObjectItem(job, "env_vars");
   cJSON * dependencies_var=NULL;
   cJSON * dep_var=cJSON_GetObjectItem(env_vars, "dependencies");
   if (dep_var)
   {
      cJSON * tmp=NULL;
      if (cJSON_IsString(dep_var))
      {
         tmp=yaml_sting_2_cJSON(NULL, dep_var->valuestring);
         if (tmp && cJSON_IsArray(tmp) && cJSON_GetArraySize(tmp)>0) dependencies_var=cJSON_DetachItemFromArray(tmp, 0);
         if (tmp) cJSON_Delete(tmp);
      }
      else if (cJSON_IsArray( dep_var)) dependencies_var=cJSON_Duplicate(dep_var, 1);
  
      //todo check if array, if string make string of 1
      if (dependencies_var)
      {
         debug("\n*******************************\nDependencies var\n");
         print_json(dependencies_var);
         cJSON * dependency_var;
         cJSON * dependency=NULL;
         cJSON_ArrayForEach(dependency_var,dependencies_var)
         {
            if (cJSON_IsString(dependency_var))
            {
               char * tofind=resolve_const_vars(dependency_var->valuestring, env_vars);
               if (tofind)
               {
                  dependency=find_dependency_obj(dependencies,tofind);
                  if (dependency)
                  {
                     make_artifacts(&artifacts,job,dependency);
                  }
                  else
                  {
                     error("requiered dependency not found\n");
                  }
                  oc_free(&tofind);
                  tofind=NULL;
               }
               else
                  error("could not resolve variables\n");
            }
            else
            {
               error("dependency not a string\n");
            }
         }
      }
   }
   else
   {
   //setdebug();
      cJSON * dependency;
      cJSON_ArrayForEach(dependency,dependencies)
      {
         make_artifacts(&artifacts,job,dependency);
      }
   }
   return artifacts;
}


// quick scan of the threads to see if ANY PROGRESS CHANGE
// resturns 1 on an update
int progress_change(struct dl_artifacts_s * dl_artifacts)
{
   int i;
   int result=0;
   for(i=0; i< NUMT; i++)
   {
      if (dl_artifacts->data[i].progress)
      {
         debug("prgress change\n");
         result= 1;
         dl_artifacts->data[i].progress=0;
      }
   }
   return result;
}

void scan_artifacts_status(cJSON * artifacts, struct trace_Struct *trace)
{
   debug("scan artifacts status\n");
   cJSON * artifact;
   clear_till_mark_dynamic_trace(trace);
   ssize_t len_job=0;
   cJSON_ArrayForEach(artifact,artifacts)
   {
      len_job=strlen(cJSON_get_key(artifact, "name"));
      char padding[]="                                 ";
      if (len_job<20) padding[20-len_job]=0; else padding[0]=0;
      Write_dyn_trace (trace,none,"  + %s%s  ",cJSON_get_key(artifact, "name"),padding);
      if (validate_key(artifact,"status", "done")==0)
      Write_dyn_trace(trace,green,"  [OK]  ");
      else if (validate_key(artifact,"status", "error")==0)
         Write_dyn_trace(trace,red," [FAIL] ");
       else if (validate_key(artifact,"status", "download")==0)
       {
          Write_dyn_trace(trace, none, "%2d%%",cJSON_GetObjectItem(artifact, "progress")->valueint);
       }
      else
         Write_dyn_trace(trace,yellow,cJSON_get_key(artifact, "status"));
      
      Write_dyn_trace(trace,none,"    \t");

      if (validate_key(artifact,"unzip_status", "done")==0)
         Write_dyn_trace(trace,green," [OK] ");
      else if (validate_key(artifact,"unzip_status", "error")==0)
         Write_dyn_trace(trace,red,"[FAIL]");
      else
        Write_dyn_trace(trace,yellow,cJSON_get_key(artifact, "unzip_status"));
      Write_dyn_trace(trace,bold_yellow,"\n");
   }
   return;
}


int download_artifacts(cJSON * artifacts, struct dl_artifacts_s * dl_artifacts)
{
   int i;
   int error = 0;
   // check if we have threads finished / maybe retry on error
   if (dl_artifacts->free_count != NUMT)
   { //swoop and check if finished
      for(i=0; i< NUMT; i++) {
         if (dl_artifacts->data[i].finish && !dl_artifacts->data[i].free )
         {
            pthread_join(dl_artifacts->tid[i], NULL); // shall not return EINTR
            debug("Joined artithread %s\n",cJSON_get_key(dl_artifacts->data[i].artifact, "name"));
            dl_artifacts->data[i].finish=0;
            dl_artifacts->data[i].free=1;
            dl_artifacts->free_count++;
            if (dl_artifacts->data[i].error !=0)
            {
               debug("got a download error, error_cout=%d\n",dl_artifacts->data[i].error_count);
               dl_artifacts->data[i].error_count++;
               if (dl_artifacts->data[i].error_count < 3)
               {
                  cJSON_GetObjectItem(dl_artifacts->data[i].artifact, "error_count")->valueint=dl_artifacts->data[i].error_count;
                	cJSON_ReplaceItemInObject(dl_artifacts->data[i].artifact, "status", cJSON_CreateString("retry"));
                  // and startit again
                  error=start_an_artifacts_download(artifacts, dl_artifacts, i);
                  if(0 != error) return error;
               }
               else return -1;
            }
            else
            {
            	cJSON_ReplaceItemInObject(dl_artifacts->data[i].artifact, "unzip_status", cJSON_CreateString("pending"));
              debug("unzip pending\n");
            }
         }
      }
   }

   // do we still have something to do? if not and all threads free we're finished return 0;
   if (dl_artifacts->todo==0 && dl_artifacts->free_count == NUMT && dl_artifacts->unzip_todo==0) return 0;

   // do we do have something to do
   if (dl_artifacts->todo > 0 )
   {
     debug("todo==%d, unzip_todo %d, next download=%d, next unzip=%d\n",dl_artifacts->unzip_todo,dl_artifacts->todo, dl_artifacts->next,dl_artifacts->next_unzip);
      //if no free threads, sleep and comeback later
      if (dl_artifacts->free_count == 0)
      {
         cJSON * tmp=cJSON_GetArrayItem(artifacts, dl_artifacts->next_unzip);
         if (tmp && validate_key(tmp,"status", "done")==0)
         { // unzip one
            if (unzip_one(tmp,dl_artifacts)==-1) return -1;
         }
         else
         	usleep(300000); // sleep a bit come back later, we got nothing else todo
         return (dl_artifacts->todo+ dl_artifacts->unzip_todo);
      }
       // we have something to do, find free thread
      for(i=0; i< NUMT; i++) {
         if (dl_artifacts->data[i].free==1) break;
      }

      error=start_an_artifacts_download(artifacts, dl_artifacts, i);
      if(0 != error)
         return error;
      else
      {
         dl_artifacts->next++;
         dl_artifacts->todo--;
      }
   }
   else if (dl_artifacts->unzip_todo > 0 )
   {
      debug("todo==%d, unzip_todo %d, next unzip=%d\n",dl_artifacts->unzip_todo,dl_artifacts->todo,dl_artifacts->next_unzip);
      cJSON * tmp=cJSON_GetArrayItem(artifacts, dl_artifacts->next_unzip);
      if (tmp && validate_key(tmp,"unzip_status", "pending")==0)
      { // unzip one
         if (unzip_one(tmp,dl_artifacts)==-1) return -1;
      }
      else {
        usleep(300000);
        //printf("waiting for downloads to provide a download to unzip\n");
      }
   }
   if ((dl_artifacts->todo+ dl_artifacts->unzip_todo)==0  && dl_artifacts->free_count!=NUMT) return NUMT-dl_artifacts->free_count;
   return (dl_artifacts->todo+ dl_artifacts->unzip_todo);
}
