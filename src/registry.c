 /*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */


/*! \file registry.c
 *  \brief Apllicatin api docker/ImageStream registry
 *  \author Created by Danny Goossen on 23/11/17.
 *  \copyright Copyright (c) 2017 Danny Goossen. All rights reserved.
 *  \code
 */


#include "registry.h"
#include "deployd.h"
#include "dkr_api.h"
//#include <libtar.h>
#include <bzlib.h>
#include <fcntl.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include "oc_api.h"
#include "manifest.h"
#include "libarchive_api.h"
#include "transfer_blobs.h"
#include "docker_config.h"
#include "wordexp_var.h"
#include "image_parameters.h"
#include "autolabel.h"

#define oc_fail_image_check -99
#define oc_fail_image_expansion -98
#define oc_fail_no_image_defined -97

//*****************************************************
// Internal forward functions and typedef declarations
//*****************************************************





// used by prep image push
static int process_options_registry_push(size_t argc, char * const *argv, dkr_parameter_t *parameters,dkr_parameter_t *parameters_from);

/**
 \brief internal
 \param apidata docker api data structure
 \param trace trace structure for job feedback
 \param parameters docker registry image parameters
 \return 201 on created
  \todo verify return code
 */
int push_image(struct dkr_api_data_s * apidata,struct trace_Struct * trace,const cJSON * docker_config,dkr_parameter_t * parameters,int ImageStream_v1Only);

/**
 \brief internal
 \param apidata docker api data structure
 \param trace trace structure for job feedback
 \param from_image source image name
 \param docker_config user config for docker in cJSON object
 \param manifest source
 \param ImageStream_v1Only in case we push to imagestream, if V1 only we take shortcut directly to V1 manifest push
 \param parameters docker target registry image parameters
 \return 201 on created
 \todo verify return code
 */
int push_transfer_append_manifest(struct dkr_api_data_s * apidata, struct trace_Struct * trace,const cJSON * docker_config,const char * from_image, dkr_parameter_t * parameters,cJSON ** manifest,int ImageStream_v1Only,int append );

/**
 \brief internal remote registry image tag function
 \param apidata docker api data structure
 \param trace trace structure for job feedback
 \param parameters docker registry image parameters
 \return 201 on success
 \todo verify return code
 */
static int tag_image(struct dkr_api_data_s * apidata,struct trace_Struct * trace,dkr_parameter_t * parameters);





/**
 \brief internal, set's the parameters according to the arguments
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameters docker registry image parameters
 
 */
static int process_options_tag(size_t argc, char *const*argv, dkr_parameter_t *parameters);

/**
 \brief internal, set's the parameters according to the arguments for image transfers
 \param argc argument count
 \param argv variable arguments passed from command line in script
 \param parameter_from from docker registry image parameters
 \param parameter_to to docker registry image parameters
 */
int process_options_transfer(size_t argc, char *const*argv, dkr_parameter_t *parameter_from,dkr_parameter_t *parameter_to);




/**
 \brief internal returns the digest of layer and config of a v2 manifest
 \param manifest an manifest.v2+json as input cJSON object
 \param digest_archive returns pointer to digest of the layer.tar.gz
 \param digest_config returns pointer to digest of the config
 \returns 0 on success
 */
static int get_digest_from_manifest(cJSON* manifest,char **digest_archive,char **digest_config,int * blob_size);


static int get_digest_from_config(cJSON* config,char **digest_content);
/**
 \brief internal compress,push archive to registry and returns the content and blob digest
 \returns 0 on success
 */
static int rootfs_push(struct trace_Struct * trace,dkr_parameter_t * parameters,struct dkr_api_data_s * api_data);

/**
 \brief internal push archive to registry and returns the content and blob digest
 
 \returns 0 on success
 */
static int archive_push(struct trace_Struct * trace,dkr_parameter_t * parameters,struct dkr_api_data_s * api_data);

/*
int registry_transfer_layers(void * trace, struct dkr_api_data_s * apidata, cJSON * layers,const char * digest_key_name, const char * from_image, const char * to_image  )
{
   return 0;
}
*/

//*****************************************************
// IPublic API code
//*****************************************************




int rootfs_2_archive(struct trace_Struct *trace ,const char * image_dir, const char * rootfs_dir,char digest_content[65+7],char digest_archive[65+7],int *layer_size,const char * layername)
{
	alert("start rootfs_2_archive\n");
	
	Write_dyn_trace_pad(trace, none, 60, "  + Compress rootfs ...");
	update_details(trace);
	vmkdir("%s", image_dir);
	
	layer_data_t * layer_data=init_layer_data(4*1024*1024);
	const char *path=".";
	
	struct achiveStream_data_s*as=achiveStream_data_init((char * const *)&path, 1,rootfs_dir, stream_source_rootfs, layer_data, output_mode_file, chmod_none, NULL,"%s/%s",image_dir,layername );
	
	if (as)
	{
		stream_archive(as);
		if (as->location)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			update_details(trace);
			snprintf(digest_content,SHA256_DIGEST_LENGTH_BLOBSUM,"%s",layer_data->sha256_content);
			snprintf(digest_archive,SHA256_DIGEST_LENGTH_BLOBSUM,"%s",layer_data->sha256_archive);
			if (layer_size) *layer_size=(int)layer_data->archive_len;
			if(layer_data)free_layer_data(&layer_data);
			oc_free(&as->location);
			achiveStream_data_free(&as);
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
			Write_dyn_trace(trace,magenta,"%s\n",layer_data->error_msg);
			update_details(trace);
			achiveStream_data_free(&as);
			if(layer_data)free_layer_data(&layer_data);
			return -1;
		}
		
	}
	else
	{
		Write_dyn_trace(trace, red, "[FAIL]\n");
		Write_dyn_trace(trace,magenta,"out of memory");
		update_details(trace);
		if(layer_data)free_layer_data(&layer_data);
		return -1;
	}
		
	return 0;
}


////const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
//int ImageStream_v1Only=cJSON_safe_Istrue(sys_vars,"ODAGRUN-IMAGESTRAM-v1Only");


int image_2_ISR_int(struct dkr_api_data_s * apidata,struct trace_Struct * trace,char* image,char *image_dir, int ImageStream_v1Only)
{
   dkr_parameter_t * parameters=calloc(1, sizeof(dkr_parameter_t));
   parameters->image_dir=strdup(image_dir);
	parameters->image=strdup(image);
   chop_image(image, &parameters->registry_name,&parameters->image_name, &parameters->reference);
	parameters->namespace=strdup(parameters->image_name);
	int res=0;
	{
		char * manifest_str=NULL;
		char * config_str=NULL;
		char * digest_config;
		
		cJSON * manifest=NULL;
		cJSON * config=NULL;
		
		manifest_str=read_a_file_v("%s/manifest.json",image_dir);
		if (manifest_str)
		{
			manifest=cJSON_Parse(manifest_str);
			int size=0;
			int res=get_digest_from_manifest(manifest,&parameters->layer_blob_digest,&digest_config,&size);
			parameters->layer_size=size;
			if (res)
			{
				oc_free(&manifest_str);
				cJSON_Delete(manifest);
			 // error could not read manifest
			 return -1;
			}
			else
			{
				config_str=read_a_file_v("%s/config.json",image_dir);
				config=cJSON_Parse(config_str);
				int res=get_digest_from_config(config,&parameters->layer_content_digest);
				
				if (res)
				{
					if (manifest) cJSON_Delete(manifest);
					if (config) cJSON_Delete(config);
					if(config_str) oc_free(&config_str);
					if (manifest_str) oc_free(&manifest_str);
					
					// error could not read manifest
					return -1;
				}
			}
		}
		if (manifest) cJSON_Delete(manifest);
		if (config) cJSON_Delete(config);
			if(config_str) oc_free(&config_str);
				if (manifest_str) oc_free(&manifest_str);
		// todo check all parameters needed=> avoid segfaults on testing
		debug("Start PUSH !!!\n\n");
		
		Write_dyn_trace(trace, yellow, "\n   * Uploading image ...\n");
		Write_dyn_trace_pad(trace, none, 60, "     + Uploading layer.tar.gz...");
		update_details(trace);
		if ((dkr_api_blob_check(apidata, parameters->image, parameters->layer_blob_digest ))!=200)
		{
			res=RETRY_HTTP(dkr_api_blob_push(apidata, parameters->image, parameters->layer_blob_digest,"%s/layer.tar.gz",image_dir));
			if (res==201)
			{
				Write_dyn_trace(trace, green, "  [OK]\n");
			}
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
				print_api_error(trace, res);
				update_details(trace);
			}
		}
		else // 200 on HEAD check
		{
			Write_dyn_trace(trace, yellow, "[skip]\n");
		}
	}
	update_details(trace);
	
	
	
	// need
   if (res==200 || res==201) res=push_image( apidata,trace,NULL,parameters,ImageStream_v1Only);
   clear_dkr_parameters(&parameters);
	update_details(trace);
   return res;
}

//TODO make general function for this no ISR/GLR, use options
int registry_tag_image(const cJSON * job,struct trace_Struct * trace,size_t argc,char*const*argv)
{
	int res=0;
	struct dkr_api_data_s * apidata=NULL;
	struct oc_api_data_s *oc_api_data=NULL;
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	dkr_parameter_t * parameters=calloc(1, sizeof(struct dkr_parameter_s));

	debug("registry tag image\n");
	debug("Push roofs to registry\n");
	int argscount=0;
	if ((argscount=process_options_tag(argc, argv,parameters)) < 0)
	{
		Write_dyn_trace(trace, red, "\nError processing option: ");
		Write_dyn_trace(trace, none, "%s\n",optarg);
	
		error(" *** ERRORin command line options, exit \n");
		return (-1);
	}
	if (argc > 1 && argscount <= (int)argc) {
		alert("ignoring non option arguments\n");
	}
	res=prep_registry(job, parameters, &apidata, &oc_api_data);
	if (res==-1)
	{
		if (parameters->GLR && !cJSON_get_key(env_vars,"CI_REGISTRY_IMAGE"))
		{
			Write_dyn_trace(trace, cyan, "\nIs the Gitlab internal registry enabled?\n");
		}
		else Write_dyn_trace(trace, red, "\nError command line options\n");
		update_details(trace);
		clear_dkr_parameters(&parameters);
		if (oc_api_data) oc_api_cleanup(&oc_api_data);
		return -1;
	}
	Write_dyn_trace(trace, white, "\n   From:");
	Write_dyn_trace(trace, cyan,    "%s\n\n",parameters->image_nick);
	
	res=tag_image( apidata,trace,parameters);
	
	update_details(trace);
	
	if (!res)
	{
		Write_dyn_trace(trace, white, "\n   Tagged as: ");
		Write_dyn_trace(trace, blue, "%s\n",parameters->image_nick);
	}
	
	clear_dkr_parameters(&parameters);
	dkr_api_cleanup(&apidata);
	return res;

	return 0;
}




int rootfs_push(struct trace_Struct * trace,dkr_parameter_t * parameters,struct dkr_api_data_s * api_data)
{
	char * upload_url=NULL;
	
	int res=0;
	Write_dyn_trace_pad(trace, none, 60, "     + get upload url...");
	update_details(trace);
	layer_data_t * layer_data=init_layer_data(4*1024*1024);
	char * auth=NULL;
	
	int count=0;
	do
	{
		if (count!=0) {
			Write_dyn_trace(trace, red,     "[FAIL]\n");
			print_api_error(trace, res);
			Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
			update_details(trace);
			usleep(2000000LL*count*count);
		}
		count++;
		res=dkr_api_blob_push_get_url(api_data, parameters->image, NULL, &upload_url, &auth, NULL);
	}while((res<100 || res>=500) && count<6);
	if (res<200 || res>=300 || !upload_url)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
		Write_dyn_trace(trace,magenta,"\tFailed to get blob upload url for:\n");
		Write_dyn_trace(trace,none,"\t%s\n",parameters->image);
		update_details(trace);
		if (upload_url) oc_free(&upload_url);
		upload_url=NULL;
		free_layer_data(&layer_data);
		return res;
	}
	else
		Write_dyn_trace(trace, green, "  [OK]\n");
	
	pthread_t ti;
	set_mark_dynamic_trace(trace);
	Write_dyn_trace_pad(trace, none, 38, "     + compress rootfs and push...");
	Write_dyn_trace(trace, blue, "  ");
	Write_dyn_trace_pad(trace,blue,6,"      ");
	Write_dyn_trace(trace, blue, "   ");
	Write_dyn_trace(trace, blue, "  ");
	Write_dyn_trace_pad(trace,blue,6,"      ");
	Write_dyn_trace(trace, blue, "   ");
	dyn_trace_add_decrement(trace);
	update_details(trace);
	
	char * location=NULL;
	struct achiveStream_data_s*as=achiveStream_data_init(parameters->pathname,parameters->pathname_cnt ,parameters->rootfs, stream_source_rootfs, layer_data, output_mode_curl,parameters->u2g, auth, "%s",upload_url);
	oc_free(&upload_url);
	if (as)
	{
		char formatbuf[FORMAT_SIZE_BUF];
		
		pthread_create(&ti, NULL, (void*)stream_archive,(void*)as);
		while (!as->finish)
		{
			struct timespec waittime;
			waittime.tv_sec = 10;
			waittime.tv_nsec = 0;
			nanosleep( &waittime, NULL);
			clear_till_mark_dynamic_trace(trace);
			Write_dyn_trace_pad(trace, none, 38, "     + compress rootfs and push...");
			char formatbuf[FORMAT_SIZE_BUF];
			const char * sizef=NULL;
			if (as->finalize)
			{   // patching done, finalize with POST
				debug("Finalize\n");
				sizef=format_size(formatbuf, (uint64_t)as->layer_data->content_len);
				Write_dyn_trace(trace, cyan, " (");
				Write_dyn_trace_pad(trace,cyan,6,"%s",sizef);
				Write_dyn_trace(trace, cyan, ")  ");
				sizef=format_size(formatbuf, (uint64_t)as->layer_data->archive_len);
				Write_dyn_trace(trace, cyan, " [");
				Write_dyn_trace_pad(trace,cyan,6,"%s",sizef);
				Write_dyn_trace(trace, cyan, "]  ");
			}
			else
			{   // still patching
					debug("as->layer_data->archive_len==0\n");
					sizef=format_size(formatbuf, (uint64_t)as->layer_data->content_len);
					Write_dyn_trace(trace, blue, " (");
					Write_dyn_trace_pad(trace,blue,6,"%s",sizef);
					Write_dyn_trace(trace, blue, ")  ");
					debug("as->layer_data->archive_len=%zd\n",as->layer_data->archive_len);
					sizef=format_size(formatbuf, (uint64_t)as->layer_data->archive_len);
					Write_dyn_trace(trace, blue, " [");
					Write_dyn_trace_pad(trace,blue,6,"%s",sizef);
					Write_dyn_trace(trace, blue, "]  ");
			}
			dyn_trace_add_decrement(trace);
			update_details(trace);
		}
		pthread_join(ti, NULL);
		
		if (!as->location)
		{
			clear_till_mark_dynamic_trace(trace);
			Write_dyn_trace_pad(trace, none, 38, "     + compress rootfs and push...");
			Write_dyn_trace(trace, blue, " (");
			Write_dyn_trace_pad(trace,blue,6," error");
			Write_dyn_trace(trace, blue, ")  ");
			Write_dyn_trace(trace, blue, " [");
			Write_dyn_trace_pad(trace,blue,6," error");
			Write_dyn_trace(trace, blue, "]  ");
			Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
			Write_dyn_trace(trace,magenta,"%s\n",layer_data->error_msg);
			update_details(trace);
			oc_free(&as->location);
			free_layer_data(&layer_data);
			achiveStream_data_free(&as);
			return res;
		}
		else
		{
			const char * sizef=NULL;
			clear_till_mark_dynamic_trace(trace);
			Write_dyn_trace_pad(trace, none, 38, "     + compress rootfs and push...");
			sizef=format_size(formatbuf, (uint64_t)as->layer_data->content_len);
			Write_dyn_trace(trace, yellow, " (");
			Write_dyn_trace_pad(trace,yellow,6,"%s",sizef);
			Write_dyn_trace(trace, yellow, ")  ");
			sizef=format_size(formatbuf, (uint64_t)as->layer_data->archive_len);
			Write_dyn_trace(trace, yellow, " [");
			Write_dyn_trace_pad(trace,yellow,6,"%s",sizef);
			Write_dyn_trace(trace, yellow, "]  ");
			Write_dyn_trace(trace, green, "  [OK]\n");
			update_details(trace);
		}
		oc_free(&location);
		achiveStream_data_free(&as);

	}
	else
	{
		Write_dyn_trace(trace, red, "                      [FAIL]\n"); // fail layer
		Write_dyn_trace(trace,magenta,"out of memory\n");
		update_details(trace);
		free_layer_data(&layer_data);
		return res;
	}
	update_details(trace);
	
	// don't free layer_data, otherwise no parameters->layer_*_digest
	parameters->layer_blob_digest=strdup(layer_data->sha256_archive);
	parameters->layer_content_digest=strdup(layer_data->sha256_content);
	parameters->layer_size=layer_data->archive_len;
	free_layer_data(&layer_data);
	return 0;
}



int archive_push(struct trace_Struct * trace,dkr_parameter_t * parameters,struct dkr_api_data_s * api_data)
{
	char * upload_url=NULL;
	
	int res=0;
	Write_dyn_trace_pad(trace, none, 60, "     + read,process and patch archive to registry");
	update_details(trace);
	layer_data_t * layer_data=init_layer_data(4*1024*1024);
	char * auth=NULL;
	
	int count=0;
	do
	{
		if (count!=0) {
			Write_dyn_trace(trace, red,     "               [FAIL]\n");
			print_api_error(trace, res);
			Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
			update_details(trace);
			usleep(2000000LL*count*count);
		}
		count++;
		res=dkr_api_blob_push_get_url(api_data, parameters->image, NULL, &upload_url, &auth, NULL);
	}while((res<100 || res>=500) && count<6);
	if (res<200 || res>=300 || !upload_url)
	{
		Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
		Write_dyn_trace(trace,magenta,"\tFailed to get blob upload url for:\n");
		Write_dyn_trace(trace,none,"\t%s\n",parameters->image);
		update_details(trace);
		if (upload_url) oc_free(&upload_url);
		upload_url=NULL;
		free_layer_data(&layer_data);
		return res;
	}
	
	
    debug("\n   archive=%s\n",parameters->archive);
	
	char * location=NULL;
	struct achiveStream_data_s*as=achiveStream_data_init(&parameters->archive,1, NULL, stream_source_archive, layer_data, output_mode_curl,chmod_none, auth, "%s",upload_url);
	oc_free(&upload_url);
	if (as)
	{
		stream_archive(as);
		if (!as->location)
		{
			Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
			Write_dyn_trace(trace,magenta,"%s\n",layer_data->error_msg);
			update_details(trace);
			free_layer_data(&layer_data);
			achiveStream_data_free(&as);
			return res;
		}
		else
			Write_dyn_trace(trace, green, "  [OK]\n");
		debug("Location: %s\n",location);
		oc_free(&as->location);
		achiveStream_data_free(&as);
	}
	else
	{
		Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
		Write_dyn_trace(trace,magenta,"out of memory\n");
		update_details(trace);
		free_layer_data(&layer_data);
		return res;
	}
	update_details(trace);
	// don't free layer_data, otherwise no parameters->layer_*_digest
		
	parameters->layer_blob_digest=strdup(layer_data->sha256_archive);
	parameters->layer_content_digest=strdup(layer_data->sha256_content);
	parameters->layer_size=layer_data->archive_len;
	debug("   blob_digest=%s\n",parameters->layer_blob_digest);
	debug("content_digest=%s\n\n",parameters->layer_content_digest);
	
	free_layer_data(&layer_data);
	return 0;
}

int int_registry_push(const cJSON * job,struct trace_Struct * trace,const char * rootfs,char * const *pathnames,size_t pathname_cnt,const char *image,const char * from_image, const cJSON*environment)
{
	int res=0;
	struct dkr_api_data_s * apidata=NULL;
	struct oc_api_data_s *oc_api_data=NULL;
	dkr_parameter_t * parameters=calloc(1, sizeof(struct dkr_parameter_s));
	dkr_parameter_t * parameters_from=calloc(1, sizeof(struct dkr_parameter_s));

	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	int ImageStream_v1Only=cJSON_safe_IsTrue(sys_vars,"ODAGRUN-IMAGESTRAM-v1Only");
	debug("int_registry_push, ImageStream_v1Only=%d\n",ImageStream_v1Only);
	if(from_image)parameters_from->image=strdup(from_image);
	if (image)parameters->image=strdup(image);
	if (rootfs)parameters->rootfs=strdup(rootfs);
	if (pathnames && *pathnames && pathname_cnt)
	{
		parameters->pathname=pathnames;
		parameters->pathname_cnt=pathname_cnt;
	}
	else
	{
		parameters->pathname=NULL;
		parameters->pathname_cnt=0;
	}
	parameters->u2g=1;
	parameters->reference=strdup("latest");
	parameters->image_name=strdup("odagrun-internal");
	res=prep_registry(job, parameters, &apidata, &oc_api_data);
	
	if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		update_details(trace);
		clear_dkr_parameters(&parameters);
		clear_dkr_parameters(&parameters_from);
		if (oc_api_data) oc_api_cleanup(&oc_api_data);
		return -1;
	}
	if (parameters_from->image || parameters_from->ISR | parameters_from->GLR)
	{
		res=prep_registry(job, parameters_from, &apidata, &oc_api_data);
		
		if (res==-1)
		{
			Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
			update_details(trace);
			clear_dkr_parameters(&parameters);
			clear_dkr_parameters(&parameters_from);
			if (oc_api_data) oc_api_cleanup(&oc_api_data);
			return -1;
		}
	}
	/* not for int push, we need to have rootfs = NULL on WorkSpaces Environment!!!!!
	else if (!parameters->archive && !parameters->rootfs)
	{  // default source if no other was defined
		if (parameters->pathname_cnt ==0)
		{
			int aspr=asprintf(&parameters->rootfs,"rootfs");
		}
		
		
	}
	 */
	if (!res && parameters->ISR )
	{
		if (oc_api_data)
		{
			const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
			const char * OKD_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
			const char * oc_image_name=parameters->namespace+strlen(OKD_NAMESPACE)+1;
			res=oc_api(oc_api_data, "GET", NULL, NULL, "/oapi/v1/namespaces/%s/imagestreams/%s",OKD_NAMESPACE,oc_image_name);
			debug("result imagestream GET %d ",res);
			if (res==404)
			{
				cJSON * imagestream=make_imagestream_template( OKD_NAMESPACE,oc_image_name);
				if (imagestream)
				{
					res=oc_api(oc_api_data, "POST", imagestream, NULL, "/oapi/v1/namespaces/%s/imagestreams",OKD_NAMESPACE);
					debug("result imagestream POST %d ",res);
				}
			}
			
		}
	}
	cJSON * manifest=NULL;
	cJSON * docker_config=NULL;
	
	if (environment)
	{
		debug("int push, WP Environment:\n");
		print_json(environment);
		docker_config=cJSON_CreateObject();
		if (docker_config)
		{
			cJSON * env=cJSON_CreateArray();
			if (env)
			{
				cJSON * this_environment=NULL;
				cJSON_AddItemToObject(docker_config,"Env",env);
				if (cJSON_IsString(environment))
				{
					debug("WP:environment is String\n");
					const char * str=environment->valuestring;
					if (str)
					{
						if (strchr(str, ',')!=NULL)
						{ // comma separated list
							debug("WP:environment is a comma separated list\n");
							this_environment=comma_sep_list_2_json_array(str);
						}
						else
						{
							this_environment=cJSON_CreateArray();
							cJSON_add_Array_string(this_environment, str);
						}
					}
				}
				else if(cJSON_IsArray(environment))
				{
					this_environment=cJSON_Duplicate(environment, 1);
				}
				if(this_environment)
				{
					debug("WP:environment Processed\n");
					print_json(this_environment);
					cJSON * item=NULL;
					cJSON_ArrayForEach(item, this_environment)
					{
						debug("item: %s\n",item->valuestring);
						const char * ev=cJSON_get_key(env_vars ,item->valuestring);
						if (ev)
						{
							debug("got value for '%s' = '%s'\n",item->valuestring,ev);
							cJSON_add_Array_string_v(env, "%s=%s",item->valuestring,ev);
						}
					}
					cJSON_Delete(this_environment);
					this_environment=NULL;
				}
				else
					debug("WP:environment processed == NULL\n");
			}
		}
		debug("int push, WP created docker_config:\n");
		print_json(docker_config);
	}
	if (parameters_from->image)
	{
		Write_dyn_trace_pad(trace, yellow, 45,"   * getting manifest");
		update_details(trace);
		
		res=RETRY_HTTP(dkr_api_get_manifest(apidata,"application/vnd.docker.distribution.manifest.v2+json" ,parameters_from->image, &manifest));
		
		int schemaVerson=0;
		if (res==200)
		{
			
			// todo check v2!!!!!!!!
			
			if (manifest)
			{
				cJSON * schema=cJSON_GetObjectItem(manifest, "schemaVersion");
				if (schema && cJSON_IsNumber(schema))
				{
					schemaVerson=schema->valueint;
					res=0;
					Write_dyn_trace(trace, green,"                 [V%d]\n",schemaVerson);
				}
				else
					Write_dyn_trace(trace, red, "   [invalid Manifest]\n");
			}
		}
		else
		{
			Write_dyn_trace(trace, red,     "               [FAIL]\n");
			print_api_error(trace, res);
		}
	}
	
	//if (!res)Write_dyn_trace(trace, yellow, "   * copy layers\n");
	update_details(trace);
	
	// here, registries initialized and default source set, if rootfs, set content if not defined and push!!
	if ( !parameters->archive && (parameters->rootfs || parameters->pathname_cnt>0))
	{
		debug("rootfs_push\n");
		res= rootfs_push(trace, parameters,apidata);
	}
	else if (!parameters->rootfs && parameters->pathname_cnt==0 && parameters->archive)
	{
		res= archive_push(trace, parameters,apidata);
		
	}

	// need to figure out when and how only environment!
	if ( !res)
	{
		alert("\n\n--------------------------------------------------------\n   -- check to sent ws_dummy layer --\n\n");
		if (!parameters->archive && !parameters->rootfs && parameters->pathname_cnt==0 && !parameters_from->image)
		{
			char wp_empty[97]= {0x1F, 0x8B, 0x08, 0x00, 0x08, 0x80, 0x7A, 0x5B, 0x00, 0x03, 0xED, 0xCF, 0xB9, 0x0D, 0x80, 0x30, 0x0C, 0x05, 0xD0, 0x8C,
				                0xC2, 0x04, 0x1C, 0x4A, 0x42, 0xE6, 0x61, 0x05, 0x8E, 0xFD, 0xB1, 0x52, 0xD2, 0xA2, 0x74, 0xEF, 0x35, 0xDF, 0x72, 0xE1,
				                0x63, 0x5E, 0xD2, 0x70, 0x6B, 0x68, 0xAD, 0xF6, 0x0C, 0xDF, 0xEC, 0xF5, 0x96, 0x73, 0xDE, 0x4B, 0xAB, 0x79, 0x2B, 0xD1,
				                0x6F, 0x25, 0x62, 0xAA, 0xE3, 0x4F, 0x4B, 0xE9, 0xB9, 0xEE, 0xE3, 0x8C, 0x95, 0x7F, 0xE7, 0x7C, 0x9F, 0x03, 0x00, 0x00,
								0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x81, 0x5E, 0x39, 0x8D, 0xF3, 0x0D, 0x00, 0x28, 0x00, 0x00 };
			
			alert("****** Sending ws_dummy layer 97 bytes\n");
			res=RETRY_HTTP(dkr_api_blob_push_raw(apidata, parameters->image, "sha256:8f6b2676fc828eb70ec7b5768fa87c6a2beea6e7694a5d6947fedafae5580562", wp_empty,97));
			
			if (res>=200 && res<300)
			{
				parameters->layer_blob_digest=strdup("sha256:8f6b2676fc828eb70ec7b5768fa87c6a2beea6e7694a5d6947fedafae5580562");
				parameters->layer_content_digest=strdup("sha256:c849f80a14f67551579a9542ca8c457526921b91827b870f256f796422fb0370");
				parameters->layer_size=97;
				res=0;
			}
		}
		
	}
	
	
	// now transfer blobs if from_image
	if (!res && parameters_from->image)
		res=transfer_blobs(manifest, parameters_from->image,parameters->image, apidata, trace);
	parameters->info_command=strdup("odagrun internal");
	// now depending on the options we create / modify manifest and push image
	if (!res)
	{
		if (!parameters_from->image && (parameters->rootfs || parameters->archive || parameters->pathname_cnt>0 || (parameters->layer_blob_digest && parameters->layer_content_digest) ))
		{
			res=push_image(apidata, trace,docker_config, parameters, ImageStream_v1Only);
		}
		else if (parameters_from->image && !parameters->rootfs && !parameters->archive && parameters->pathname_cnt==0)
		{
			res=push_transfer_append_manifest(apidata, trace,docker_config, parameters_from->image, parameters, &manifest, ImageStream_v1Only,0);
		}
		else if(parameters_from->image && (parameters->rootfs || parameters->archive || parameters->pathname_cnt>0))
		{
			res=push_transfer_append_manifest(apidata, trace,docker_config, parameters_from->image, parameters, &manifest, ImageStream_v1Only,1);
		}
	}
	update_details(trace);
	if (docker_config) cJSON_Delete(docker_config);
	docker_config=NULL;
	if (manifest) cJSON_Delete(manifest);
	manifest=NULL;
	if (oc_api_data) oc_api_cleanup(&oc_api_data);
	clear_dkr_parameters(&parameters);
	clear_dkr_parameters(&parameters_from);
	dkr_api_cleanup(&apidata);
	if (oc_api_data) oc_api_cleanup(&oc_api_data);
	debug("finish push registry\n");
	return res;
}

int bootstrap_expander_registry_push(word_exp_var_t * wev_main, const char* valuestring ,const cJSON * env_vars,const char * dir)
{
	int res=0;
	word_exp_var_t wev;
	memset(&wev, 0, sizeof(word_exp_var_t));
	res=word_exp_var_dyn(&wev, valuestring, env_vars, dir);
	if (!res)
	{
		int i=0;
		for (i= 0;i<wev.argc;i++)
		{
			debug("all args pass 1: %s\n",wev.argv[i]);
		}
		int argscount=0;
		dkr_parameter_t * parameters=calloc(1, sizeof(struct dkr_parameter_s));
		dkr_parameter_t * parameters_from=calloc(1, sizeof(struct dkr_parameter_s));
		if ((argscount=process_options_registry_push(wev.argc,wev.argv,parameters,parameters_from)) < 0)
		{
			if (!wev_main->errmsg) wev_main->errmsg=create_stringf("Error PreProcessing command line options\n");
			res=-1;
		}
		else
		{
			
			if (parameters->rootfs && argscount<wev.argc)
			{
				// when we got rootfs and non option arguments:
				word_exp_var_t wev_nonopt;
				memset(&wev_nonopt, 0, sizeof(word_exp_var_t));
				// now reexpand in new directory, number of options should not change, only non-option arguments
				
				res=word_exp_var_dyn(&wev_nonopt, valuestring, env_vars, parameters->rootfs);
				if(!res)
				{
					
					//now construct a new wev_main->args_obj
					// 0..argscount-1 => use wev (the non options)
					// argscount..wev_nonopt.argc-1 => use wev_nonopt
					wev_main->args_obj=cJSON_CreateStringArray((const char**)wev.argv, argscount);
					debug("obj from PASS1:\n");
					print_json(wev_main->args_obj);
					//print_json(wev_main->args_obj);
					
					int i= 0;
					
					for (i=argscount;i<wev_nonopt.argc;i++)
					{
						debug("nonoption args pass2: %s\n",wev_nonopt.argv[i]);
						cJSON_AddItemToArray(wev_main->args_obj, cJSON_CreateString(wev_nonopt.argv[i]));
					}
					print_json(wev_main->args_obj);
				}
				
			}
			else
			{
				wev_main->args_obj=cJSON_CreateStringArray((const char**)wev.argv, wev.argc);
				print_json(wev_main->args_obj);
			}
			// create wev_main.argv from wev_main->args_obj
			if(!res && wev_main->args_obj)
			wev_main->argc=cJSON_GetArraySize(wev_main->args_obj);
			wev_main->argv=calloc(wev_main->argc,sizeof(char*));
			char**walk=(char**)wev_main->argv;
			cJSON*item=NULL;
			cJSON_ArrayForEach(item, wev_main->args_obj)
			{
				*walk=item->valuestring;
				walk++;
			}
		}
		clear_dkr_parameters(&parameters);
		clear_dkr_parameters(&parameters_from);
	}
	word_exp_var_free(&wev);
	return res;
}

void construct_info_command(dkr_parameter_t * parameters, const char * from,int append,const char *from_sha);

void construct_info_command(dkr_parameter_t * parameters, const char * from,int append, const char *from_sha)
{
	
	char * info_dockerconfig=NULL;
	char * info_append=NULL;
	char * info_from=NULL;
	if (parameters->docker_config)
		info_dockerconfig=create_stringf( " CONFIG #%.*s",16,parameters->docker_config_digest);
	else
		info_dockerconfig=create_stringf( "%s","");
	
	if (append)
	{
		if (parameters->rootfs) info_append=create_stringf( " ADD file:%s in /",parameters->layer_content_digest+7);
		else if (parameters->archive) info_append=create_stringf( " ADD archive:%s in /",parameters->layer_content_digest+7);
		else  info_append=create_stringf( " ADD file:%s in /",parameters->layer_content_digest+7);
	}
	else
		info_append=create_stringf( "%s","");
	
	if ( from)
		info_from=create_stringf( " FROM %s (@%s)", from,from_sha);
	else
		info_from=create_stringf( "%s","");
	
	parameters->info_command=create_stringf( "registry_push%s%s%s",info_from,info_dockerconfig,info_append);
	
}


int registry_push(const cJSON * job,struct trace_Struct * trace,size_t argc,char * const *argv)
{
	int res=0;
	struct dkr_api_data_s * apidata=NULL;
	struct oc_api_data_s *oc_api_data=NULL;
	dkr_parameter_t * parameters=calloc(1, sizeof(struct dkr_parameter_s));
	dkr_parameter_t * parameters_from=calloc(1, sizeof(struct dkr_parameter_s));
    const cJSON * env_vars=cJSON_GetObjectItem(job ,"env_vars");
	const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
	int ImageStream_v1Only=cJSON_safe_IsTrue(sys_vars,"ODAGRUN-IMAGESTRAM-v1Only");
	int argscount=0;
	if ((argscount=process_options_registry_push(argc, argv,parameters,parameters_from)) < 0)
	{
		error(" *** ERRORin command line options, exit \n");
		Write_dyn_trace(trace, red, "\nError processing option: ");
		Write_dyn_trace(trace,none, "%s\n",optarg);
		update_details(trace);
		clear_dkr_parameters(&parameters);
		if (oc_api_data) oc_api_cleanup(&oc_api_data);
		return -1;
	}
	if (argc > 1 && argscount < (int)argc) {
		parameters->pathname= &argv[argscount];
		parameters->pathname_cnt=argc-(size_t)argscount;
	}
	else
	{
		parameters->pathname= NULL;
		parameters->pathname_cnt=0;

	}
	if (parameters->image)
	{
		if (strchr(parameters->image, '@')!=NULL)
		{
			Write_dyn_trace(trace, red, "\nError <repo>@<DIGEST> not supported\n");
			update_details(trace);
			clear_dkr_parameters(&parameters);
			clear_dkr_parameters(&parameters_from);
			if (oc_api_data) oc_api_cleanup(&oc_api_data);
			return -1;
		}
	}
	
	res=prep_registry(job, parameters, &apidata, &oc_api_data);
	if (res==-1)
	{
		Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
		if (parameters->GLR && !cJSON_get_key(env_vars,"CI_REGISTRY_IMAGE"))
		{
			Write_dyn_trace(trace, cyan, "\nIs the Gitlab internal registry enabled?\n");
		}
		update_details(trace);
		clear_dkr_parameters(&parameters);
		clear_dkr_parameters(&parameters_from);
		if (oc_api_data) oc_api_cleanup(&oc_api_data);
		return -1;
	}
	if (parameters_from->image && strchr(parameters_from->image, '@')!=NULL)
	{
		Write_dyn_trace(trace, red, "\nError <repo>@<DIGEST> not supported YET\n");
		update_details(trace);
		clear_dkr_parameters(&parameters);
		clear_dkr_parameters(&parameters_from);
		if (oc_api_data) oc_api_cleanup(&oc_api_data);
		return -1;
	}
		if (parameters_from->image || parameters_from->ISR || parameters_from->GLR)
	{
		
		  res=prep_registry(job, parameters_from, &apidata, &oc_api_data);
		
		if (res==-1)
		{
			Write_dyn_trace(trace, red, "\nError preparing registry settings\n");
			if (parameters->GLR && !cJSON_get_key(env_vars,"CI_REGISTRY_IMAGE"))
			{
				Write_dyn_trace(trace, cyan, "\nIs the Gitlab internal registry enabled?\n");
			}

			update_details(trace);
			clear_dkr_parameters(&parameters);
			clear_dkr_parameters(&parameters_from);
			if (oc_api_data) oc_api_cleanup(&oc_api_data);
			return -1;
		}
	}
	else if (!parameters->archive && !parameters->rootfs)
	{  // default source if no other was defined
		if (parameters->pathname_cnt ==0)
		{
			parameters->rootfs=create_stringf("rootfs");
		}
		
		
	}
	if (!res && parameters->ISR )
	{
		if (oc_api_data)
		{
			const cJSON * sys_vars=cJSON_GetObjectItem(job ,"sys_vars");
			const char * OKD_NAMESPACE=cJSON_get_key(sys_vars,"ODAGRUN-OC-NAMESPACE");
			const char * oc_image_name=parameters->namespace+strlen(OKD_NAMESPACE)+1;
			res=oc_api(oc_api_data, "GET", NULL, NULL, "/oapi/v1/namespaces/%s/imagestreams/%s",OKD_NAMESPACE,oc_image_name);
			debug("result imagestream GET %d ",res);
			if (res==404)
			{
				cJSON * imagestream=make_imagestream_template( OKD_NAMESPACE,oc_image_name);
				if (imagestream)
				{
					res=oc_api(oc_api_data, "POST", imagestream, NULL, "/oapi/v1/namespaces/%s/imagestreams",OKD_NAMESPACE);
					debug("result imagestream POST %d ",res);
				}
			}
		}
	}
	cJSON * docker_config=NULL;
	Write_dyn_trace(trace,none,"\n");
	if (!res && parameters->docker_config)
	{
		cJSON * tmp=yaml_file_2_cJSON_env(NULL, cJSON_GetObjectItem(job, "env_vars"), parameters->docker_config);
		if (tmp)
		{
			docker_config=process_docker_config(cJSON_GetArrayItem(tmp, 0));
			char * docker_config_str=cJSON_Print(docker_config);
			char output[SHA256_DIGEST_LENGTH_HASH];
			if (docker_config_str)
			{
				calc_sha256_digest_hash (docker_config_str,0, output);
				parameters->docker_config_digest=strdup(output);
				oc_free(&docker_config_str);
			}
			cJSON_Delete(tmp);
			tmp=NULL;
			if (docker_config)
			{
			Write_dyn_trace(trace,none,"     with  config: ");
			Write_dyn_trace(trace, cyan, "\'%s\'\n",parameters->docker_config,output);
				
			}
			else
			{
				Write_dyn_trace(trace,magenta,"     Error processing docker config file:\n         ");
				Write_dyn_trace(trace, cyan, " \'%s\' \n",parameters->docker_config);
				res=-1;
			}
			
		}
		else
		{
			Write_dyn_trace(trace,magenta,"     Error parsing / or opening docker config file:\n         ");
			Write_dyn_trace(trace, cyan, " \'%s\' \n",parameters->docker_config);
			res=-1;
		}
		update_details(trace);
	}
	debug("user docker config\n");
	print_json(docker_config);
	
	
	
	
	if (parameters_from->image)
	{
		Write_dyn_trace(trace,none,"     from   Image: ");
		Write_dyn_trace(trace, cyan, " %s \n",parameters_from->image_nick);
	}
	if (parameters->archive)
	{
		Write_dyn_trace(trace,none,"     from Archive: ");
		Write_dyn_trace(trace, cyan, "%s \n",parameters->archive);
	}
	else if(parameters->rootfs)
	{
		Write_dyn_trace(trace,none,"     from  RootFS: ");
		Write_dyn_trace(trace, cyan, " %s \n",parameters->rootfs);
	}
	Write_dyn_trace(trace,none,"\n");
	update_details(trace);
	cJSON * manifest=NULL;
	
	if (parameters_from->image)
	{
			alert("image sha: %s\n",parameters_from->image_sha);
			Write_dyn_trace_pad(trace, yellow, 45,"   * getting manifest");
			update_details(trace);
			int count=0;
			do
			{
				if (count!=0) {
					Write_dyn_trace(trace, red,     "               [FAIL]\n");
					print_api_error(trace, res);
					Write_dyn_trace_pad(trace, yellow, 45,"     -- RETRY");
					update_details(trace);
					usleep(2000000LL*count*count);
				}
				count++;
			res=dkr_api_get_manifest(apidata,"application/vnd.docker.distribution.manifest.v2+json" ,parameters_from->image, &manifest);
			}while((res<100 || res>=500) && count<6);
			int schemaVerson=0;
			if (res==200)
			{
				
				// todo check v2!!!!!!!!
				
				if (manifest)
				{
					cJSON * schema=cJSON_GetObjectItem(manifest, "schemaVersion");
					if (schema && cJSON_IsNumber(schema))
					{
						schemaVerson=schema->valueint;
						res=0;
						Write_dyn_trace(trace, green,"                 [V%d]\n",schemaVerson);
					}
					else
						Write_dyn_trace(trace, red, "   [invalid Manifest]\n");
				}
			}
			else
			{
				Write_dyn_trace(trace, red,     "               [FAIL]\n");
				print_api_error(trace, res);
			}
	}
	
	if (!res)Write_dyn_trace(trace, yellow, "   * copy layers\n");
	update_details(trace);
		
	
	// here, registries initialized and default source set, if rootfs, set content if not defined and push!!
	if ( !parameters->archive && (parameters->rootfs || parameters->pathname_cnt))
	{
		res= rootfs_push(trace, parameters,apidata);
	}
	else if (!parameters->rootfs && parameters->pathname_cnt==0 && parameters->archive)
	{
		res= archive_push(trace, parameters,apidata);
		
	}
	// now transfer blobs if from_image
	if (!res && parameters_from->image)
	{
		alert("\n\n--------------------------------------------------------\n   -- check to sent empty layer --\n\n");
		if (!parameters->rootfs && !parameters->archive && parameters->pathname_cnt==0)
		{
			
			// sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4
			char empty[32]={ 0x1F, 0x8B, 0x08, 0x00, 0x00, 0x09, 0x6E, 0x88,
				0x00, 0xFF, 0x62, 0x18, 0x05, 0xA3, 0x60, 0x14,
				0x8C, 0x58, 0x00, 0x08, 0x00, 0x00, 0xFF, 0xFF,
				0x2E, 0xAF, 0xB5, 0xEF, 0x00, 0x04, 0x00, 0x00};
			alert("****** Sending first empty layer 32 bytes\n");
			res=RETRY_HTTP(dkr_api_blob_push_raw(apidata, parameters->image, "sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4", empty,32));
			if (res>=200 && res<300)
				res=transfer_blobs(manifest, parameters_from->image,parameters->image, apidata, trace);
			
		}
		else
			res=transfer_blobs(manifest, parameters_from->image,parameters->image, apidata, trace);
	}
	if (!res)
	{
	if (parameters->rootfs || parameters->archive || parameters->pathname_cnt || docker_config)
		// full autolabel on creating a layer (rootfs or archive)
		autolabel(&docker_config ,job,parameters_from);
	else if(!parameters->skip_auto && (parameters_from->image && !parameters->rootfs && !parameters->archive && parameters->pathname_cnt==0))
	{
		autolabel_limited(&docker_config,parameters_from);
	}
		else
		
		debug("no auto label\n");
	}
	// now depending on the options we create / modify manifest and push image
	if (!res) 
	{
		if (!parameters_from->image && (parameters->rootfs || parameters->archive || parameters->pathname_cnt>0))
		{
			construct_info_command(parameters, NULL, 1,NULL);
			res=push_image(apidata, trace, docker_config,parameters, ImageStream_v1Only);
			
		}
		else if (parameters_from->image && !parameters->rootfs && !parameters->archive && parameters->pathname_cnt==0)
		{ // it's a FROM
			if ( docker_config) construct_info_command(parameters, parameters_from->image_nick, 0,parameters_from->image_sha);
			res=push_transfer_append_manifest(apidata, trace,docker_config, parameters_from->image, parameters, &manifest, ImageStream_v1Only,0);
		}
		else if(parameters_from->image && (parameters->rootfs || parameters->archive || parameters->pathname_cnt>0))
		{
			construct_info_command(parameters, parameters_from->image_nick, 1,parameters_from->image_sha);
			res=push_transfer_append_manifest(apidata, trace,docker_config, parameters_from->image, parameters, &manifest, ImageStream_v1Only,1);
		}
		if (!res)
		{
			Write_dyn_trace(trace, white, "\n   Image: ");
			Write_dyn_trace(trace, blue, "%s\n",parameters->image_nick);
			if (!parameters->ISR && parameters->GLR)
			{
				Write_dyn_trace(trace, cyan, "   (%s)\n",parameters->image);
			}
			
		}
	}
	if (parameters_from) clear_dkr_parameters(&parameters_from);
	update_details(trace);
	if (docker_config) cJSON_Delete(docker_config);
	docker_config=NULL;
	if (manifest) cJSON_Delete(manifest);
	manifest=NULL;
	update_details(trace);
	if (oc_api_data) oc_api_cleanup(&oc_api_data);
	clear_dkr_parameters(&parameters);
	dkr_api_cleanup(&apidata);
	if (oc_api_data) oc_api_cleanup(&oc_api_data);

	return res;
}


//*****************************************************
// Internal Functions code
//*****************************************************

int push_transfer_append_manifest(struct dkr_api_data_s * apidata, struct trace_Struct * trace, const cJSON * docker_config,const char * image, dkr_parameter_t * parameters,cJSON ** manifest,int ImageStream_v1Only , int append)
{
	int res=0;
	int schemaVerson=0;
	char  digest_config[65+7];
	
	cJSON * config=NULL;
	cJSON * manifest_config=NULL;
	cJSON * manifest_layers=NULL;
	const char * blob=NULL;
	
	if (!manifest || !*manifest) return -1;
	debug("push_transfer_append_manifest\n");
	// we got an image, no errors yet and image is not scratch
	if (*manifest)
	{
		
		cJSON * schema=cJSON_GetObjectItem(*manifest, "schemaVersion");
		if (schema && cJSON_IsNumber(schema))
		{
			schemaVerson=schema->valueint;
			debug("Got manifest v%d\n",schemaVerson);
			//print_json(*manifest);
			res=0;
		}
		if (res || schemaVerson<1 || schemaVerson>2)
		{
			debug("Invalid manifest Manifest\n");
			Write_dyn_trace(trace, red, "ERROR:\n");
			Write_dyn_trace(trace, magenta, "\nInvalid manifest\n");
			res=-1;
		}
	}
	else
	{
		res=-1;
		debug("No Manifest\n");
		Write_dyn_trace(trace, red, "ERROR:\n");
		Write_dyn_trace(trace, magenta, "\nShould have manifest but have not\n");
	}
	if ( res==0 && schemaVerson==2 && *manifest && (!parameters->ISR || (parameters->ISR && ImageStream_v1Only==0)))
	{
		debug("schemaVerson==2\n");
		Write_dyn_trace_pad(trace, yellow,60, "   * Get config blob");
		update_details(trace);
		manifest_config=cJSON_GetObjectItem(*manifest, "config");
		if (manifest_config) blob=cJSON_get_key(manifest_config, "digest");
		if (blob)
		{
			char  *config_str=NULL;
			res=dkr_api_blob_get(apidata, image, blob, &config_str);
			debug ("request config, res=%d\n%s\n",res,config_str);
			if (res==200)
			{
				config=cJSON_Parse(config_str);
				if (config)
				{
					Write_dyn_trace(trace, green, "  [OK]\n");
					res=0;
				}
				else
				{
					Write_dyn_trace(trace, red, "[FAIL]\n");
					Write_dyn_trace(trace, magenta, "\nFailed to parse config blob\n");
					debug("failure parsing config\n");
					res=-1;
				}
			}
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				print_api_error(trace, res);
			}
			if (config_str) oc_free(&config_str);
			config_str=NULL;
		}
		else
		{
			res=-1;
			debug("\nFailed to extract config blob from manifest\n");
			Write_dyn_trace(trace, red, "[FAIL]\n");
			Write_dyn_trace(trace, magenta, "\nFailed to extract config blob from manifest\n");
		}
		
		update_details(trace);
	}
	
	// got config v2, prepare add exec layer etc... and clean
	
	if (!res && config && schemaVerson==2)
	{
		debug("# got config and manifest v2, modify config\n");
		//print_json(config);
		cJSON * rootfs=cJSON_GetObjectItem(config, "rootfs");
		cJSON * diff_ids=cJSON_GetObjectItem(rootfs,"diff_ids");
		cJSON * Image_digest=NULL;
		
		if (append) cJSON_AddItemToArray(diff_ids, cJSON_CreateString(parameters->layer_content_digest));
		// push config.yml blob, but remove container
		
		cJSON * config_config=cJSON_GetObjectItem(config, "config");
		if (append) Image_digest=cJSON_DetachItemFromObject(config_config,"Image");
		if (docker_config)
		{
			cJSON * tmp=process_docker_config(config_config);
			if (tmp)
			{
				cJSON_ReplaceItemInObject(config, "config", tmp);
				config_config=tmp;
				debug("merge image config with docker config\n");
				docker_merge_config(&config_config, docker_config);
				if (autolabel_need_v1(config,*manifest ,image))
				{
					Write_dyn_trace_pad(trace, yellow,60, "   * Autolabel need v1 manifest...");
					update_details(trace);
					cJSON* manifestv1=NULL;
					int resv1=dkr_api_get_manifest(apidata, "application/vnd.docker.distribution.manifest.v1+json", image, &manifestv1);
					if (resv1==200 )
					{
						if (manifestv1)
						{
							Write_dyn_trace(trace, green, "  [OK]\n");
							autolabel_update(config,manifestv1 ,image);
							
						}
						else
						{
							Write_dyn_trace(trace, yellow, "[FAIL]\n");
							autolabel_update(config,*manifest ,image);
						}
					}
					else
					{
						Write_dyn_trace(trace, yellow, "[FAIL]\n");
						debug("failure getting v1 ,manifest\n");
						autolabel_update(config,*manifest ,image);
					}
					if (manifestv1) cJSON_Delete(manifestv1);
					update_details(trace);
				}
				else
					autolabel_update(config,*manifest ,image);
				
			}
		}
		if (append || docker_config)
		{
		//cJSON_DeleteItemFromObject(config, "container");
		cJSON_DeleteItemFromObject(config, "container_config");
			
		}
		
		if (append || docker_config)
		{
			debug("V2 append or config\n");
			cJSON_add_string(config, "docker_version", PACKAGE_STRING);
			
			cJSON * config_history=cJSON_GetObjectItem(config, "history");
			cJSON * config_history_item=cJSON_CreateObject();
			cJSON_AddItemToArray(config_history, config_history_item);
			
			cJSON_AddTimeStampToObject(config_history_item, "created");
			cJSON_add_string_from_object(config, "created",config_history_item, "created");
			cJSON_add_string_v(config_history_item, "created_by", "/bin/sh -c #(nop)  RUN %s",parameters->info_command);
			//cJSON_add_string_v(config_history_item, "created_by", "%s",info_command);
			if (!append)
			{
				cJSON_AddTrueToObject(config_history_item,"empty_layer");
			}
			else if (cJSON_IsString(Image_digest)&& parameters->layer_content_digest)
			{
				unsigned char hash[SHA256_DIGEST_LENGTH];
				SHA256_CTX sha256;
				SHA256_Init(&sha256);
				SHA256_Update(&sha256, Image_digest->valuestring, strlen(Image_digest->valuestring));
				SHA256_Update(&sha256, parameters->layer_content_digest, strlen(parameters->layer_content_digest));
				SHA256_Final(hash, &sha256);
				char output[SHA256_DIGEST_LENGTH_BLOBSUM];
				sha256_digest_string(hash, output);
				//cJSON_AddStringToObject(config_config, "Image", output);
			}
		}
		else
		{
			//update labels
			//autolabel_limited(cJSON**docker_config ,const cJSON * job,dkr_parameter_t * parameters,dkr_parameter_t * parameters_from)
			//autolabel_update(cJSON**docker_config ,const cJSON * job,dkr_parameter_t * parameters,dkr_parameter_t * parameters_from)
			debug("V2 no append nor config, autolabel update\n");
			autolabel_update(config,*manifest ,image);
		}
		if (append || docker_config)
		{
			cJSON_DeleteItemFromObject(config, "container_config");
			cJSON* container_config= cJSON_Duplicate(config_config, 1);
			cJSON_AddItemToObject(config, "container_config",container_config);
			
				cJSON_DeleteItemFromObject(container_config, "Cmd");
				cJSON * cmd=cJSON_CreateStringArray( (const char *[]){parameters->info_command,NULL},1);
				cJSON_AddItemToObject(container_config, "Cmd", cmd);
		}
		
		//print_json(config);
	}
	if (!res && config && schemaVerson==2)
	{
		debug("adding manifest layer\n");
		manifest_layers=cJSON_GetObjectItem(*manifest, "layers");
		if (manifest_layers)
		{
			if (append)
			{
			cJSON * layer=cJSON_CreateObject();
			cJSON_AddStringToObject(layer,"mediaType","application/vnd.docker.image.rootfs.diff.tar.gzip");
			cJSON_AddNumberToObject(layer, "size", parameters->layer_size);
			cJSON_AddStringToObject(layer, "digest",parameters->layer_blob_digest );
			cJSON_AddItemToArray(manifest_layers, layer);
			}
			debug("added manifest layer\n");
			char * config_str=cJSON_Print(config);
			int config_len=(int)strlen(config_str);
			calc_sha256_buff(config_str, config_len, digest_config);
			if (config_str) oc_free(&config_str);
			config_str=NULL;
			cJSON_ReplaceItemInObject(manifest_config, "digest",cJSON_CreateString(digest_config));
			cJSON_ReplaceItemInObject(manifest_config, "size",cJSON_CreateNumber(config_len));
			print_json(*manifest);
		}
	}
	// push v2 config
	
	if (!res && config && schemaVerson==2)
	{
		debug("push new config (have manifest v2 and v1Only)\n");
		Write_dyn_trace_pad(trace, yellow,60, "   * Push new config");
		update_details(trace);
		char * config_str=cJSON_Print(config);
		if (config_str)
		{
			int count=0;
			do
			{
				if (count!=0) {
					Write_dyn_trace(trace, red,     "[FAIL]\n");
					print_api_error(trace, res);
					Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
					update_details(trace);
					usleep(2000000LL*count*count);
				}
				res=dkr_api_blob_push_raw(apidata, parameters->image, digest_config, config_str,0);
				count++;
			}while((res<100 || res>=500) && count<6);
			oc_free(&config_str);
			config_str=NULL;
		}
		else res=-1;
		if (res==200||res==201||res==204)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
		    if (res==-1)
				Write_dyn_trace(trace, magenta, "\nOut of memory\n");
			else
				print_api_error(trace, res);
		}
		update_details(trace);
	}
	
	if (!res && config && schemaVerson==2)
	{
		debug("pushing manifest v2\n");
		Write_dyn_trace_pad(trace, yellow,60, "   * Push new manifest V2");
		update_details(trace);
		// update manifest with new config file
	
		// and push new manifest
		char * manifest_str=cJSON_Print(*manifest);
		debug("manifest v2 w updated config digest: \n%s\n",manifest_str);
		
		int count=0;
		do
		{
			if (count!=0) {
				Write_dyn_trace(trace, red,     "[FAIL]\n");
				print_api_error(trace, res);
				Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
				update_details(trace);
				usleep(2000000LL*count*count);
			}
			res=dkr_api_manifest_push_raw(apidata, parameters->image, NULL, manifest_str,1);
			count++;
		}while((res<100 || res>=500) && count<6);
		
		
		
		//calc_sha256_buff(manifest_str, 0, digest_manifest);
		oc_free(&manifest_str);
		manifest_str=NULL;
		if (res>=200 && res<300)
		{
			Write_dyn_trace(trace, green,   "  [OK]\n");
			res=0;
		}
		else if (res==400 || res==415)
		{
			Write_dyn_trace(trace, yellow, "[noV2]\n");
			res=99;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
			print_api_error(trace, res);
		}
		update_details(trace);
		
	}
	
	

	// get V1 manifest if we had v2 and v2 push failed or ImageStream_v1Only
	if ((res==99 || (ImageStream_v1Only==1 && res==0 && parameters->ISR)) && schemaVerson==2 )
	{
		debug("Forced V1 only\n");
		
		if (*manifest)cJSON_Delete(*manifest);
		*manifest=NULL;
		
		cJSON * this_manifest=NULL;
		
		Write_dyn_trace_pad(trace, yellow,60, "   * Get manifest V1");
		res=dkr_api_get_manifest(apidata, "application/vnd.docker.distribution.manifest.v1+json", image, &this_manifest);
		
		
		int this_manifest_version=0;
		if (res>=200 && res<300 && this_manifest)
		{
			debug("received V1 Manifest\n");
			//print_json(this_manifest);
			cJSON * schema=cJSON_GetObjectItem(this_manifest, "schemaVersion");
			if (schema && cJSON_IsNumber(schema))
			{
				this_manifest_version=schema->valueint;
				if (this_manifest_version==1)
				{
					res=99;
					Write_dyn_trace(trace, green,"  [V%d]\n",this_manifest_version);
				}
				else res=98;
			}
			else res =98;
		}
		else
		{
			res=98;
		}
		if (this_manifest_version==1)
		{
			if (*manifest)cJSON_Delete(*manifest);
			*manifest=this_manifest;
			this_manifest=NULL;
		}
		update_details(trace);
	}
//TODO v2 to v1, needed for @sha tag if @tag = v2, registry v1only!!!
	if (res==98)
	{
		
		
		//convert v2 to v1, source registry does only provide v2
		// Not implemented yet
		
		
		Write_dyn_trace(trace, red,     "[FAIL]\n");
		// if converted, set res=99
	}
		
	manifest_config=NULL;
	manifest_layers=NULL;
	if (config) cJSON_Delete(config);
	config=NULL;
	
	// Do push V1 manifest, if no errors
	if (((res==99 && schemaVerson==2 )|| (res==0 && schemaVerson==1)) && manifest)
	{
		// if we're here, we need to modify v1 and push v1
		res=0;
		Write_dyn_trace_pad(trace, yellow,60, "   * Push new manifest V1");
		update_details(trace);
		
		// remove signatures and change name
		cJSON_DeleteItemFromObject(*manifest, "signatures");
		cJSON_ReplaceItemInObject(*manifest, "name", cJSON_CreateString(parameters->namespace));
		
		
		cJSON * config_config=NULL;
		char * last_id=NULL;
		cJSON * Image_digest=NULL;
		cJSON * v1Compat_os=NULL;
		cJSON * v1Compat_arch=NULL;
		cJSON * v1Compat_container=NULL;
		// history V1
		cJSON * history=cJSON_GetObjectItem(*manifest, "history");
		if (history)
		{
			cJSON * v1Compat_0=NULL;
			const char * v1Comp_str=cJSON_get_key(cJSON_GetArrayItem(history, 0), "v1Compatibility");
			v1Compat_0=cJSON_Parse(v1Comp_str);
			if (v1Compat_0)
			{
				debug("last layer original vacompat\n");
				//print_json(v1Compat_0);
				
				last_id=cJSON_get_stringvalue_dup(v1Compat_0,"id");
				// delete unwanted V1 config items
				if (!res && (append || docker_config))
				{
					// modify existing last layer of manifest
					config_config=cJSON_DetachItemFromObject(v1Compat_0, "config");
					Image_digest=cJSON_DetachItemFromObject(config_config,"Image");
					v1Compat_container=cJSON_DetachItemFromObject(v1Compat_0, "container");
					v1Compat_arch=cJSON_DetachItemFromObject(v1Compat_0, "architecture");
					v1Compat_os=cJSON_DetachItemFromObject(v1Compat_0, "os");
					// renove container config
					cJSON * container_config=cJSON_DetachItemFromObject(v1Compat_0, "container_config");
					cJSON * new_container_config=cJSON_CreateObject();
					//add new container config for history
					cJSON_AddItemToObject(v1Compat_0, "container_config",new_container_config);
					// copy only Cmd in new config for history
					cJSON * cmd= cJSON_DetachItemFromObject(container_config, "Cmd");
					cJSON_AddItemToObject(new_container_config, "Cmd", cmd);
				
					cJSON_Delete(container_config);
					container_config=NULL;
					//debug("reduced v1 compat of last layer\n");
					//print_json(v1Compat_0);
				}
				if (!res && docker_config && config_config)
				{
					print_json(docker_config);
					cJSON * tmp=process_docker_config(config_config);
					if (tmp)
					{
						cJSON_Delete(config_config);
						docker_merge_config(&tmp, docker_config);
						
						//print_json(tmp);
						config_config=tmp; // for new layer
					}
				}
				if(!res)
				{
					if (!append && !docker_config)
					autolabel_update(v1Compat_0, *manifest, image);
					
				}
				if (!res && v1Compat_0) // update last layer with new v1Compat
				{
					
					char * v1Compat_0_str=cJSON_PrintUnformatted(v1Compat_0);
					//debug("Parent executer v1Compat\n%s\n",v1Compat_0_str);
					cJSON_ReplaceItemInObject(cJSON_GetArrayItem(history, 0), "v1Compatibility", cJSON_CreateString(v1Compat_0_str));
					//debug("replaced history[0]\n");
					oc_free(&v1Compat_0_str);
					v1Compat_0_str=NULL;
				}
			
				
				// cleanup our v1
				cJSON_Delete(v1Compat_0);
			
			
			
			}
			else res=-1;
			v1Compat_0=NULL;
		
		
		
		}
		else res=-1;
		// where to put??? autolabel_update(config,*manifest ,image);
		// if append, we copy config
		if (!res && (append || docker_config))
		{
			int throwaway_layer=0;
			// add layer to fsLAYERS if needed
			
			if( docker_config && !append)
			{   // add empty layer
				cJSON * fsLayers=cJSON_GetObjectItem(*manifest, "fsLayers");
				cJSON * fsLayer=cJSON_CreateObject();
				// add our executor layer
				cJSON_AddStringToObject(fsLayer, "blobSum", "sha256:a3ed95caeb02ffe68cdd9fd84406680ae93d633cb16422d00e8a7c22955b46d4");
				cJSON_AddItemToBeginArray(fsLayers, fsLayer);
				throwaway_layer=1;
			}
			else
			{   // add our new layer
				cJSON * fsLayers=cJSON_GetObjectItem(*manifest, "fsLayers");
				cJSON * fsLayer=cJSON_CreateObject();
				// add our executor layer
				cJSON_AddStringToObject(fsLayer, "blobSum", parameters->layer_blob_digest);
				cJSON_AddItemToBeginArray(fsLayers, fsLayer);
			}
			
			// Create new config (v1compat item)
			cJSON* config= cJSON_CreateObject();
			cJSON_AddItemToObject(config,"os",v1Compat_os);
			v1Compat_os=NULL;
			cJSON_AddItemToObject(config,"architecture",v1Compat_arch);
			v1Compat_arch=NULL;
			cJSON_AddTimeStampToObject(config, "created");
			cJSON_AddItemToObject(config,"container",v1Compat_container);
			v1Compat_container=NULL;
			
			cJSON_add_string(config, "docker_version", PACKAGE_STRING);
			// delete, add later correct ones
			cJSON_DeleteItemFromObject(config, "parent");
			cJSON_DeleteItemFromObject(config, "id");
			cJSON_DeleteItemFromObject(config, "throwaway");
			cJSON_AddItemToObject(config,"config" ,config_config);
			autolabel_update(config, *manifest, image);
			cJSON* container_config=cJSON_Duplicate(config_config,1);
			cJSON_AddItemToObject(config,"container_config" ,container_config);
				cJSON_DeleteItemFromObject(container_config, "Cmd");
				cJSON * cmd=cJSON_CreateStringArray( (const char *[]){parameters->info_command,NULL},1);
				cJSON_AddItemToObject(container_config, "Cmd", cmd);
		
	
			if (last_id)
			{
				cJSON_AddStringToObject(config, "parent", last_id);
				oc_free(&last_id);
				last_id=NULL;
			}
			if (throwaway_layer)
			{
				cJSON_safe_addBool2Obj(config, "throwaway", 1);
			}
			// Calc image
			if (cJSON_IsString(Image_digest)&& parameters->layer_content_digest)
			{
				unsigned char hash[SHA256_DIGEST_LENGTH];
				SHA256_CTX sha256;
				SHA256_Init(&sha256);
				SHA256_Update(&sha256, Image_digest->valuestring, strlen(Image_digest->valuestring));
				SHA256_Update(&sha256, parameters->layer_content_digest, strlen(parameters->layer_content_digest));
				SHA256_Final(hash, &sha256);
				char output[SHA256_DIGEST_LENGTH_BLOBSUM];
				sha256_digest_string(hash, output);
				// TODO figure out how to do prperly, what is image sha???
                // cJSON_AddStringToObject(config_config, "Image", output);
			}
			
			//debug("v1 config\n");
			//print_json(config_config);
			
			if(config)
			{
			// calc new id
				
				char * v1Comp_str=cJSON_PrintUnformatted(config);
				if (v1Comp_str)
				{
					char digest[65+7];
					calc_sha256_buff(v1Comp_str,(int)strlen(v1Comp_str),digest);
					oc_free(&v1Comp_str);
					v1Comp_str=NULL;
					debug("\n digest for image id: %s\n",digest);
					cJSON_AddStringToObject(config, "id", digest+7);
				}
				cJSON_AddTimeStampToObject(config, "created");
				v1Comp_str=cJSON_PrintUnformatted(config);
				debug("new v1 compat layer");
				
				if (v1Comp_str)
				{
					cJSON * v1Compatibility=cJSON_CreateObject();
					cJSON_AddStringToObject(v1Compatibility, "v1Compatibility", v1Comp_str);
					//debug("New layer v1Compat\n-------------------\n\n");
					//print_json(config);
					oc_free(&v1Comp_str);
					v1Comp_str=NULL;
					cJSON_AddItemToBeginArray(history,v1Compatibility);
				}
				cJSON_Delete(config);
				config=NULL;
			}
		}
		else
		{
			// get rid of
			debug("not append, get rid of \n");
			
		}
		// vcleanup before continue
		if (v1Compat_arch)cJSON_Delete(v1Compat_arch);
		v1Compat_arch=NULL;
		if (v1Compat_os)cJSON_Delete(v1Compat_os);
		v1Compat_arch=NULL;
		if (v1Compat_container)cJSON_Delete(v1Compat_container);
		v1Compat_arch=NULL;
		
		debug("update tag in manifest\n");
		
		
		// add tag after container id calculation, speedup startup for same images, regardless tag
		if(cJSON_GetObjectItem(*manifest, "tag") && parameters->reference)
			cJSON_ReplaceItemInObject(*manifest, "tag", cJSON_CreateString( parameters->reference));
		else if (parameters->reference)
			cJSON_AddStringToObject(*manifest, "tag", parameters->reference);
		else if (cJSON_GetObjectItem(*manifest, "tag"))
			cJSON_ReplaceItemInObject(*manifest, "tag", cJSON_CreateString("latest"));
		else debug("problem replacing or adding tag");
			
		char * Signed_manifest=SignManifest(*manifest);
		
		debug("Signed v1 Manifest\n--------------%s\n",Signed_manifest);
		debug("Push image as %s\n",parameters->image);
		
		int count=0;
		do
		{
			if (count!=0) {
				Write_dyn_trace(trace, red,     "[FAIL]\n");
				print_api_error(trace, res);
				Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
				update_details(trace);
				usleep(2000000LL*count*count);
			}
			res=dkr_api_manifest_push_raw(apidata, parameters->image, NULL, Signed_manifest,0);
			count++;
		}while((res<100 || res>=500) && count<6);
		
		
		if (res>=200 && res<300)
		{
			Write_dyn_trace(trace, green,   "  [OK]\n");
			if (schemaVerson==2) setenv( "OKD_REGISTRY_V1only", "True", 1);
			res=0;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
		    print_api_error(trace, res);
		}
		update_details(trace);
	} // push v1 manifest
	debug("free config\n");
	if (config) cJSON_Delete(config); //TODO fix double free when having this
	config=NULL;
	debug("done push append image\n");
	return res;
}





int push_image(struct dkr_api_data_s * apidata,struct trace_Struct * trace,const cJSON * docker_config,dkr_parameter_t * parameters,int ImageStream_v1Only)
{
	int res=0;
	char digest_config_buf[SHA256_DIGEST_LENGTH_BLOBSUM];
	char *digest_config=NULL;
	//char *digest_layer=NULL;
	cJSON * manifest=NULL;
	char * manifest_str=NULL;
	char * config_str=NULL;
	int config_len=0;
	debug("push image\n");
	Write_dyn_trace_pad(trace, none, 60, "     + creating config & manifest .. ");
	if( parameters->layer_content_digest && parameters->layer_blob_digest)
	{
		//create config and manifest
		debug("create_config_json\n");
		config_str=create_config_json(parameters->layer_content_digest, &config_len, digest_config_buf, docker_config,parameters->info_command);
		debug("create manifest\n");
		manifest_str=create_manifest_json(parameters->layer_blob_digest, digest_config_buf,(int) parameters->layer_size, config_len, NULL);
		if (config_str && manifest_str)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0;
		}
		else
		{
			if (config_str) oc_free(&config_str);
			if (manifest_str) oc_free(&manifest_str);
			config_str=NULL;
			manifest_str=NULL;
			Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
			Write_dyn_trace(trace, magenta, "\nFailed create manifest and/or config.\n");
			res=-1;
		}
		update_details(trace);
	}
	else {
		Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
		Write_dyn_trace(trace, magenta, "\nMissing digest to construct manifest\n");
		update_details(trace);
		return -1;
		
	}
	
	
	if (!res && (!parameters->ISR || (parameters->ISR && ImageStream_v1Only==0)) )
    {
	   
		Write_dyn_trace_pad(trace, none, 60, "     + Uploading config (../blobs/<digest>)...");
		update_details(trace);
		//if ((res=dkr_api_blob_check(apidata, parameters->image, digest_config))!=200)
		{
			if (config_str)
				res=RETRY_HTTP(dkr_api_blob_push_raw(apidata, parameters->image, digest_config_buf,config_str,0));
			else
				res=RETRY_HTTP(dkr_api_blob_push(apidata, parameters->image, digest_config,"%s/config.json",parameters->image_dir));
			if (res==201)
			  {
				  Write_dyn_trace(trace, green, "  [OK]\n");
				  res=0;
			  }
			  else
			  {
				  Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
				  print_api_error(trace, res);
				  res=-1;
			  }
		}
		update_details(trace);
    }
	
	

	if (!res && (!parameters->ISR || (parameters->ISR && ImageStream_v1Only==0)))
	{
		Write_dyn_trace_pad(trace, none, 60, "     + Get manifest V2 ...");
		update_details(trace);
		cJSON * remote_manifest=NULL;
		int res_get;
		int replace=0;

		int count=0;
		do
		{
			if (count!=0) {
			  Write_dyn_trace(trace, red,     "[FAIL]\n");
			  print_api_error(trace, res);
			  Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
			  update_details(trace);
			  usleep(2000000LL*count*count);
			}
			res_get=dkr_api_get_manifest(apidata, "application/vnd.docker.distribution.manifest.v2+json", parameters->image, &remote_manifest);
			count++;
		}while((res_get<100 || res_get>=500) && count<6);

		res=404;
		if (res_get==200 && remote_manifest)
		{
		 if (!manifest) manifest=cJSON_Parse(manifest_str);
		 if (manifest)
		 {
			 remote_manifest=NULL;
			cJSON* remote_config=cJSON_GetObjectItem(remote_manifest, "config");
			cJSON* config=cJSON_GetObjectItem(manifest, "config");
			if (remote_config&&config)
			{
			   const char * digest_remote=cJSON_get_key(remote_config, "digest");
			   const char * digest_local=cJSON_get_key(config, "digest");
			   if (digest_local && digest_remote && strcmp(digest_remote,digest_local)==0)
			   {
				   res=200;
				   debug("manifest exists and is equal\n");
				   Write_dyn_trace_pad(trace, none, 60, "     + Uploading manifest V2 ...");
				   Write_dyn_trace(trace, green, "[skip]\n");
				   update_details(trace);
			   }
			   else
			   {
				  replace=1;
			   }
			}
			 
		 }
		}
		if (remote_manifest) cJSON_Delete(remote_manifest);
		remote_manifest=NULL;
		if (res!=200 )
		{
		  Write_dyn_trace_pad(trace, none, 60, "     + Uploading manifest V2 ...");
		  update_details(trace);
		  int count=0;
		  do
		  {
			  if (count!=0) {
				  Write_dyn_trace(trace, red,     "[FAIL]\n");
				  print_api_error(trace, res);
				  Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
				  update_details(trace);
				  usleep(2000000LL*count*count);
			  }
			  res= dkr_api_manifest_push_raw(apidata, parameters->image, parameters->reference,manifest_str,1);
			  count++;
		  }while((res<100 || res>=500) && count<6);

		  
		 if (res==201) // created
		 {
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0; // pushed
		 }
		 else if (res==400 || res==415)
		 {
			Write_dyn_trace(trace, yellow, "[noV2]\n");
			res=99;
		 }
		 else
		 {
			Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
			if (replace)
			   Write_dyn_trace(trace, red, "\n replacing error %d\n",res);
			else
			   Write_dyn_trace(trace, red, "\n    create error %d\n",res);
			print_api_error(trace, res);
		 }
		  update_details(trace);
		} else{
		  res=0; // already up to date
		}
	}

	else if (!res)
	{
	   // ISR ImageStream_v1Only=true
	   res=99;

	}
	if (res==99)
	{
		debug("Create manifest V1\n");
		char *blobSum=NULL;
		if (!manifest)  manifest=cJSON_Parse(manifest_str);


		cJSON * manifest_v2_layers=cJSON_GetObjectItem(manifest, "Layers");
		cJSON * manifest_v2_layer=cJSON_GetArrayItem(manifest_v2_layers, 0);
		blobSum=cJSON_get_stringvalue_dup(manifest_v2_layer, "digest");
		  
		if (!config_str)config_str= read_a_file_v("%s/config.json",parameters->image_dir);
		cJSON * image_config=NULL;
		if (!image_config)
		{
		image_config=cJSON_Parse(config_str);
		}
		else error("image_configstring==NULL");

		if (!parameters->reference) parameters->reference=strdup("latest");
		Write_dyn_trace_pad(trace, none,60, "     + push manifests_v1/%s",parameters->reference);
		update_details(trace);

		debug("   create V1\n");
		// create v1 manifest from v2 / config
		// time to create!!!!
		// add layertov1 manifest
		cJSON * manifest_V1=cJSON_CreateObject();
		cJSON_AddNumberToObject(manifest_V1, "schemaVersion", 1);
		debug("manifest name: %s:%s\n",parameters->namespace,parameters->reference);
		cJSON_AddItemToObject(manifest_V1, "name", cJSON_CreateString(parameters->namespace));
		cJSON_AddItemToObject(manifest_V1, "tag", cJSON_CreateString( parameters->reference));
		cJSON_AddStringToObject(manifest_V1,"architecture","amd64");
		cJSON * fsLayers=cJSON_CreateArray();
		cJSON_AddItemToObject(manifest_V1, "fsLayers",fsLayers);
		cJSON * fsLayer=cJSON_CreateObject();
		cJSON_AddItemToArray(fsLayers, fsLayer);


		if (blobSum)cJSON_AddStringToObject(fsLayer, "blobSum", blobSum);

		// history V1

		cJSON * history= cJSON_CreateArray();
		cJSON_AddItemToObject(manifest_V1, "history",history);
		  
		  // modify image_config to add as v1compatibility
		cJSON_DeleteItemFromObject(image_config, "rootfs");
		cJSON_DeleteItemFromObject(image_config, "history");
		  
		cJSON * config_v1=cJSON_GetObjectItem(image_config, "config");
		  debug("config V1, adding Image\n");
		  print_json(config_v1);
		//if (parameters->layer_content_digest && !cJSON_GetObjectItem(config_v1, "Image")) cJSON_AddStringToObject(config_v1, "Image", parameters->layer_content_digest);
		  
		cJSON * v1Compatibility=cJSON_CreateObject();
		// add v1 Compat to histroy array
		cJSON_AddItemToArray(history,v1Compatibility);

		// calc new id
		char * v1Comp=cJSON_PrintUnformatted(image_config);
		char digest[SHA256_DIGEST_LENGTH_BLOBSUM ];
		if (v1Comp)
		{ // for id calc
		calc_sha256_buff(v1Comp,(int)strlen(v1Comp),digest);
		oc_free(&v1Comp);
		v1Comp=NULL;
		cJSON_AddStringToObject(image_config, "id", digest+7);
		}
		// print modified config for V1 compat
		v1Comp=cJSON_PrintUnformatted(image_config);

		  if (image_config) cJSON_Delete(image_config);
		image_config=NULL;

		cJSON_AddStringToObject(v1Compatibility, "v1Compatibility", v1Comp);
		//debug("added v1Compat\n-------------------\n%s\n",v1Comp);
		oc_free(&v1Comp);
		  v1Comp=NULL;

		cJSON_ReplaceItemInObject(manifest_V1, "tag", cJSON_CreateString( parameters->reference));
		// Sign the new manifest
		char * Signed_manifest=SignManifest(manifest_V1);
		if (manifest_V1) cJSON_Delete(manifest_V1);
		manifest_V1=NULL;

		debug("Signed v1 Manifest\n--------------%s\n",Signed_manifest);
		debug("Push Image name: %s\n",parameters->image);
		// And push it
		
		int count=0;
		do
		{
			if (count!=0) {
				Write_dyn_trace(trace, red,     "[FAIL]\n");
				print_api_error(trace, res);
				Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
				update_details(trace);
				usleep(2000000LL*count*count);
			}
			res=dkr_api_manifest_push_raw(apidata, parameters->image, NULL, Signed_manifest,0);
			count++;
		}while((res<100 || res>=500) && count<6);
		if (res>=200 && res<300)
		{
		   Write_dyn_trace(trace, green,   "  [OK]\n");
		   res=0;
		}
		else
		{
			Write_dyn_trace(trace, red, "[FAIL]\n");
			print_api_error(trace, res);
		}
		 
		if (blobSum) oc_free(&blobSum);
		if(Signed_manifest) oc_free(&Signed_manifest);
		Signed_manifest=NULL;
		  
	}
	
	
	update_details(trace);
	if (config_str) oc_free(&config_str);
	if (manifest_str) oc_free(&manifest_str);
	if (manifest) cJSON_Delete(manifest);
	if (digest_config) oc_free(&digest_config);
	debug("done pushing image\n");
	return res;
}

int tag_image(struct dkr_api_data_s * apidata,struct trace_Struct * trace,dkr_parameter_t * parameters)
{
	int res=0;
	debug("Start TAG !!!\n\n");
	if (!parameters->tag) return-1;
	char  * newtag=strdup(parameters->tag);
	
	Write_dyn_trace(trace, yellow, "   * Tag image to %s ...\n",newtag);
	Write_dyn_trace_pad(trace, none, 60, "     + get reference manifest...");
	update_details(trace);

    char * manifestV2_str=NULL;
    cJSON * manifest_V2=NULL;
    cJSON * image_config=NULL;
	
	
	int count=0;
	do
	{
		if (count!=0) {
			Write_dyn_trace(trace, red,     "[FAIL]\n");
			print_api_error(trace, res);
			Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
			update_details(trace);
			usleep(2000000LL*count*count);
		}
		res=dkr_api_get_manifest_raw(apidata, "application/vnd.docker.distribution.manifest.v2+json", parameters->image, &manifestV2_str);
		count++;
	}while((res<100 || res>=500) && count<6);
	if (res==200 && manifestV2_str)
	{
       debug("manifest:\n%s\n",manifestV2_str);
		int schemaVersion=0;
		if (manifestV2_str)
		{
			cJSON * manifest=cJSON_Parse(manifestV2_str);
			if (manifest)
			{
			cJSON * schema=cJSON_GetObjectItem(manifest, "schemaVersion");
			
			if (schema && cJSON_IsNumber(schema))
			{
				schemaVersion=schema->valueint;
				res=0;
				Write_dyn_trace(trace, green,"  [V%d]\n",schemaVersion);
			}
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				Write_dyn_trace(trace, magenta, "\n      ERROR: Invalid Manifest, could not find schemaVersion!\n");
			}
			}
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				Write_dyn_trace(trace, magenta, "\n      ERROR: Invalid Manifest, could not parse manifest!\n");
			}
			cJSON_Delete(manifest);
			manifest=NULL;
			update_details(trace);
		}
		
		if (parameters->GLR && schemaVersion==2)
		{
			debug("\n Getting config.json for gitlab tag\n\n");
			Write_dyn_trace_pad(trace, none, 60, "     + get config.json ...");
			cJSON * manifest_V2=cJSON_Parse(manifestV2_str);
			   
			const char * blob=NULL;
			char * config=NULL;
			cJSON * manifest_config=NULL;
			if (manifest_V2)  manifest_config=cJSON_GetObjectItem(manifest_V2, "config");
			if(manifest_config) blob=cJSON_get_key(manifest_config, "digest");
			if (blob)
			{
				int count=0;
				do
				{
					if (count!=0) {
						Write_dyn_trace(trace, red,     "[FAIL]\n");
						print_api_error(trace, res);
						Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
						update_details(trace);
						usleep(2000000LL*count*count);
					}
					res=dkr_api_blob_get(apidata, parameters->image, blob, &config);
					count++;
				}while((res<100 || res>=500) && count<6);
			}
			if ((res==200 || res==201) && blob)
			{
				Write_dyn_trace(trace, green, "  [OK]\n");
				Write_dyn_trace_pad(trace, none, 60, "     + push config.json blob...");
				image_config=cJSON_Parse(config);
			}
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
				Write_dyn_trace(trace, red, "\n pull config.json error: %d\n",res);
				print_api_error(trace, res);
			}
			if (config) res=RETRY_HTTP(dkr_api_blob_push_raw (apidata, parameters->image, blob, config,0));
			if (res==200 || res==201)
			{
				Write_dyn_trace(trace, green, "  [OK]\n");
				//Write_dyn_trace(trace, yellow, "         %s\n",blob);
			}
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
				Write_dyn_trace(trace, red, "\n push config.json error: %d\n",res);
				print_api_error(trace, res);
			}
			if (config) oc_free(&config);
		}
		
		if (schemaVersion==2)
		{
			Write_dyn_trace_pad(trace, none, 60, "     + push tag (.../manifests/%s) (v2) ...",newtag);
			update_details(trace);
			
			int count=0;
			do
			{
				if (count!=0) {
				  Write_dyn_trace(trace, red,     "[FAIL]\n");
				  print_api_error(trace, res);
				  Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
				  update_details(trace);
				  usleep(2000000LL*count*count);
				}
				res=dkr_api_manifest_push_raw(apidata, parameters->image, newtag,manifestV2_str,1);
				count++;
			}while((res<100 || res>=500) && count<6);
			if (res==201) // created
			{
				Write_dyn_trace(trace, green, "  [OK]\n");
				res=0; // pushed
			}
			else if (res>=400 && res<500)
			{
				   Write_dyn_trace(trace, yellow, "[V1??]\n");
				   res=99;
				   cJSON_Delete(manifest_V2);
				   manifestV2_str=NULL;
			}
			else
			{
				Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
				Write_dyn_trace(trace, red, "\n push tag error: %d\n",res);
				print_api_error(trace, res);
			}
		}
		
		if (res==99 || schemaVersion==1)
		{
			 cJSON * manifest=NULL;
			char * manifest_str=NULL;
			if (manifestV2_str && schemaVersion==1)
			{
				manifest_str=manifestV2_str;
				manifestV2_str=NULL;
			}
			
			if (!manifest_str)
			{
				Write_dyn_trace_pad(trace, none,60, "     + get V1 manifest...");
				update_details(trace);
				int count=0;
				do
				{
					if (count!=0) {
					  Write_dyn_trace(trace, red,     "[FAIL]\n");
					  print_api_error(trace, res);
					  Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
					  update_details(trace);
					  usleep(2000000LL*count*count);
					}
					res=dkr_api_get_manifest_raw(apidata, "application/vnd.docker.distribution.manifest.v1+json", parameters->image, &manifest_str);
					count++;
				}while((res<100 || res>=500) && count<6);
				if (res!=200 || !manifest_str)
				{
					// create v1 manifest from v2 / config
					 Write_dyn_trace(trace, red, "[FAIL]\n");
					 print_api_error(trace, res);
				}
				else
				{
					manifest=cJSON_Parse(manifest_str);
					if (!manifest)
			 		{
				 		Write_dyn_trace(trace, red, "[FAIL]\n");
				 		Write_dyn_trace(trace, magenta, "Could not parse manifest string\n");
			 		}
				}
				update_details(trace);
			}
			else
			{
			 // add layertov1 manifest
				manifest=cJSON_Parse(manifest_str);
			}
			if ( manifest )
			{
				Write_dyn_trace_pad(trace, none,60, "     + push tag (.../manifests/%s) (v1) ...",newtag);
				update_details(trace);
				if (cJSON_GetObjectItem(manifest, "signatures")) cJSON_DeleteItemFromObject(manifest, "signatures");
				cJSON_ReplaceItemInObject(manifest, "tag", cJSON_CreateString( newtag));
				char * Signed_manifest=SignManifest(manifest);
				int count=0;
				do
				{
					if (count!=0) {
						Write_dyn_trace(trace, red,     "[FAIL]\n");
						print_api_error(trace, res);
						Write_dyn_trace_pad(trace, yellow, 60,"     -- RETRY");
						update_details(trace);
						usleep(2000000LL*count*count);
					}
					res=dkr_api_manifest_push_raw(apidata, parameters->image, newtag, Signed_manifest,0);
					count++;
				}while((res<100 || res>=500) && count<6);

				if (res>=200 && res<300)
				{
				 Write_dyn_trace(trace, green,   "  [OK]\n");
				 res=0;
				}
				else
				{
				 Write_dyn_trace(trace, red, "[FAIL]\n");
				 print_api_error(trace, res);
				}
			}
			update_details(trace);
			if (manifest_str) oc_free(&manifest_str);
			if (manifest) cJSON_Delete(manifest);
			manifest=NULL;
			manifest_str=NULL;
		}
		oc_free(&manifestV2_str);
	}
	else
	{
		Write_dyn_trace(trace, red, "[FAIL]\n"); // fail layer
		Write_dyn_trace(trace, red, "\n getting reference %d\n",res);
		print_api_error(trace, res);
	}
	if (manifest_V2) cJSON_Delete(manifest_V2);
	if (image_config) cJSON_Delete(image_config);
	manifest_V2=NULL;
	image_config=NULL;
	if (!res)
	{
		change_image_tag(&parameters->image_nick,newtag );
	}
	if (newtag) oc_free(&newtag);
	update_details(trace);
	return res;
}



int process_options_transfer(size_t argc, char *const*argv, dkr_parameter_t *parameter_from,dkr_parameter_t *parameter_to)
{
	
	struct option long_options[] = {
		{ "from_name",       1, NULL, 'n' },
		{ "name",            1, NULL, 'm' },
		{ "from_reference",  1, NULL, 'r' },
		{ "reference",       1, NULL, 's' },
		{ "from_image",      1, NULL, 'i' },
		{ "image",           1, NULL, 'j' },
		{ "from_GLR",        0, NULL, 'g' },
		{ "ISR",             0, NULL, 'h' }, // fix this afgter bug fix segment fault
		{ "from_creds",      1, NULL, 'c' },
		{ "creds",           1, NULL, 'd' },
		{ "config"       , 2, NULL, 'f' },

//		{ "from_namespace",  1, NULL, 'a' },
//		{ "to_namespace",    1, NULL, 'b' },
		{ NULL,         0, NULL, 0 } };
	
	int           which;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'f': {
					parameter_to->docker_config=get_opt_value(optarg, "docker_config.yml",NULL);
				}
					break;
				case 'n': {
					parameter_from->image_name =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'm': {
					parameter_to->image_name =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'r': {
					parameter_from->reference =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 's': {
					parameter_to->reference =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'i': {
					parameter_from->image =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'j': {
					parameter_to->image =get_opt_value(optarg, NULL,NULL);
				}
				    break;
					
				case 'c': {
					parameter_from->creds=get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'd': {
					parameter_to->creds =get_opt_value(optarg, NULL,NULL);
				}
					break;
					
				case 'a': {
					parameter_from->namespace =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'b': {
					parameter_to->namespace =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'g': {
					parameter_from->GLR=1;
				}
					break;
				case 'h': {
					parameter_to->ISR=1;
				}
					break;
					/* otherwise    : display usage information */
				default:
					;
					break;
			}
		}
	}
	return optind;
}



int process_options_registry_push(size_t argc,char * const *argv, dkr_parameter_t *parameters,dkr_parameter_t *parameters_from)
{
	struct option long_options[] = {
		
		{ "verbose",       0, NULL, 'v' },
		{ "quiet",         0, NULL, 'q' },
		{ "u2g",           0, NULL, 'u' },
		{ "rootfs",        2, NULL, 'p' },
		{ "archive",       2, NULL, 'l' },
		{ "image_dir",     2, NULL, 'k' },
		{ "name",          1, NULL, 'm' },
		{ "reference",     1, NULL, 's' },
		{ "image",         1, NULL, 'j' },
		{ "GLR",           0, NULL, 'h' },
		{ "ISR",           0, NULL, 'o' },
		{ "credentials",   1, NULL, 'd' },
		{ "from_reference",1, NULL, 'r' },
		{ "from_GLR",      0, NULL, 'g' },
		{ "from_ISR",      0, NULL, 'e' },
		{ "from_creds",    1, NULL, 'b' },
		{ "from_name",     1, NULL, 'n' },
		{ "from_image",    1, NULL, 'i' },
		{ "config"       , 2, NULL, 'f' },
		{ "skip_label"       , 0, NULL, 'S' },
		{ "skip-label"       , 0, NULL, 'S' },
		{ NULL,            0, NULL,  0  }
	};
	
	int           which;
	parameters->ISR=0;
	parameters->GLR=0;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameters->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameters->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameters->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameters->quiet = 1;
				}
					break;
				case 'l': {
					parameters->archive=get_opt_value(optarg, "layer.tar.gz",NULL);
//TODO:                if(!parameters->archive) error
				}
					break;
					break;
				case 'u': {
					parameters->u2g=1;
				}
					break;
				case 'p': {
					parameters->rootfs=get_opt_value(optarg, "rootfs",NULL);
//TODO:                if(!parameters->rootfs) error
				}
					break;
				case 'f': {
					parameters->docker_config=get_opt_value(optarg, "docker_config.yml",NULL);
				}
					break;
				case 'm': {
					parameters->image_name =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'S': {
					parameters->skip_auto =1;
				}
					break;
				
				case 's': {
					parameters->reference =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'j': {
					parameters->image =get_opt_value(optarg, NULL,NULL);
				}
					break;
					
				case 'd': {
					parameters->creds =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'k': {
					parameters->image_dir=get_opt_value(optarg, "image",NULL);
				}
					break;
				case 'h': {
					parameters->GLR=1;
				}
					break;
				case 'g': {
					parameters_from->GLR=1;
				}
					break;
				case 'e': {
					parameters_from->ISR=1;
				}
					break;
				case 'o': {
					parameters->ISR=1;
				}
					break;
				case 'r': {
					
						parameters_from->reference=get_opt_value(optarg, NULL,NULL);
				}
					break;

				case 'i': {
					 parameters_from->image=get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'b': {
					parameters_from->creds=get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'n': {
					parameters_from->image_name=get_opt_value(optarg, NULL,NULL);
				}
					break;

					/* otherwise    : display usage information */
				default:
					;// TODO: need create error for unkown
					break;
			}
		}
	}
	//TODO: cannot, we might have --rootfs or --archive
	//to, defaults to GLR
	if (!parameters->image && !parameters->GLR && !parameters->ISR)
	{
		
		parameters->GLR=1;
	}
	//from
	if (!parameters_from->image && !parameters_from->GLR && !parameters_from->ISR && (parameters_from->image_name || parameters_from->reference))
	{
	   
		parameters_from->ISR=1;
	}
	//if (parameters->image) {parameters->GLR=0;parameters->ISR=0;}

	return optind;
}


int process_options_tag(size_t argc, char *const*argv, dkr_parameter_t *parameters)
{
	
	struct option long_options[] = {
		
		{ "verbose",       0, NULL, 'v' },
		{ "quiet",         0, NULL, 'q' },
		{ "name",          1, NULL, 'm' },
		{ "reference",     1, NULL, 's' },
		{ "image",         1, NULL, 'j' },
		{ "GLR",           0, NULL, 'h' },
		{ "tag",           1, NULL, 't' },
		{ "credentials",   1, NULL, 'd' },
		{ NULL,            0, NULL,  0  }
	};
	
	int           which;
	parameters->ISR=0;
	parameters->GLR=0;
	optind=0;  // reset if called again, needed for fullcheck as we call twice (server and main)
	// ref : http://man7.org/linux/man-pages/man3/getopt.3.html
	if (argc>1)
	{
		/* for each option found */
		while ((which = getopt_long((int)argc, argv, "+", long_options, NULL)) > 0) {
			
			/* depending on which option we got */
			switch (which) {
					/* --verbose    : enter verbose mode for debugging */
				case 'v':
				{
					if (parameters->quiet)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
					{
						parameters->verbose = 1;
					}
				}
					break;
				case 'q':
				{
					if (parameters->verbose)
					{
						error("Invalid option, choose quiet or verbose\n");
						return -1;
					}
					else
						parameters->quiet = 1;
				}
					break;
					
				case 'm': {
					parameters->image_name =get_opt_value(optarg, NULL,NULL);
				}
					break;
					
				case 's': {
					parameters->reference =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'j': {
					parameters->image =get_opt_value(optarg, NULL,NULL);				}
					break;
					
				case 'd': {
					parameters->creds =get_opt_value(optarg, NULL,NULL);				}
					break;
					
				case 't': {
				 parameters->tag =get_opt_value(optarg, NULL,NULL);
				}
					break;
				case 'h': {
					parameters->GLR=1;
					parameters->ISR=0;
				}
					break;
					/* otherwise    : display usage information */
				default:
					;
					break;
			}
		}
	}
	if (!parameters->image && !parameters->GLR) parameters->ISR=1;
	if (parameters->image) parameters->GLR=0;
	return optind;
}


char * create_config_json(const char digest_content[SHA256_DIGEST_LENGTH_BLOBSUM], int * config_len,char digest_config[SHA256_DIGEST_LENGTH_BLOBSUM],const cJSON*docker_config, char * info_command )
{
	
	cJSON * config=cJSON_CreateObject();
	cJSON_AddStringToObject(config, "architecture", "amd64");
	cJSON_AddStringToObject(config, "os", "linux");
	cJSON_add_string(config, "docker_version", PACKAGE_STRING);
	cJSON_AddTimeStampToObject(config, "created");
	char container[SHA256_DIGEST_LENGTH_HASH];
	const char * created=cJSON_get_key(config, "created");
	
	cJSON * config_config=docker_config_get_default_config();
	print_json(config_config);
	if (docker_config)
	{
		debug("merge docker config");
		docker_merge_config(&config_config, docker_config);
	}
	
	cJSON * container_config=cJSON_Duplicate(config_config, 1);
	cJSON_AddItemToObject(config, "config", config_config);
    cJSON_AddStringToObject(config_config, "Image", digest_content);
	
	calc_sha256_digest_hash (created,0, container);
	cJSON_add_string(config, "container", container);
	
	cJSON_AddItemToObject(config, "container_config",container_config);
	cJSON_DeleteItemFromObject(container_config, "Cmd");
	cJSON * cmd=NULL;
	//cJSON * cmd=cJSON_CreateStringArray((const char**)((char * const[]){"/bin/sh","#(nop) ",info_command,NULL}),1);
	debug("info Command=%s\n",info_command);
	if (info_command) cmd=cJSON_CreateStringArray( (const char *[]){info_command,NULL},1);
	else cmd=cJSON_CreateStringArray( (const char *[]){"odagrun internal",NULL},1);
	cJSON_AddItemToObject(container_config, "Cmd", cmd);
	

	cJSON * history= cJSON_CreateArray();
	cJSON_AddItemToObject(config, "history", history);
	cJSON * history_item=cJSON_CreateObject();
	cJSON_AddItemToArray(history, history_item);
	if (info_command)
		cJSON_add_string_v(history_item, "created_by", "/bin/sh -c #(nop)  RUN %s",info_command);
	else
		cJSON_add_string_v(history_item, "created_by", "/bin/sh -c #(nop)  RUN odagrun internal use");
    cJSON_add_string(history_item, "created", created);
	
	
	
	
	
	cJSON * config_rootfs=cJSON_CreateObject();
	cJSON_AddItemToObject(config, "rootfs", config_rootfs);
	cJSON_AddStringToObject(config_rootfs, "type","layers" );
	
	cJSON_AddItemToObject(config_rootfs, "diff_ids", cJSON_CreateStringArray( (const char *[]){digest_content}, 1));
	char * config_string=cJSON_Print(config);
	debug("\nCreate config \n");
	print_json(config);
	
	*config_len=(int)strlen(config_string);
	calc_sha256_buff(config_string,*config_len, digest_config);
	if (config)	cJSON_Delete(config);
	
	return config_string;
}

char * create_config_json_file(const char digest_content[SHA256_DIGEST_LENGTH_BLOBSUM], int * config_len,char digest_config[SHA256_DIGEST_LENGTH_BLOBSUM],const char * filename,... )
{
	int aspr=0;
	char * config_json=CSPRINTF_L(filename,&aspr);
	
	if (aspr==-1)
	{
		error("Buffer error contents\n");
		if (config_json) free (config_json);
		config_json=NULL;
		return NULL;
	}
	
	cJSON * config=cJSON_CreateObject();
	cJSON_AddStringToObject(config, "architecture", "amd64");
	cJSON * config_config=docker_config_get_default_config();
	cJSON_AddItemToObject(config, "config", config_config);
	cJSON_AddStringToObject(config_config, "Image", digest_content);
	cJSON * history= cJSON_CreateArray();
	cJSON_AddItemToObject(config, "history", history);
	cJSON * history_item=cJSON_CreateObject();
	cJSON_AddItemToArray(history, history_item);
	//cJSON_AddStringToObject(history_item, "created_by", "odagrun");
	cJSON_add_string_v(history_item, "created_by", "/bin/sh -c #(nop) ADD file:%s in /",digest_content+7);

	struct tm *tm_gmt;
	time_t t;
	t = time(NULL);
	tm_gmt = gmtime(&t);
	char date_buffer[36];
	strftime(date_buffer, 36, "%Y-%m-%dT%H:%M:%S.000000000Z", tm_gmt);
	cJSON_AddStringToObject(history_item, "created", date_buffer);
	cJSON_AddStringToObject(config, "created", date_buffer);
	cJSON_AddStringToObject(config, "os", "linux");
	cJSON * config_rootfs=cJSON_CreateObject();
	cJSON_AddItemToObject(config, "rootfs", config_rootfs);
	cJSON_AddStringToObject(config_rootfs, "type","layers" );
	
	cJSON_AddItemToObject(config_rootfs, "diff_ids", cJSON_CreateStringArray( (const char *[]){digest_content}, 1));
	char * config_string=cJSON_Print(config);
	print_json(config);
	
	*config_len=(int)strlen(config_string);
	calc_sha256_buff(config_string,*config_len, digest_config);
	if (config_json)
	{
		FILE * f = fopen ( config_json, "wb" );
		if ( !f ) {
			/* handle error */
			fclose(f);
			
			if (config_json) free (config_json);
			config_json=NULL;
			return NULL;
		}
		fwrite(config_string, 1, *config_len, f);
		fclose(f);
		free (config_string);
		cJSON_Delete(config);
		return config_json;
	}
	else
	{
		cJSON_Delete(config);
		return config_string;
	}
}



char * create_manifest_json(const char digest_archive[65+7],const char digest_config[65+7],int layer_size,int config_len,const char * filename,...)
{
	
	va_list varg;
	char * manifest_json=NULL;
	
	if (filename)
	{
		va_start (varg, filename);
		size_t consten_buf_len=0;
		size_t cnt=0;
		int res=0;
		do {
			cnt++;
			if (manifest_json) oc_free(&manifest_json);
			manifest_json=NULL;
			manifest_json=(char *)calloc(cnt,1024);
			res=-1;
			if (manifest_json)
			{
				consten_buf_len=(size_t)(1024 * cnt);
				res=vsnprintf(manifest_json,consten_buf_len ,filename,varg);
			}
			else
				break;
		} while ((size_t)res >= consten_buf_len);
		
		va_end(varg);
		
		if (res<0)
		{
			error("Buffer error contents\n");
			if (manifest_json) free (manifest_json);
			manifest_json=NULL;
			return NULL;
		}
	}
	cJSON * manifest=cJSON_CreateObject();
	cJSON_AddNumberToObject(manifest,"schemaVersion", 2);
	cJSON_AddStringToObject(manifest,"mediaType", "application/vnd.docker.distribution.manifest.v2+json");
	cJSON * manifest_config=cJSON_CreateObject();
	cJSON_AddItemToObject(manifest, "config",manifest_config);
	cJSON_AddStringToObject(manifest_config,"mediaType", "application/vnd.docker.container.image.v1+json");
	cJSON_AddNumberToObject(manifest_config,"size",config_len);
	
	cJSON_AddStringToObject(manifest_config,"digest",digest_config);
	cJSON * manifest_layers=cJSON_CreateArray();
	cJSON_AddItemToObject(manifest, "layers", manifest_layers);
	cJSON * manifest_layer=cJSON_CreateObject();
	cJSON_AddItemToArray(manifest_layers, manifest_layer);
	cJSON_AddStringToObject(manifest_layer, "mediaType", "application/vnd.docker.image.rootfs.diff.tar.gzip");
	cJSON_AddNumberToObject(manifest_layer,"size",layer_size);
	cJSON_AddStringToObject(manifest_layer,"digest",digest_archive);
	print_json(manifest);
	
	char * manifest_string=cJSON_Print(manifest);
	
	if (manifest_json)
	{
		int manifest_len=(int)strlen(manifest_string);
		// save
		FILE * f = fopen ( manifest_json, "w" );
		if ( !f ) {
			/* handle error */
			fclose(f);
			if (manifest_json) free (manifest_json);
			manifest_json=NULL;
			return NULL;
		}
		fwrite(manifest_string, 1, manifest_len, f);
		fclose(f);
		oc_free(&manifest_string);
		cJSON_Delete(manifest);
		return manifest_json;
	}
	else
	{
		cJSON_Delete(manifest);
		return manifest_string;
	}

}

int get_digest_from_config(cJSON* config,char **digest_content)
{
	print_json(config);
	cJSON * rootfs=cJSON_GetObjectItem(config, "rootfs");
	cJSON * diff_ids=cJSON_GetObjectItem(rootfs,"diff_ids");
	cJSON * layer=cJSON_GetArrayItem(diff_ids, 0);
	
		if (layer) *digest_content=strdup(layer->valuestring);
	
	return 0;
}


static int get_digest_from_manifest(cJSON* manifest,char **digest_archive,char **digest_config,int * blob_size)
{
	cJSON * config=cJSON_GetObjectItem(manifest, "config");
	if (config)
	{
		*digest_config=cJSON_get_stringvalue_dup(config, "digest");
	}
	else return -1;
	
	cJSON * layers=cJSON_GetObjectItem(manifest, "layers");
	cJSON * layer=cJSON_GetArrayItem(layers, 0);
	if (config)
	{
		*digest_archive=cJSON_get_stringvalue_dup(layer, "digest");
		*blob_size =cJSON_safe_GetNumber(layer, "size");
	}
	else return -1;
	
	return 0;
}

