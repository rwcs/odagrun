/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.

 This file is part of the odagrun

 MIT License

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
/*! \file simple_http.c
 *  \brief simple http requests
 *  \author Created by Danny Goossen on 23/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 *
 */

#include "common.h"
#include "utils.h"
#include <stdio.h>
#include <cJSON.h>
#include "simple_http.h"
#include "dyn_buffer.h"
#include "curl_headers.h"


CURL * simple_http_init(const char * CAINFO,enum easy_http_folow_location FOLLOWLOCATION,enum easy_http_verbose VERBOSE,enum easy_http_SSL SSL_VERIFYPEER, const char * USERAGENT)
{
	CURL * curl=curl_easy_init();

	if (curl)
	{
	curl_easy_setopt(curl, CURLOPT_CAINFO, CAINFO);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, (long)SSL_VERIFYPEER);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, (long)FOLLOWLOCATION);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, (long)VERBOSE);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, USERAGENT);
	}
	return curl;
}

void simple_http_clean(CURL ** curlp)
{
	if (curlp)
	{
		curl_easy_cleanup(*curlp);
		*curlp=NULL;
	}
}

int simple_httpget_v(CURL * curl,char**data_in,size_t*len_in,const char *url,...)
{
	int aspr=0;
	char * request=CSPRINTF_L(url,&aspr);
	if (aspr==-1)
		return -1;

	int res=-1;
	if(curl)
	{
		dynbuffer * chunk=dynbuffer_init();
		curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);

		curl_easy_setopt(curl, CURLOPT_URL, request);

		/* send all data to this function  */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		/* we pass our 'chunk' struct to the callback function */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, chunk);
		//curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
		//curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, 0L);
		res = curl_easy_perform(curl);
		long http_code = 0;
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
		dynbuffer_pop(&chunk, data_in, len_in);
		dynbuffer_clear(&chunk);

			if (res==0) res=(int)http_code;

	}
	if (request) free(request);
	return res;
}


int simple_httppost_v(CURL * curl,char**data_in,size_t*len_in,const char *url,...)
{
	int aspr=0;
	char * request=CSPRINTF_L(url,&aspr);
	if (aspr==-1)
		return -1;

	int res=-1;
	if(curl)
	{
		dynbuffer*chunk=dynbuffer_init();
		curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
		curl_easy_setopt(curl, CURLOPT_POST,1L);
		/* send all data to this function  */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		/* we pass our 'chunk' struct to the callback function */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)chunk);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
		curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, 0L);
		res = curl_easy_perform(curl);
		long http_code = 0;
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
		dynbuffer_pop(&chunk, data_in, len_in);
		dynbuffer_clear(&chunk);


			if (res==0) res=(int)http_code;

	}
	if (request) free(request);
	return res;
}

struct put_data
{
	const char *data;
	size_t len;
	size_t size;
};

static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream)
{
	struct put_data * userdata=(struct put_data *)stream;
	size_t curl_size = nmemb * size;
	size_t to_copy = (userdata->len < curl_size) ? userdata->len : curl_size;
	memcpy(ptr, userdata->data, to_copy);
	userdata->len -= to_copy;
	userdata->data += to_copy;
	return to_copy;
}

int custom_http_v(CURL * curl,const char *data_out,size_t len_out,const cJSON * headers_out,char**data_in,size_t * len_in,cJSON ** headers_in,const char * methode,const char *url,...)
{
	int aspr=0;
	char * request=CSPRINTF_L(url,&aspr);
	if (aspr==-1)
		return -1;

	int res=-1;
	if(curl)
	{
		struct curl_headers_struct headers_struct_in;
		curl_headers_init(&headers_struct_in);
		struct curl_slist *headers = NULL;
		struct put_data putData;
		dynbuffer *chunk=dynbuffer_init();

		curl_easy_setopt(curl, CURLOPT_URL, (char*)request);

		if (methode)
		{
			curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, methode);
			if (strcmp(methode, "HEAD")==0)
				curl_easy_setopt(curl, CURLOPT_NOBODY, 1L);
			else
				curl_easy_setopt(curl, CURLOPT_NOBODY, 0L);
		}
		else
			curl_easy_setopt(curl, CURLOPT_HTTPGET,1L);

		/* send all data to this function  */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		/* we pass our 'chunk' struct to the callback function */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)chunk);


		if(data_out)
		{
			//debug("set data\n%s\n",data);
			if (!len_out)
				putData.size=strlen(data_out);
			else
				putData.size=len_out;

			putData.data=data_out;
			putData.len=putData.size;

			curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,(curl_off_t)putData.size);
			curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
			curl_easy_setopt(curl, CURLOPT_READDATA,&putData);
			/* enable uploading */
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "");
		}
		else
		{
			curl_easy_setopt(curl, CURLOPT_READDATA, NULL);
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 0L);
			curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,0L);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS,"");
			curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
		}
		if (headers_out)
		{
			cJSON * header=NULL;
			cJSON_ArrayForEach(header, headers_out)
			{
				char *tmp=NULL;
				int aspr=0;
				if (header->valuestring)
				    aspr=asprintf(&tmp, "%s: %s",header->string,header->valuestring);
				else
					aspr=asprintf(&tmp, "%s: ",header->string);
				if (aspr!=-1)
				{
					headers = curl_slist_append(headers, tmp);
					free(tmp);
					tmp=NULL;
				}
			}
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		}
		if (headers_in)
		{
			curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION,curl_headers_cb);
			curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &headers_struct_in);
		}

		res = curl_easy_perform(curl);

		if (headers)curl_slist_free_all(headers);
		headers=NULL;

		long http_code = 0;
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
		if (headers_in)
		{
			*headers_in=headers_struct_in.header;
			headers_struct_in.header=NULL;
		}
		curl_headers_free( &headers_struct_in);

		dynbuffer_pop(&chunk, data_in, len_in);
		dynbuffer_clear(&chunk);


		if (res==0) res=(int)http_code;

		curl_easy_setopt(curl, CURLOPT_READDATA,NULL);
		curl_easy_setopt(curl, CURLOPT_WRITEHEADER, NULL);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION,NULL);
		curl_easy_setopt(curl, CURLOPT_READDATA, NULL);
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 0L);
		curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE,0L);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS,"");
		curl_easy_setopt(curl, CURLOPT_READFUNCTION, NULL);
		curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
		curl_easy_setopt(curl, CURLOPT_URL,NULL);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);

	}
	if (request) free(request);
	request=NULL;
	return res;
}

void print_api_error_stderr(int error)
{
	if (error<200||error>=300)
	{
		{
			if (error<90 && error>0)
			{
				fprintf(stderr, "\n\tERROR:(%d)  %s\n",error,curl_easy_strerror((CURLcode)error));
			}
			else
				switch (error) {
					case 404:
						fprintf(stderr, "\n\tNot Found (HTTP %d)\n",error);
						break;
					case 500:
						fprintf(stderr, "\n\tServer error (HTTP %d)\n",error);
						break;
					case 403:
						fprintf(stderr,"\nForbidden (HTTP %d)\n",error);
						break;
					case 401:
						fprintf(stderr, "\n\tNot Authorised (HTTP %d)\n",error);
						break;
					default:
						fprintf(stderr, "\n\tHttp-Error : HTTP %d\n",error);
						break;
				}
		}
	}
}

const char * print_curl_error(int error)
{
	return (curl_easy_strerror(error));
}
