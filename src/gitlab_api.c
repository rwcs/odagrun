/*
 gitlab_api.c
 Created by Danny Goossen, Gioxa Ltd on 4/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include "deployd.h"

/*
 
 */


/*------------------------------------------------------------------------
 * get_release(void * opaque, cJSON ** release)
 * returns the gitlab cJSON response of:
 * /api/v4/projects/<project id>/repository/tags/<tag>
 * with the CI-token
 * returns 1 on success
 * caller need to free release
 *------------------------------------------------------------------------*/
int get_release(void * opaque, cJSON ** release)
{
   cJSON * env_json= ((data_exchange_t *)opaque)->env->environment;
   CURLcode res;
   *release=NULL;
   CURL *curl = curl_easy_init();
        if(curl)
    {
		curl_easy_setopt(curl, CURLOPT_CAINFO, ((data_exchange_t *)opaque)->parameters->ca_bundle);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, PACKAGE_STRING);
        //build request
		
        //"CI_PROJECT_URL":   "https://gitlab.gioxa.com/deployctl/test_deploy_release",

        char * buildrepo=cJSON_GetObjectItem(env_json, "CI_PROJECT_URL")->valuestring;

        //CI_PROJECT_PATH="gitlab-org/gitlab-ce"
        char * project_path=cJSON_GetObjectItem(env_json, "CI_PROJECT_PATH")->valuestring;
		char * request=NULL;
		//find position of CI_PROJECT_PATH in url
        ssize_t pos = strstr(buildrepo, project_path) - buildrepo;
        if (pos>0)
		{
			request=calloc(1, 0x1000);
       // copy till CI_PROJECT_PATH
            memcpy(request,buildrepo ,pos );
        // and add =>api/v4/projects/{CI_PROJECT_ID}/repository/tags/{CI_COMMIT_SHA}
			snprintf(request+pos,0x1000-pos ,"/api/v4/projects/%s/repository/tags/%s",cJSON_get_key(env_json, "CI_PROJECT_ID"),cJSON_get_key(env_json, "CI_COMMIT_TAG"));
		}
		
        //Done

        debug("\nRequest API: %s\n",request);
        dynbuffer *chunk=dynbuffer_init();
        struct curl_slist *list = NULL;

        curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
        debug("CI_JOB_TOKEN : %s\n",cJSON_get_key(env_json, "CI_JOB_TOKEN"));
        debug("CI_COMMIT_TAG : %s\n",cJSON_get_key(env_json, "CI_COMMIT_TAG"));
       //CI_JOB_TOKEN
        char headertoken[1024];
        snprintf(headertoken,1024, "PRIVATE-TOKEN: %s",cJSON_get_key(env_json, "CI_JOB_TOKEN"));
        debug("-H \"%s\"\n",headertoken);

        list = curl_slist_append(list, headertoken);
        list = curl_slist_append(list, "Content-Type: application/json");
        debug("-H \"Content-Type: application/json\"\n");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
        curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
        /* send all data to this function  */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, chunk);
        res = curl_easy_perform(curl);
        curl_slist_free_all(list);
        if (res!=CURLE_OK )
        {
            debug("\n Failed to get %s\n",request);

            dynbuffer_clear(&chunk);

            if (request) oc_free(&request);
            curl_easy_cleanup(curl);
            return 1;
        }
        long http_code = 0;
        curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
        if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
        {
        //result in chunk
        debug("gitlab_api reply:\n>>>\n%s\n<<<\n",dynbuffer_get(chunk));
        cJSON * tag = cJSON_Parse(dynbuffer_get(chunk));
        if (!tag)
        {
            debug("failed json parse request: -->\n%s\n<--\n",dynbuffer_get(chunk));
            if (request) oc_free(&request);
            curl_easy_cleanup(curl);
            dynbuffer_clear(&chunk);
            return 1;
        }
        *release=tag;

        dynbuffer_clear(&chunk);
        if (request) oc_free(&request);
        curl_easy_cleanup(curl);
        return 0;
      }
      dynbuffer_clear(&chunk);
      if (request) oc_free(&request);
      curl_easy_cleanup(curl);
      return 0;
    }
   return 1;
}


// bug gitlab
static void fix_reset_escape_sequence(char * output_buf,size_t o_read)
{
  char * walk=output_buf;
  walk[o_read]=0; // we read max one char less then buffer size!
  while (1) {
      char *p = strstr(walk,"\033[00m");
      // walked past last occurrence of needle; copy remaining part
      if (p == NULL) break;
      *(p+3)=';'; // change second 0 with ;
      walk=p+1;
  }
}

int  update_details_state(void * userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   cJSON_DeleteItemFromObject(trace->params, "trace");
    return(update_details(userp));
}

int  update_details_null(void * userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   
   cJSON * tmp_trace=cJSON_DetachItemFromObject(trace->params,"trace");
   cJSON_AddStringToObject(trace->params, "trace", ANSI_CROSS_BLACK" temptrace \033[0;m \n\n");
   int response=1;
   dynbuffer * chunk=dynbuffer_init();
	
   CURLcode res;
   if(trace->curl && trace->url && strlen(trace->url)>5)
   {
      curl_easy_setopt(trace->curl, CURLOPT_URL, trace->url);
      char *data=cJSON_Print(trace->params);
      curl_easy_setopt(trace->curl, CURLOPT_POSTFIELDS, data);
      
      // add JSON header
      struct curl_slist *headers = NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      headers = curl_slist_append(headers, "accept: application/json");
      curl_easy_setopt(trace->curl, CURLOPT_HTTPHEADER, headers);
      // change request to PUT
      curl_easy_setopt(trace->curl, CURLOPT_CUSTOMREQUEST, "PUT");
      
      /* send all data to this function  */
      curl_easy_setopt(trace->curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
      /* we pass our 'chunk' struct to the callback function */
      curl_easy_setopt(trace->curl, CURLOPT_WRITEDATA, chunk);
      /* complete within 20 seconds */
      curl_easy_setopt(trace->curl, CURLOPT_TIMEOUT, 20L);
      // Do request
      res = curl_easy_perform(trace->curl);
      curl_slist_free_all(headers);
      long http_code = 0;
      curl_easy_getinfo (trace->curl, CURLINFO_RESPONSE_CODE, &http_code);
      if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
      {
         //Succeeded
         //printf("DONE PUT\n");
         if(strcmp(dynbuffer_get(chunk),"true")==0 || strcmp(dynbuffer_get(chunk),"null")==0)
            response=0;
         if(strcmp(dynbuffer_get(chunk),"false")==0)
            response=-1;
         if (response ) debug( "update job #%10d: http 200, reply \"true\": >%s< \n",trace->job_id,dynbuffer_get(chunk));
         else debug( "update job #%10d: http 200, no reply \"true\": >%s< \n",trace->job_id,dynbuffer_get(chunk));
         
      }
      else
      {
         if (res != CURLE_OK && trace->url) error( "update job #%10d: failed: %s\n",trace->job_id,
                                    curl_easy_strerror(res));
         else if (trace->url)
         {
            error( "update job #%10d: HTTP %ld feedback: %s\n",trace->job_id,http_code,dynbuffer_get(chunk));
         }
         response=1;
      }
      
      if (data) oc_free(&data);
      curl_easy_setopt(trace->curl, CURLOPT_WRITEDATA, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_WRITEFUNCTION,NULL);
      curl_easy_setopt(trace->curl, CURLOPT_HTTPPOST, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_HTTPHEADER, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_POSTFIELDS, NULL);
   }
   dynbuffer_clear(&chunk);
   cJSON_ReplaceItemInObject(trace->params, "trace", tmp_trace);
   return response;
}


int  update_details(void * userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   
    char * tmp_trace=cJSON_get_stringvalue_as_var(trace->params,"trace");
     if (tmp_trace) fix_reset_escape_sequence(tmp_trace,strlen(tmp_trace));
  // ssize_t n=trace->prev_len-strlen(tmp_trace);
 /*
   if (n==0)
   {
      Write_dyn_trace(trace, , ".");
   }
 
   else if (n>0)
   {
      int i;
      for (i=0;i<=n+1;i++) Write_dyn_trace(trace, none, ".");
      Write_dyn_trace(trace, yellow, "\n");
      debug("trace was smaller, adding %d\n",n+1);
   }
   */
  // const char myspinner[]=" *=-/|\\-0123456789";
  // tmp_trace=cJSON_get_key(trace->params,"trace");
  // tmp_trace[0]=myspinner[trace->count];
  // tmp_trace[8]=myspinner[trace->count];
  // trace->prev_len=strlen(tmp_trace);
   int response=1;
   dynbuffer* chunk=dynbuffer_init();

  ;
   CURLcode res;
	if(trace->curl && trace->url && strlen(trace->url)>5)
   {
      curl_easy_setopt(trace->curl, CURLOPT_URL, trace->url);
      char *data=cJSON_Print(trace->params);
      curl_easy_setopt(trace->curl, CURLOPT_POSTFIELDS, data);

      // add JSON header
      struct curl_slist *headers = NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      headers = curl_slist_append(headers, "accept: application/json");
      curl_easy_setopt(trace->curl, CURLOPT_HTTPHEADER, headers);
      // change request to PUT
      curl_easy_setopt(trace->curl, CURLOPT_CUSTOMREQUEST, "PUT");

      /* send all data to this function  */
      curl_easy_setopt(trace->curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
      /* we pass our 'chunk' struct to the callback function */
      curl_easy_setopt(trace->curl, CURLOPT_WRITEDATA, chunk);
      /* complete within 20 seconds */
      curl_easy_setopt(trace->curl, CURLOPT_TIMEOUT, 20L);
      // Do request
      res = curl_easy_perform(trace->curl);
      curl_slist_free_all(headers);
      long http_code = 0;
      curl_easy_getinfo (trace->curl, CURLINFO_RESPONSE_CODE, &http_code);
      if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
      {
         //Succeeded
         //printf("DONE PUT\n");
         if(strcmp(dynbuffer_get(chunk),"true")==0 || strcmp(dynbuffer_get(chunk),"null")==0)
            response=0;
         if(strcmp(dynbuffer_get(chunk),"false")==0)
            response=-1;
         if (response) debug( "update job #%10d: http 200, reply \"true\": >%s< \n",trace->job_id,dynbuffer_get(chunk));
         else debug( "update job #%10d: http 200, no reply \"true\": >%s< \n",trace->job_id,dynbuffer_get(chunk));
      
      }
      else
      {
         if (res != CURLE_OK) error( "update job #%10d: failed: %s\n",trace->job_id,
                        curl_easy_strerror(res));
         else
         {
            error( "update job #%10d: HTTP %ld feedback: %s\n",trace->job_id,http_code,dynbuffer_get(chunk));
         }
         response=1;
      }

      if (data) oc_free(&data);
      curl_easy_setopt(trace->curl, CURLOPT_WRITEDATA, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_WRITEFUNCTION,NULL);
      curl_easy_setopt(trace->curl, CURLOPT_HTTPPOST, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_HTTPHEADER, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_POSTFIELDS, NULL);
   }
   dynbuffer_clear(&chunk);
   return response;
}


// Progress of artifacts download
// set's data->progress to 1 if an update
// set's data->artifact json key progress int % progress value.
// scan_artifacts_update will use artifact
// scan progress will use data-> porgress
static int progress_update_arti(void *userp,  curl_off_t dltotal, curl_off_t dlnow,
                         __attribute__((unused)) curl_off_t ultotal, __attribute__((unused)) curl_off_t ulnow)
{
   struct data_thread *data = (struct data_thread *)userp;
   CURL *curl = data->curl;
   double curtime = 0;
   curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &curtime);
   double lastruntime= data->lastruntime;
   if((curtime - lastruntime) >= 10)
   {
      data->lastruntime = curtime;
      debug( "DL TIME: %f => %d / %d \r\n", curtime,(int)dlnow,(int) dltotal);
      int progress=-1;
      if ((int) dltotal !=0)
      {
            progress=(100LL*(int)dlnow)/dltotal;
      }
      cJSON_GetObjectItem(data->artifact, "progress")->valueint=progress;
      data->progress=1;
   }
   return 0;
}

// for curl version < 0x072000 (7.32)
static int progress_update_arti_oldcurl(void *userp,   double dltotal,   double dlnow,   double ultotal,   double ulnow)
{
   return progress_update_arti(userp, (curl_off_t)dltotal,
                               (curl_off_t)dlnow,
                               (curl_off_t)ultotal,
                               (curl_off_t)ulnow);
}

void *get_artifact(void *userp)
{
   FILE *fp;
   CURL *curl=NULL;
   struct data_thread *data = (struct data_thread *)userp;
   if (!data) {printf("no data structure to pull one url\n"); return 0;}
   else
      debug("start download %s\n",cJSON_get_key(data->artifact, "name"));
   if (data->error_count==0)
      cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("download"));
   else
      cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("retrying"));
   curl = curl_easy_init();

   if (curl)
   {
      data->lastruntime = 0;
      data->curl = curl;
	   
	   curl_easy_setopt(curl, CURLOPT_CAINFO, data->ca_bundle);
	   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
	   curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	   curl_easy_setopt(curl, CURLOPT_USERAGENT, PACKAGE_STRING);
      curl_easy_setopt(curl, CURLOPT_URL, cJSON_get_key(data->artifact, "api"));
      debug("url_artifact: %s ,headerAuth: %s\n",cJSON_get_key(data->artifact, "api"),cJSON_get_key( data->artifact, "header_auth"));
      struct curl_slist *headers = NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      headers = curl_slist_append(headers, cJSON_get_key( data->artifact, "header_auth"));
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

      curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
      // Enable intruption for gracefull shutdown
      curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, &progress_update_arti_oldcurl);
      curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, userp);
   #if LIBCURL_VERSION_NUM >= 0x072000
       curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_update_arti);
       curl_easy_setopt(curl, CURLOPT_XFERINFODATA, userp);
   #endif
      /* enable progress meter */
      curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);

      int res=1;
      char filename[1024];
      char *path=data->path;
      char * name=cJSON_get_stringvalue_as_var(data->artifact, "name");
      if (!name || !path) error("no name or path %p %p",name,path);

      snprintf(filename,1024, "%s/%s.zip",path, name);
      debug("curl download to %s\n",filename);

      long http_code = 0;
      fp = fopen(filename,"wb");
      if (fp==NULL) error("ERR: curl pull %s prob %s\n",filename,strerror(errno));
      else
      {
         //curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, 0);
         curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

         res= curl_easy_perform(curl); /* ignores error */
         fclose(fp);
         curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
         data->http_code=http_code;
      }
      curl_slist_free_all(headers);
      if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
      {
         cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("done"));
         debug("Finish download %s\n",cJSON_get_key(data->artifact, "name"));
      }
      else
      {
         data->error=1;
         if (res)
            debug( "curl failed for %s : %s\n",cJSON_get_key(data->artifact, "name"),curl_easy_strerror(res));
         else {
            debug("HTTP_code: %ld for %s\n",data->http_code,cJSON_get_key(data->artifact, "name"));
         }
         cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("error"));
      }
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);
      curl_easy_setopt(curl, CURLOPT_HTTPPOST, NULL);
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, NULL);
      curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
   #if LIBCURL_VERSION_NUM >= 0x072000
         curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, NULL);
         curl_easy_setopt(curl, CURLOPT_XFERINFODATA, NULL);
   #endif
      curl_easy_cleanup(curl);
      data->finish=1;
      return NULL;
   }
   else
   {
      data->error=1;
      cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("error"));
      error("No Curl init\n");
      return NULL;
   }
}

int upload_artifact(cJSON * job, char * upload_name,const char * ca_bundle)
{
	char *url=NULL;
	int asr=asprintf(&url,"%s/api/v4/jobs/%d/artifacts",cJSON_get_key(job, "ci_url"),cJSON_GetObjectItem(job, "id")->valueint);
	if (asr==-1) return -1;
	CURL *curl = curl_easy_init();
	if (!curl)
	{
		oc_free(&url);
		return -1;
	}
   dynbuffer * chunk=dynbuffer_init();
   if (!chunk)
   {
	   oc_free(&url);
	   curl_easy_cleanup(curl);
	   return -1;
   }
   debug("\n****************************************************************\nUpLoad artifacts\n\n");
	
   curl_easy_setopt(curl, CURLOPT_URL, url);

	char JOB_TOKEN[1024];
	snprintf(JOB_TOKEN,1024,"JOB-TOKEN: %s",cJSON_get_key( job, "token"));
	struct curl_slist *headers = NULL;
	// headers = curl_slist_append(headers, "Content-Type: application/json");
	headers = curl_slist_append(headers, JOB_TOKEN);
	headers = curl_slist_append(headers, "expect:");
	headers = curl_slist_append(headers, "accept: application/json");

	curl_easy_setopt(curl, CURLOPT_CAINFO, ca_bundle);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
	curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
	curl_easy_setopt(curl, CURLOPT_USERAGENT, PACKAGE_STRING);

	struct curl_httppost *post=NULL;
	struct curl_httppost *last=NULL;
	cJSON * artifacts= cJSON_GetArrayItem( cJSON_GetObjectItem(job,"artifacts"),0);

	if (cJSON_get_key(artifacts,"expire_in"))
	{
		debug("expire_in %s\n",cJSON_get_key(artifacts,"expire_in"));
		curl_formadd(&post, &last,
			CURLFORM_COPYNAME, "expire_in",
			CURLFORM_COPYCONTENTS, cJSON_get_key(artifacts,"expire_in"),
			CURLFORM_END);
	}
	else
	{
		debug("expire_in [default] 1w\n");
		curl_formadd(&post, &last,
			CURLFORM_COPYNAME, "expire_in",
			CURLFORM_COPYCONTENTS, "1w",
			CURLFORM_END);
	}
 //curl_formadd(&post, &last,
//            CURLFORM_COPYNAME, "file",
//            CURLFORM_COPYCONTENTS, "artifacts.zip", CURLFORM_END);

	char * filename=NULL;
	int aspr=-1;
	if (cJSON_get_key(artifacts,"name"))
	{
	  aspr=asprintf(&filename,"%s.zip",cJSON_get_key(artifacts,"name"));
	}
	else
	{
	  aspr=asprintf(&filename,"artifacts.zip");
	}
	int result_f=0;
	if (aspr!=-1)
	{
		debug("filename=%s\n",filename);
		curl_formadd(&post, &last,
					CURLFORM_COPYNAME, "file",
					CURLFORM_CONTENTTYPE, "application/zip",
					CURLFORM_FILE,  upload_name,
					CURLFORM_FILENAME,filename, CURLFORM_END);

				  /* Set the form info */
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

		long http_code = 0;

		/* send all data to this function  */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
		/* we pass our 'chunk' struct to the callback function */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, chunk);
		/* complete within 20 seconds */
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 1200L);

		int res= curl_easy_perform(curl);
		
		/* free the post data again */
		curl_formfree(post);
		curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

		if ((http_code == 201 ||  http_code == 200)&& res == CURLE_OK)
		{
		  //debug("return data: %s\n",dynbuffer_get(chunk));
		  result_f=0;
		}
		else
		{
		 result_f=res;
		  if (res)
			 error("Upload failed ");
		  else {
			 error("HTTP_code: %ld for upload artifact \n",http_code);
			 error("return data: %s\n",dynbuffer_get(chunk));
			 result_f=(int)http_code;
		  }
		}
	}
	else
		result_f=-1;
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, NULL);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
	oc_free(&filename);
	//if (params_str) oc_free(&params_str);
	curl_slist_free_all(headers);
	dynbuffer_clear(&chunk);
	curl_easy_cleanup(curl);
	return result_f;
}
