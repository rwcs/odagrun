//
//  docker_config.c
//  odagrun
//
//  Created by Danny Goossen on 26/6/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#include "../src/deployd.h"
#include "../src/cJSON_deploy.h"
#include "../src/yaml2cjson.h"

#include "docker_config.h"

/** Enumeration of docker config data types */
enum dkr_types{
	/** docker boolean */
	dkr_bool,
	/** docker string */
	dkr_string,
	/** docker struct in form of `value: {}` */
	dkr_struct,
	/** docker config string array */
	dkr_string_array,
	/** \brief docker config Env array, in form of [ "VARNAME=<value>",...]
	 
	 Main diference towards dkr_string_array is specific merging of environment variables.
	 
	 
	 1. a value of `null` will delete all items on merge
	 
	 2. ["_*: null","TEST=123"] will Delete all value's and defines new ones
	 
	 3. ["TEST"] will DELETE the environment variable `TEST` if it was present
	 
	 4. while ["TEST="] will set and replace of present the enviroment value to ""
	 
	 5. and [TEST=123] will set or overide a previous value
	 
	 */
	dkr_string_array_env,
	/** docker config integer type */
	dkr_integer,
	/** \brief docker config "key: value" type
	 
write as
\code {"key/value-item": {{"key": "value"},...}}\endcode
	 or
	 \code #yaml
item:
  key: value
  key:
	 \endcode
	 
# On Merge
Old value's are merged,deleted or overwriten.
	 
1. Set item as \code {"key/value-item" : null } \endcode and  all old key:values are deleted and the key/value-item is set to null.
	 
2. set key/value with \code {"key/value-item": {{"_*": null },{"key": "value"},...}}\endcode to delete all key:value's and append new values on Merge
	 
3. set an key/value to \code {"key/value-item": {{"key": null },...}}\endcode to delete that key on Merge
	 
4. set 
	 \code #yaml
key/value-item:
  key:
\endcode will set \code{"key" : ""}\endcode after merge, no delete!!
	 
	 */
	dkr_key_value
};

#define docker_config_item_cnt 14
const char * docker_config_key[docker_config_item_cnt] = {"ExposedPorts","Entrypoint"     ,"Volumes"  ,"WorkingDir","Cmd"             ,"Labels"       ,"User"     ,"Env"      ,"Memory"     ,"MemorySwap" ,"CpuShares" ,"OnBuild","ArgsEscaped","Image"};
enum dkr_types docker_config_type[docker_config_item_cnt]={dkr_struct   ,dkr_string_array ,dkr_struct ,dkr_string  , dkr_string_array , dkr_key_value ,dkr_string , dkr_string_array_env , dkr_integer , dkr_integer ,dkr_integer ,dkr_string_array,dkr_bool,dkr_string};

/**
 \brief generates the default's values for a given type
 */
cJSON * dkr_getdefault_type(enum dkr_types type);

cJSON * docker_config_get_default_config(void)
{
	cJSON * result=cJSON_CreateObject();
	int i=0;
	for (i=0;i<docker_config_item_cnt;i++)
	{
		cJSON * item= dkr_getdefault_type(docker_config_type[i]);
		if (item)
		cJSON_AddItemToObject(result,docker_config_key[i],item);
	}
	return result;
}

cJSON * process_docker_config(const cJSON * config)
{
	//swoop keys, if not present use defs
	int i=0;
	cJSON * result=NULL;
	for (i=0;i<docker_config_item_cnt;i++)
	{
		cJSON * resultItem=NULL;
		cJSON * tt=cJSON_GetObjectItem(config, docker_config_key[i]);
		if (tt)
		{
			switch  (docker_config_type[i])
			{
				case dkr_bool:
				{
					if(cJSON_IsBool(tt))
						resultItem=cJSON_CreateBool(cJSON_IsTrue(tt));
					else if(cJSON_IsNull(tt)) resultItem=cJSON_CreateNull();
				}
					break;
				case dkr_struct:
				{
					cJSON * item;
					resultItem=NULL;
					if (cJSON_IsString(tt))
					{
						if(!resultItem) resultItem=cJSON_CreateObject();
						cJSON_AddItemToObject(resultItem, tt->valuestring,cJSON_CreateObject());
					}
					else if (cJSON_IsNumber(tt))
					{
						char * key=NULL;
						int aspr=asprintf(&key, "%d",tt->valueint);
						if (aspr!=-1 && key)
						{
							if(!resultItem) resultItem=cJSON_CreateObject();
							cJSON_AddItemToObject(resultItem, key,cJSON_CreateObject());
							oc_free(&key); key=NULL;
						}
					}
					else if (cJSON_IsArray(tt))
					{
						cJSON_ArrayForEach(item, tt)
						{
							
							if (item->valuestring)
							{
								if(!resultItem) resultItem=cJSON_CreateObject();
								cJSON_AddItemToObject(resultItem,item->valuestring,cJSON_CreateObject());
							}
							else if (cJSON_IsNumber(item))
							{
								char * key=NULL;
								int aspr=asprintf(&key, "%d",item->valueint);
								if (aspr !=-1 && key)
								{
									if(!resultItem) resultItem=cJSON_CreateObject();
									cJSON_AddItemToObject(resultItem, key,cJSON_CreateObject());
									oc_free(&key); key=NULL;
								}
							}
							else if (cJSON_IsNumber(item))
							{
								char * key=NULL;
								int aspr=asprintf(&key, "%d",item->valueint);
								if (aspr!=-1&& key)
								{
									if(!resultItem) resultItem=cJSON_CreateObject();
									cJSON_AddItemToObject(resultItem, key,cJSON_CreateObject());
									oc_free(&key); key=NULL;
								}
							}
							else if (cJSON_IsObject(item) && cJSON_IsNull(item->child))
							{
								if(!resultItem) resultItem=cJSON_CreateObject();
								cJSON_AddItemToObject(resultItem,item->child->string,cJSON_CreateNull());
							}
						}
					}else if (cJSON_IsObject(tt))
					{
						cJSON_ArrayForEach(item, tt)
						{
							if(!resultItem) resultItem=cJSON_CreateObject();
							if (item->string && !cJSON_IsNull(item))
								cJSON_AddItemToObject(resultItem, item->string,cJSON_CreateObject());
							else if (item->string && cJSON_IsNull(item))
								cJSON_AddItemToObject(resultItem,item->string,cJSON_CreateNull());
						}
					}
					else if(cJSON_IsNull(tt))
						resultItem=cJSON_CreateNull();
				}
					break;
				case dkr_string:
				{
					if (cJSON_IsString(tt) && tt->valuestring) resultItem=cJSON_CreateString(tt->valuestring);
					else if(cJSON_IsNull(tt)) resultItem=cJSON_CreateNull();
					
				}
					break;
				case dkr_string_array:
				{
					// validation??? use of null to delete item???
					if (cJSON_IsArray(tt))
					{
						cJSON * item=NULL;
						cJSON_ArrayForEach(item, tt)
						{
							if (cJSON_IsString(item))
							{
								if(!resultItem) resultItem=cJSON_CreateArray();
								cJSON_AddItemToArray(resultItem, cJSON_CreateString(item->valuestring));
								
							}
							if (cJSON_IsObject(item) && cJSON_IsNull(item->child))
							{
								if(!resultItem) resultItem=cJSON_CreateArray();
								cJSON_AddItemToArray(resultItem, cJSON_Duplicate(item, 1));
							}
							
						}
					}
					else if(cJSON_IsNull(tt)) resultItem=cJSON_CreateNull();
				}
					break;
				case dkr_string_array_env:
				{
					// validation??? use of null to delete item???
					if (cJSON_IsArray(tt))
					{
						cJSON * item=NULL;
						cJSON_ArrayForEach(item, tt)
						{
							if (cJSON_IsString(item))
							{
								if(!resultItem) resultItem=cJSON_CreateArray();
								cJSON_AddItemToArray(resultItem, cJSON_CreateString(item->valuestring));
								
							}
							if (cJSON_IsObject(item) && cJSON_IsNull(item->child))
							{
								if(!resultItem) resultItem=cJSON_CreateArray();
								cJSON_AddItemToArray(resultItem, cJSON_Duplicate(item, 1));
							}
							
						}
					}
					else if(cJSON_IsNull(tt)) resultItem=cJSON_CreateNull();
				}
					break;
				case dkr_key_value:
				{
					if (cJSON_IsObject(tt))
					{
						cJSON * item=NULL;
						cJSON_ArrayForEach(item, tt)
						{
							// todo could be Number or Bool !!!!
							if ( item->valuestring && item->string)
							{
								if (!resultItem) resultItem=cJSON_CreateObject();
								
									cJSON_AddStringToObject(resultItem, item->string, item->valuestring);
							}
							else if(cJSON_IsNumber(item))
							{
								char * key=NULL;
								int aspr=asprintf(&key, "%d",item->valueint);
								if (aspr!=-1 && key)
								{
									if (!resultItem) resultItem=cJSON_CreateObject();
									cJSON_AddStringToObject(resultItem, item->string, key);
									oc_free(&key); key=NULL;
								}
							}
							else if(cJSON_IsTrue(item))
							{
								if (!resultItem) resultItem=cJSON_CreateObject();
								cJSON_AddStringToObject(resultItem, item->string, "True");
							}
							else if(cJSON_IsFalse(item))
							{
								if (!resultItem) resultItem=cJSON_CreateObject();
								cJSON_AddStringToObject(resultItem, item->string, "False");
							}
							else if(cJSON_IsNull(item))
							{
								if (!resultItem) resultItem=cJSON_CreateObject();
								cJSON_AddNullToObject(resultItem, item->string);
							}
						}
					}
					else
						if(cJSON_IsNull(tt)) resultItem=cJSON_CreateNull();
				}
					break;
				case dkr_integer:
				{
					if(cJSON_IsNumber(tt))
						resultItem=cJSON_CreateNumber(tt->valueint);
					else if(cJSON_IsNull(tt)) resultItem=cJSON_CreateNull();
				}
					break;
			}
		}
		if (resultItem)
		{
			if (!result) result=cJSON_CreateObject();
			cJSON_AddItemToObject(result, docker_config_key[i], resultItem);
		}
	}
	return result;
}

int docker_merge_config(cJSON ** old, const cJSON * new)
{
	int i=0;
	for (i=0;i<docker_config_item_cnt;i++)
	{
		
		cJSON * old_item=NULL;
		cJSON * new_item=cJSON_GetObjectItem(new, docker_config_key[i]);
		if (old && *old)
			old_item=cJSON_GetObjectItem(*old, docker_config_key[i]);
		cJSON * result_item=old_item;
		if (new_item)
		{
			switch  (docker_config_type[i])
			{
				case dkr_struct:
				{
					cJSON * item;
					if (cJSON_IsObject(new_item))
					{
						cJSON_ArrayForEach(item, new_item)
						{
							
							if (result_item &&  cJSON_IsNull(item) )
							{
								if (cJSON_GetObjectItem(result_item,item->string ))
									cJSON_DeleteItemFromObject(result_item, item->string);
								else if (strcmp(item->string,"_*")==0)
								{
									if (old_item)
										cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
									old_item=NULL;
									result_item=cJSON_CreateNull();
								}
							}
							else if (cJSON_IsString(item) || (cJSON_IsObject(item) && !cJSON_IsNull(item) ))
							{
								if(!result_item) result_item=cJSON_CreateObject();
								
								if (item->string && !cJSON_GetObjectItem(result_item,item->string ))
									cJSON_AddItemToObject(result_item, item->string,cJSON_CreateObject());
								if (result_item && cJSON_IsNull(result_item))
									result_item->type=cJSON_Object;
							}
						}
					}
					else if (cJSON_IsNull(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
						old_item=NULL;
						result_item=cJSON_CreateNull();
					}
				}
					break;
				case dkr_string:
				{
					if (cJSON_IsString(new_item) && new_item->valuestring)
					{
						if (!result_item)
							result_item=cJSON_CreateString(new_item->valuestring);
						else
						{
							if (result_item->valuestring)
								oc_free(&result_item->valuestring);
							result_item->valuestring=strdup(new_item->valuestring);
							if (result_item && cJSON_IsNull(result_item))
								result_item->type=cJSON_String;
						}
						
					}
					else if (cJSON_IsNull(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
					}
				}
					break;
				case dkr_string_array_env:
				{
					cJSON * sub_item=NULL;
					if (cJSON_IsArray(new_item))
					{
						cJSON_ArrayForEach(sub_item, new_item)
						{
							if (sub_item->valuestring && !cJSON_IsNull(sub_item) && strchr(sub_item->valuestring,'='))
							{
								if (!old_item)
								{
									if (!result_item) result_item=cJSON_CreateArray();
									cJSON_AddItemToArray(result_item, cJSON_CreateString( sub_item->valuestring));
								}
								else
								{
									// check if array not exists
									cJSON * o_ob=NULL;
									int match=-1;
									int cnt=0;
									cJSON_ArrayForEach(o_ob, old_item)
									{
										char *es=strchr(sub_item->valuestring,'=');
										size_t cmp_len=strlen(sub_item->valuestring);
										if (es) cmp_len=es-sub_item->valuestring;
										if (strncmp(sub_item->valuestring,o_ob->valuestring,cmp_len)==0)
										{
											match=cnt;
											break;
										}
										cnt++;
									}
									if (match==-1)
									{
										if (result_item && cJSON_IsNull(result_item))
											result_item->type=cJSON_Array;
										cJSON_AddItemToArray(result_item, cJSON_CreateString( sub_item->valuestring));
									}
									else
										cJSON_ReplaceItemInArray(result_item, match, cJSON_CreateString( sub_item->valuestring));
								}
							}
							else if (cJSON_IsNull(sub_item->child) && result_item && sub_item->child->string && strcmp(sub_item->child->string,"_*")==0)
							{
								if (old_item)
									cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
								old_item=NULL;
								result_item=cJSON_CreateArray();
							}
							else if ((cJSON_IsNull(sub_item->child) || ( sub_item->valuestring &&  !strchr(sub_item->valuestring,'=') ) ) && result_item && cJSON_IsArray(result_item))
							{
								// check if array not exists
								cJSON * o_ob=NULL;
								int match=-1;
								int cnt=0;
								cJSON_ArrayForEach(o_ob, old_item)
								{
									char *es=NULL;
									if (cJSON_IsObject(sub_item) && sub_item->child->string)
									{
										size_t cmp_len=strlen(sub_item->child->string);
										es=strchr(sub_item->child->string,'=');
										if (es) cmp_len=es-sub_item->child->string;
										if (strncmp(sub_item->child->string,o_ob->valuestring,cmp_len)==0)
										{
											match=cnt;
											break;
										}
										
									}
									else if (sub_item->valuestring)
									{
										size_t cmp_len=strlen(sub_item->valuestring);
										es=strchr(sub_item->valuestring,'=');
										if (es) cmp_len=es-sub_item->valuestring;
										if (strncmp(sub_item->valuestring,o_ob->valuestring,cmp_len)==0)
										{
											match=cnt;
											break;
										}
									}
									
									cnt++;
								}
								if (match!=-1) cJSON_DeleteItemFromArray(result_item, match);
							}
							
						}
					}
					else if (cJSON_IsNull(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
						old_item=NULL;
						result_item=cJSON_CreateNull();
					}
				}
					break;
				case dkr_string_array:
				{
					cJSON * sub_item=NULL;
					if (cJSON_IsArray(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
						old_item=NULL;
						result_item=NULL;
						cJSON_ArrayForEach(sub_item, new_item)
						{
							if (sub_item->valuestring)
							{
								if (sub_item->valuestring)
								{
									if (!result_item) result_item=cJSON_CreateArray();
									else if (cJSON_IsNull(result_item)) result_item->type=cJSON_Array;
									cJSON_AddItemToArray(result_item, cJSON_CreateString( sub_item->valuestring));
								}
							}
						}
					}
					else if (cJSON_IsNull(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
						old_item=NULL;
						result_item=cJSON_CreateNull();
					}
				}
					break;
				case dkr_key_value:
				{
					if (cJSON_IsObject(new_item) && new_item->string)
					{
						cJSON * sub_item=NULL;
						cJSON_ArrayForEach(sub_item, new_item)
						{
							if (result_item && cJSON_IsNull(sub_item))
							{
								if (sub_item->string && strcmp(sub_item->string,"_*")==0)
								{
									if (old_item)
										cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
									old_item=NULL;
									result_item=NULL;
								}
								else
								{
									cJSON * tt=cJSON_GetObjectItem(result_item, sub_item->string);
									if (tt) cJSON_DeleteItemFromObject(result_item, sub_item->string);
								}
							}
							else if (!cJSON_IsNull(sub_item))
							{
								if (! result_item)result_item=cJSON_CreateObject();
								else if (cJSON_IsNull(result_item)) result_item->type=cJSON_Object;
								cJSON_safe_addString2Obj(result_item, sub_item->string, sub_item->valuestring);
							}
						}
					}
					else if (cJSON_IsNull(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
					}
				}
					break;
				case dkr_integer:
				{
					if(cJSON_IsNumber(new_item))
					{
						if (!result_item)
							result_item=cJSON_CreateNumber(new_item->valueint);
						else
							result_item->valueint=new_item->valueint;
						
					}
					else if (cJSON_IsNull(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
						
					}
					
				}
					break;
				case dkr_bool:
				{
					if(cJSON_IsBool(new_item))
					{
						if (!result_item)
							result_item=cJSON_CreateBool(cJSON_IsTrue(new_item));
						else
							result_item->type=new_item->type;
						
					}
					else if (cJSON_IsNull(new_item))
					{
						if (old_item)
							cJSON_DeleteItemFromObject(*old, docker_config_key[i]);
						
					}
					
				}
					break;
			}
			if (result_item)
			{
				if (old && !*old) *old=cJSON_CreateObject();
				if (old && !old_item)
					cJSON_AddItemToObject(*old, docker_config_key[i], result_item);
			}
		}
		
	}
	
	return 0;
}

cJSON * dkr_getdefault_type(enum dkr_types type)
{
	cJSON * result=NULL;
	switch(type){
		case dkr_string_array:
			result=cJSON_CreateArray();
			break;
		case dkr_string_array_env:
			result=cJSON_CreateArray();
			break;
		case dkr_integer: result=NULL;
			break;
		case dkr_struct:
			result=cJSON_CreateObject();
			break;
		case dkr_string:
			result=cJSON_CreateString("");
			break;
		case dkr_key_value:
			result=cJSON_CreateObject();
			break;
		case dkr_bool: result=NULL;
			break;
	}
	return result;
}





