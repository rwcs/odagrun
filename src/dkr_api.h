/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file dkr_api.h
 *  \brief Header file for docker api
 *  \author Created by Danny Goossen on 10/10/17.
 *  \copyright Copyright (c) 2017 Danny Goossen. All rights reserved.
 *  \code{.c}
 #include "dkr_api.h"

 struct dkr_api_data_s * apidata=NULL;
 dkr_api_init(&apidata, "testci","pKrcnmsdlkfderl", NULL, NULL);
 
 cJSON * result_data=NULL;
 dkr_api(apidata, "GET", "https://dockregi.gioxa.com/v2/deployctl/oc-image/manifests/latest", NULL, NULL, &result_data, NULL, 1);
 if (result_data)
 {
 char *printit=cJSON_Print(result_data);
 printf("request1: \n%s\n",printit);
 oc_free(&printit);
 }
 
 //-> apidata->Basic_auth:  "Basic base64....."
 // => apidata->Bearer_auth: "Bearer 2k3n......"
 // => apidata-> scope: scope of the Bearer Auth
 
 dkr_api_cleanup(&apidata);
 \endcode
 \todo extend example
 \bug can't deal yet with secured ImageStream due to need of different ca's for registry api(service ca) and oc_authorisation with ca_bundle....???!!!
 */

#ifndef __odagrun__dkr_api__
#define __odagrun__dkr_api__

#include "common.h"

#define DOCKER_REGISTRY "registry.hub.docker.com"
#define QUAY_REGISTRY "quay.io"
#define DOCKER_HUB "hub.docker.com"


/**
 \brief api_data the docker api data structure
 \todo add mutex for registry updates in the database for parallel actions(pulling multiple layers in parallel)
 */
struct dkr_api_data_s {
    CURL * curl;              /**< CURL connection */
    cJSON * registries;       /**< the database of the registries */
    char * default_ca_bundle; /**< the default ca_cert bundle */
    void * test; /**< optional for testing */
    //  add mutext for registries
    // individual request data
};

/**
 * \brief init the docker api
 * \param api_data the data struture is created and initialized and returned
 * \param ca_cert_bundle the default ca_bundle to use
 * \return 0 on success
 * \note apidata should be NULL
 */
int dkr_api_init(struct dkr_api_data_s ** api_data, const char *ca_cert_bundle);

/**
 * \brief cleanup the api data
 * \param api_data is *api_data will be NULL when returned
 */
void dkr_api_cleanup(struct dkr_api_data_s ** api_data);

/**
 \brief adds a new registry to the database
  if user = api_token then auth is stored as Bearer_auth
 \returns 0 on success??
 \todo figur this out
 */
int dkr_api_add_registry(struct dkr_api_data_s * api_data, const char * registry_name, const char * namespace ,const char * user,const char * pwd, const char *Bearer_auth, int insecure);

/**
 \brief checks if we got a registry V2
 \param api_data the docker api data structure
 \param registry_name domain name of the registry
 \returns 1 on success
 */
int dkr_api_check_registry(struct dkr_api_data_s * api_data, const char * registry_name);

/**
 \brief checks if manifest exists on remote registry
 \param dkr_api_data the docker api data structure
 \param image domain name of the registry
 \returns 1 on success
 */
int dkr_api_manifest_exists( struct dkr_api_data_s * dkr_api_data ,const char * image,char ** image_sha, char ** tag);

int dkr_api_get_manifest( struct dkr_api_data_s * dkr_api_data ,const char * accept,const char * image, cJSON ** result);
int dkr_api_get_manifest_raw( struct dkr_api_data_s * dkr_api_data ,const char * accept,const char * image, char** result);
int dkr_api_get_manifest_2_file( struct dkr_api_data_s * dkr_api_data ,const char * accept,const char * image, const char * filepath,...);

//int dkr_api_pull_layer(struct dkr_api_data_s * dkr_api_data,int ref,const char *registry_api,const char *name,const char *blobSum, const char * filename);

int dkr_api_blob_check(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum);
int dkr_api_blob_get(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, char ** result);
int dkr_api_blob_get_file(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, const char * filename,...);
int dkr_api_blob_push_raw(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, const char *blob,size_t size);
int dkr_api_blob_push(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum, const char *filepath,...);
int dkr_api_manifest_push(struct dkr_api_data_s * dkr_api_data,const char *image,const char *tag, const char *filepath,...);
int dkr_api_manifest_push_raw(struct dkr_api_data_s * dkr_api_data, const char *image,const char *tag,char *manifest, int V2);

int dkr_api_manifest_delete(struct dkr_api_data_s * dkr_api_data, char **image,const char* manifest_digest);
/**
 \brief get registry struct and number from internal database
 \param registries the data base with Basic auth/CA_bundle/name .. etc per registry that is added
 \param number return the registry id from the database (array item)
 \param registry_name pointer to string, if not NULL, will contain string with result, caller is responsible for freeing
 \param namespace pointer to string, if not NULL, will contain string with result, caller is responsible for freeing
 \param tag_reference pointer to string, if not NULL, will contain string with result, caller is responsible for freeing
 \param protocol pointer to string,(https:// or http:// ) if not NULL, will contain string with result, caller is responsible for freeing
 \note Caller is responsible for freeing returned registry pointers
 \returns the registry info for the given \b name
 */
const cJSON * dkr_lookup_registry(const cJSON * registries,const char * image,int * number, char ** registry_name,char** namespace, char ** tag_reference, char ** protocol);

/**
 \brief get url to push blob
 \returns 202 and url on success
*/
int dkr_api_blob_push_get_url(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum,char ** url,char ** Auth,int *reg);
/**
 \brief get url to pull blob
 \returns 0 on success
 \todo figure out what to do if no registry!!!
 */
int dkr_api_blob_pull_get_url(struct dkr_api_data_s * dkr_api_data,const char *image,const char *blobSum,char ** url,char ** Auth);


/**
 * \brief check if ImageStream Registry secure/non-secure
 * \param ImageStream_domain  domain or ip of the internal ImageStram registry
 * \param cert_file_dest      Location of the ca certificates file
 * \param trace               trace struct
 * \note if trace =NULL, no output, if not NULL error message is printed on the trace
 * \note use writepad with padding of 58 for alignement
 * \returns 0 on secure, 1 on in-secure and -1 on connection issues, -2 on no apiv2 docker registry
 */
int dkr_api_check_registry_secure(const char * ImageStream_domain, const char * cert_file_dest, void * trace);


int verify_image(struct dkr_api_data_s * apidata, char * image,struct trace_Struct * trace, char ** image_sha, char ** tag );

void debug_registry_print(cJSON * registry );


#endif /* defined(__odagrun__dkr_api__) */
