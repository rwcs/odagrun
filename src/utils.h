/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */


/*! @file utils.h
 *  @brief header utils.c
 *  @author danny@gioxa.com
 *  @date 9 Sep 2017
 *  @copyright (c) 2017 Danny Goossen,Gioxa Ltd.
 */

#ifndef __deployctl__utils__
#define __deployctl__utils__

#include "common.h"

/*! 
 * \def PIPE_READ
 * \brief defines the read end of a pipe
 */
#define PIPE_READ 0

/*!
 * \def PIPE_WRITE
 * \brief defines the write end of a pipe
 */
#define PIPE_WRITE 1

/**
 * \brief change a string to uppercase
 * \param s a null terminated string to change uppercase
 */
void upper_string(char s[]);

/**
 \brief change a string to uppercase
 \param s a non null terminated string to change to uppercase
 \param n length of \a s to process
 */
void upper_string_n(char s[],size_t n);

/**
 \brief change a string to lowercase
 \param s a null terminated string to change to lower case
 */
void lower_string(char s[]);

/**
 \brief Recursive remove directory with dir printf like arg
 \param dir directory to remove
 */
void vrmdir(const char *dir, ...);

/**
 \brief Recursive remove directory with dir printf like arg, and follow symlinks
 \param dir directory to remove
 */
void vrmdir_s(const char *dir, ...);


/**
 \brief mdir -p printf like args, returning the path
 
 Use like: \code char * dir=vmkdir("/mydir/%s",mysubdir); \endcode
 \param dir directory to remove
 \return 0 on success -1 on error with error Noset if no errors format
 */
int vmkdir(const char *dir, ...);

/**
 \brief mdir -p printf like args, returning the path
 
 Use like: \code char * dir=vmkdir("/mydir/%s",mysubdir); \endcode
 \param dir directory to remove
 \return path
 \note caller needs to free returned result
 */
char * vcmkdir(const char *dir, ...);

/**
 \brief check if a directory exists, with printf like args
 
 Use like: \code v_exist_dir("/mydir/%s",mysubdir); \endcode
 \param dir directory to remove
 \return 1 on exist
 */
int v_exist_dir(const char *dir, ...);

/**
 \brief copy file form \a source to \a destination
 \param source source file
 \param destination destination file
 \return 0 on succes
 */
int cp_file(const char * source,const char * destination);

/**
 \brief append file form \a source to \a destination
 \param source source file
 \param destination destination file
 \return 0 on succes
 */
int append_file_2_file(const char * source,const char * destination);

/**
 \brief append  \a data to file \a destination
 \param data source file
 \param destination destination file
 \return amount of bytes writen on succes
 */
int append_data_2_file(const char * data,const char * destination);


/**
 \brief returns in *\a p the directory of the filename \a pf
 \param p pointer to path string
 \param pf filename to split
 */
void split_path_file_2_path(char** p,const char *pf);


/**
 \brief get filelist from artfacts paths
 \param projectdir base directory
 \param paths cJSON struct with the paths
 \param filelist pointer to the result
 \return 0 on success
 */
int getfilelist(const char * projectdir,cJSON * paths,cJSON ** filelist);


/**
 * \brief read a file to memory
 * \param dirpath file to read from
 * \return buffer with content of file, NULL if there was a problem
 * \note Caller is responsible for freeing the returned buffer
 */
char * read_a_file(const char * dirpath);

/**
 * \brief Normanilse path and don't follow symlinks
 * \param pwd is the directory path (absolute path) relative to which other paths will be normalized. It is generally the absolute path of the current directory.
 * \param src source path to normalize
 * \param res buffer to put result in
 * \return Normalized path or NULL on error
 * \note char *res must have the required memory/capacity to contain the normalized path.
 * \see https://stackoverflow.com/a/34202207/6821811
 * \see https://gist.github.com/Eugeny/5127791
 * \code{.c}
 #include <stdio.h>
 
 int main()
 {
    char path[FILENAME_MAX+1];
    printf("\n%s\n",normalizePath((char*)"/usr/share/local/apps",(char*)"./../../../",path));
   return 0;
 }
*/
char * normalizePath(const char* pwd, const char * src, char* res);

/**
 * \brief read a file with printf like args
 * \param filename file to read from
 * \return buffer with content of file, NULL if there was a problem
 * \note Caller is responsible for freeing the returned buffer
 */
char * read_a_file_v(const char * filename,...);

/**
 * \brief write a null terminated string to a file with printf like args
 * \param data to write to file
 * \param len number of bytes to write, or 0 on a null terminated data string
 * \param filepath printf like args filename to write data to
 * \return 0 on success
 */
int write_file_v(const char *data,size_t len,const char * filepath,... );

/**
 * \brief Reverse memchr()
 *
 * Find the last occurrence of 'c' in the buffer 's' of size 'n'.
 *
 * \see https://github.com/st3fan/osx-10.9/blob/master/sudo-72/src/memrchr.c
 * \param s source
 * \param c char to find
 * \param n size of source
 */
void * memrchr(const void *s,int c, size_t n);

#define FORMAT_SIZE_BUF 7

/**
 * \brief format file size
 * \copyright simplified BSD license https://stackoverflow.com/users/82294/dietrich-epp
 * \see https://stackoverflow.com/a/7847087/6821811
 * convert file size in kMG
 *
 * \param [out] buf ouput buffer containing the result
 * \param[in] sz uint64 file size
 * \return pointer to \a buff
 */
const char *
format_size(char buf[FORMAT_SIZE_BUF], uint64_t sz);


/**
 * \brief creates and returns optarg, if null, use deafult value else return pointer char * size 1
 * \param optarg const char* retrurned from ;ongoptions
 * \param default_value value if null
 * \param len ptr if defined get's the length of the newly created string
 * \note caller is responsible for freeing the output
 */
char * get_opt_value(const char * optarg,const char* default_value, size_t*len);

/**
 * \brief Macro that creates and returns formated output from printf like (format,...)
 * \note caller is responsible for freeing the output
 * \param fmt format
 * \param plen ptr to lenght integer
 * \return newly allocated formated string output with length plen
 */
#define CSPRINTF_L(fmt,plen) ({va_list vargs; va_start(vargs, fmt); char * s; *plen=vasprintf(&s,fmt,vargs);va_end(vargs); s;})

/**
 \brief strip trailing newline from string
 \param data the string to process
 \note memory manipulations, only add \0
 */
void strip_nl(char * data);

/**
 \brief redirect stderr to pipe, use with pipe_redirect_undo() to retrieve info
 \param my_pipe provide uninitialised int[2]
 \return stderr saved file descriptor
 \code
char *errormsg=NULL;
int my_pipe[2];
int saved_stderr=pipe_redirect_stderr(my_pipe);
fprintf(stderr,"let's see were this ends up\n");
pipe_redirect_undo(my_pipe, saved_stderr, errormsg);
printf("FROM_redirect: %s\n",errormsg);
if (errormsg) free(errormsg);
errormsg=NULL;
 */
int pipe_redirect_stderr(int my_pipe[2]);

/**
 \brief retrieve stderr redirect and undo the redirect of  pipe_redirect_stderr()
 \param my_pipe same as drom  pipe_redirect_stderr()
 \param saved_stderr return value from  pipe_redirect_stderr()
 \param errormsg pointer to string containing the redirect
 \note caller needs to free
 */
void pipe_redirect_undo(int my_pipe[2],int saved_stderr,char**errormsg);

/**
 \brief check is a directory patch exists
 \copyright https://stackoverflow.com/users/3422102/david-c-rankin
 \param d directory path to check
 \return 0 on success, -1 on does not exist, -2 if exists but not directory
 */
int xis_dir (const char *d);

/**
 /brief safe chmod
 */
int safe_chmod(const char * target,mode_t m);

/**
 /brief free and set Null
 */
void oc_free(char ** m);

/**
 * \brief free when strlen is zero and set to NULL;
 */
void unsetifzero(char**x);

/**
 \brief append printf format to target and return new allocated string on success
 \param target string pointer containing result, on errors unmodified
 \param fmt for output print append
 \return int -1 on failure, on success number of bytes appended
 */
int astrcatf(char ** target,const char * fmt,...)
	__attribute__ ((format (printf, 2, 3)));  /* Flawfinder: ignore */

/**
 \brief safely create string in printf formatlt, on errors unmodified
 \param fmt for output print append
 \return char * if success, on failure returns NULL
 */
char* create_stringf(const char * fmt,...)
	__attribute__ ((format (printf, 1, 2)));  /* Flawfinder: ignore */

#endif
