/*
 Copyright (c) 2017 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

/*! \file oc_do_job.c
 *  \brief job handler for the dispatcher
 *  \author Created by Danny Goossen on 9/10/17.
 *  \copyright (c) 2017 Danny Goossen. All rights reserved.
 */

#include "deployd.h"
#include "oc_do_job.h"
#include "do_job.h"
#include <stdarg.h>

#include <wordexp.h>
#include "wordexp_var.h"
#include "registry.h"
#include "manifest.h"
#include "c-ares.h"
#include "transfer_blobs.h"
#include "utils.h"
#include "cache.h"
#include "image_parameters.h"



/**
 *  \brief Creates the config map with the job to execute
 *  \param job output in the config map
 *  \param namespace we operate in
 *  \param id ci_job_id used for the map naming
 *  \return the configmap in cjason struct or NULL on failure
 *  \note caller is responsible to free the struct
 */
static cJSON* make_configmap_template(const cJSON * job, const char * namespace,int id);


/**
 *  \brief Creates the pod-template to execute the job
 *  \param name of the pod template
 *  \param image Docker image to use for the executer pod
 *  \param id ci_job_id used for the map naming
 *  \param exec_uid run pod with user_id: exec_uid
 *  \return the pod template or NULL on failure
 *  \note caller is responsible to free the struct
 */
static cJSON* make_pod_config(const char* name, const char * image, int id, int exec_uid,int builds,cJSON ** limits);



/**
 * \brief check if manifest can be downloaded
 * \todo rework with HEAD and no accept
 */
int verify_image(struct dkr_api_data_s * apidata, char * image,struct trace_Struct * trace, char ** image_sha, char ** image_tag);

/**
 * \brief parse variable `cache` into job cache pull, cache_push and cache_links
 *
 */

cJSON * Set_Pod_limits(cJSON * compute_resources_timebound,const char * podsize);

#define oc_fail_image_check -99
#define oc_fail_image_expansion -98
#define oc_fail_no_image_defined -97


// Public functions

int do_job_oc(cJSON * runner,cJSON* job,parameter_t * parameters )
{
	
	
	char * token=read_a_file_v( "%s/token",parameters->service_dir);
	char *oc_namespace=read_a_file_v("%s/namespace",parameters->service_dir);
	
	// TODO findout registry name in ip-adress otherwise problems!!! (github issue ...... known issue)
	char * ImageStream_domain=NULL;
	
	char cert_file_dest[1024];
	snprintf(cert_file_dest, 1024, "/bin-runner/rootfs%s",DEFAULT_CA_BUNDLE);
	
	void *trace=NULL;
	int runner_id=cJSON_GetObjectItem(runner, "id")->valueint;
	
	int res=0; // failed or success
	cJSON * id_j=cJSON_GetObjectItem(job, "id");
	int id=0;
	if (cJSON_IsNumber(id_j)) id=id_j->valueint;
	alert("runner %2d: Start Job %d (%s)\n",runner_id,id,cJSON_get_key(runner, "ci_url"));
	char *temp=NULL;
	int aspr=asprintf(&temp,"%s/api/v4/jobs/%d",cJSON_get_key(job, "ci_url"),id);
	if (aspr!=-1)
	{
		init_dynamic_trace(&trace,cJSON_get_key(job, "token") , temp,id,parameters->ca_bundle);
	}
	oc_free(&temp);
	
	Write_dyn_trace(trace,bold_cyan,"%s  ",PACKAGE_STRING);
	update_details(trace);
	char * registry_ip=getip("docker-registry.default.svc.cluster.local");
	char * tmp_is=getenv("OKD_REGISTRY_IP");
	if (tmp_is)
	{
		aspr=asprintf(&ImageStream_domain,"%s",tmp_is);
	}
	else if (registry_ip)
	{
		aspr=asprintf(&ImageStream_domain,"%s:%d", registry_ip,5000);
	}
	else
	{
		aspr=asprintf(&ImageStream_domain,"%s","172.30.1.1:5000");
	}
	if (registry_ip) oc_free(&registry_ip);
	if(aspr==-1)
	{
		error("Error, no Imagestream Domain\n");
		exit (1);
	}
	int ImageStream_Insecure=0;
	
	int ImageStream_v1Only=0;
	char * is_registry_v1only=getenv("OKD_REGISTRY_V1only");
	if (is_registry_v1only)
	{
		debug("OKD_REGISTRY_V1only=%s\n",is_registry_v1only);
		char * tmp=strdup(is_registry_v1only);
		is_registry_v1only=tmp;
		
		lower_string(is_registry_v1only);
		if(strcmp(is_registry_v1only,"true")==0 || strcmp(is_registry_v1only,"yes")==0 || strcmp(is_registry_v1only,"1")==0)
			ImageStream_v1Only=1;
		oc_free(&is_registry_v1only);
		is_registry_v1only=NULL;
	}
	int ImageStream_v2support=0;
	char * is_registry_v2support=getenv("OKD_REGISTRY_V2support");
	if (is_registry_v2support)
	{
		debug("OKD_REGISTRY_V2support=%s\n",is_registry_v2support);
		char * tmp=strdup(is_registry_v2support);
		is_registry_v2support=tmp;
		
		lower_string(is_registry_v2support);
		if(strcmp(is_registry_v2support,"true")==0 || strcmp(is_registry_v2support,"yes")==0 || strcmp(is_registry_v2support,"1")==0)
			ImageStream_v2support=1;
		oc_free(&is_registry_v2support);
		is_registry_v2support=NULL;
	}
	debug("OKD_REGISTRY_v2support=%d\n",ImageStream_v2support);
	char * is_debug=getenv("ODAGRUN_DEBUG");
	if (is_debug)
	{
		debug("ODAGRUN_DEBUG=%s\n",is_debug);
		char * tmp=strdup(is_debug);
		is_debug=tmp;
		
		lower_string(is_debug);
		if(strcmp(is_debug,"true")==0 || strcmp(is_debug,"yes")==0 || strcmp(is_debug,"1")==0)
			setdebug();
		oc_free(&is_debug);
		is_debug=NULL;
	}
	
	
	
	char * master_url=NULL;
	const char * master_url_env=getenv("OKD_MASTER_URL");
	if (master_url_env)
		master_url=strdup(master_url_env);
	else
		master_url=strdup("https://openshift.default.svc.cluster.local:443");
	
	
	char hostname[1024];
	gethostname(hostname, 1024);
	
	uid_t start_uid=0;
	uid_t range_uid=1;
	char * scc=NULL;
	char * node_name=NULL;
	struct oc_api_data_s *oc_api_data=NULL;
	// init oc_api
	debug ("checking api uid range and scc\n");
	
	oc_api_init(&oc_api_data, cert_file_dest, master_url, token);
	oc_api_get_restrictions(oc_api_data,oc_namespace, hostname, &start_uid, &range_uid, &scc,&node_name);
	
	uid_t exec_uid=0;
	
	const char * runas_str=cJSON_get_key(runner, "RunAsUser");
	debug("Try runas : %s, scc: %s, uidrange:%d/%d\n",runas_str,scc,start_uid,range_uid);
	debug("nodeName=%s\n",node_name);
	if (runas_str)
	{
		if (strcmp(runas_str,"root")==0 )
		{
			// TODO check if possible according to scc, but how if non clusteradmin cannot check scc????
		 exec_uid=0;
		}
		else
		{
		 exec_uid=start_uid;
		}
	}
	else
	{
		exec_uid=start_uid;
	}
	
	Write_dyn_trace(trace,blue,"  ( node %s )\n",node_name);
	char dashes[]="--------------------------------";
	dashes[strlen(PACKAGE_STRING)]=0;
	Write_dyn_trace(trace,bold_magenta,"%s\n",dashes);
	
	Write_dyn_trace(trace,none,"\n");
	
	time_t timer;
	char time_str[26];
	struct tm* tm_info;
	time(&timer);
	tm_info = localtime(&timer);
	strftime(time_str, 26, "%Y-%m-%d %H:%M:%S", tm_info);
	
	Write_dyn_trace(trace,none,"jobid #%d with dispatcher '%s' @ ",id,hostname);
	Write_dyn_trace(trace,none,"%s",time_str);
	Write_dyn_trace(trace,none," \n");
	update_details(trace);
	Write_dyn_trace(trace,bold_yellow,"* Starting executer to RunAs ");
	if (!exec_uid)
		Write_dyn_trace(trace,red,"'root'\n");
	else
		Write_dyn_trace(trace,green,"'non-root (uid:%d)'\n",exec_uid);
	update_details(trace);
	cJSON *env_vars=cJSON_CreateObject();
	Environment *Env=NULL;
	Env=Environment_init(env_vars, environment_type_cJSON);
	
	if(!res)
	{
		cJSON * var=NULL;
		cJSON_ArrayForEach(var,cJSON_GetObjectItem(job, "variables"))
		{
			const char * value=cJSON_get_key(var, "value");
			const char * vs=cJSON_get_key(var, "key");
			if (vs&&strncmp(vs, "CI_BUILD_", 9)!=0 && value )
			{ // remove legacy CI_BUILD_xxxx vars
				
				int overwrite=1;
				if (
					strcmp(vs,"CI_PROJECT_PATH"  )==0 ||
					strcmp(vs,"CI_PROJECT_URL"   )==0 ||
					strcmp(vs,"GITLAB_USER_NAME" )==0 ||
					strcmp(vs,"GITLAB_USER_EMAIL")==0 ||
					strcmp(vs,"GITLAB_USER_ID"   )==0 ||
					strcmp(vs,"GITLAB_USER_LOGIN")==0 ||
					strcmp(vs,"CI_REPOSITORY_URL")==0 ||
					strcmp(vs,"CI_REGISTRY"      )==0 ||
					strcmp(vs,"CI_REGISTRY_IMAGE")==0
					)
					overwrite=0;
				Environment_setenv(Env, vs, value,overwrite);
			}
			
		}
	}
	if (env_vars) cJSON_AddItemToObject(job, "env_vars", env_vars);
	if(!res)
	{
		Write_dyn_trace_pad(trace, bold_yellow,45, "* Check ImageStream Registry ...");
		update_details(trace);
		int reg_check=dkr_api_check_registry_secure( ImageStream_domain, cert_file_dest,trace);
		if (reg_check==1)
		{
			ImageStream_Insecure=1;
			res=0;
		}
		else if (reg_check==0)
		{
			ImageStream_Insecure=0;
			res=0;
		}
		else
		{
			ImageStream_Insecure=0;
			res=-1;
		}
	}
	debug("imagestream insecure %d\n",ImageStream_Insecure);
	cJSON *sys_vars=NULL;
	sys_vars=cJSON_CreateObject();
	if (sys_vars)
	{
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-IP",ImageStream_domain);
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-OC-NAMESPACE",oc_namespace);
		cJSON_safe_addBool2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-Insecure",ImageStream_Insecure);
		cJSON_safe_addBool2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-v1Only",ImageStream_v1Only);
		cJSON_safe_addBool2Obj(sys_vars,"ODAGRUN-IMAGESTRAM-v2Support",ImageStream_v2support);
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-TOKEN",token);
		cJSON_safe_addString2Obj(sys_vars,"ODAGRUN-DOCKER-REGISTRY",DOCKER_REGISTRY);
		cJSON_safe_addString2Obj(sys_vars,"OC-MASTER-URL",master_url);
		cJSON_AddItemToObject(job, "sys_vars", sys_vars);
	}
	print_json(sys_vars);
	if (master_url) oc_free(&master_url);
	master_url=NULL;
	// create image strean with oc-api, seemed like a bug with aws, need to create the image stream first!!!
	if (!res)
	{
		res=oc_api(oc_api_data, "GET", NULL, NULL, "/oapi/v1/namespaces/%s/imagestreams/is-executer-image",oc_namespace);
		debug("result imagestream GET %d ",res);
		if (res==404)
		{
			cJSON * imagestream=make_imagestream_template( oc_namespace,"is-executer-image");
			if (imagestream)
			{
				res=oc_api(oc_api_data, "POST", imagestream, NULL, "/oapi/v1/namespaces/%s/imagestreams",oc_namespace);
				debug("result imagestream POST %d ",res);
			}
		}
	}
	// obtain main image
	cJSON * image_json=NULL;
	char * image=NULL;
	char * org_image=NULL;
	
	if (!res)
	{
		const char *tmp= NULL;
		image_json=cJSON_GetObjectItem(job, "image");
		tmp= cJSON_get_key(image_json, "name");
		if (tmp)
		{
			image=strdup(tmp);
			tmp=NULL;
		}
		if (!image)
		{
			if(cJSON_get_key(runner, "default_image"))
			 image=cJSON_get_stringvalue_dup(runner, "default_image");
		}
		if (!image)
		{
			Write_dyn_trace(trace, red, "\nERROR: No image defined nor default image in config\n");
			Write_dyn_trace(trace, cyan, "\n\t Define 'image: ....' in '.gitlab.yml'\n");
			Write_dyn_trace(trace, cyan, "\t OR\n");
			Write_dyn_trace(trace, cyan, "\t Define 'default_image: ....' in the odagrun ConfigMap\n");
			Write_dyn_trace(trace, cyan, "\n\t Define 'image: ....' in '.gitlab.yml'\n");
			res=oc_fail_no_image_defined;
		}
		
		else if (image && strcmp(image,"scratch")!=0)
		{
			org_image=image;
			//resolve_image_var(&org_image,env_vars);
			image=NULL;
			//res=prep_image(&image,env_vars,ImageStream_domain,oc_namespace);
			//char * process_image_nick(char ** image,const cJSON * env_vars,const char * Imagestream_ip, const char * oc_name_space,const char * docker_registry)
			image=process_image_nick(&org_image,env_vars,ImageStream_domain,oc_namespace,DOCKER_REGISTRY);
			if (!image)
			{
			 Write_dyn_trace(trace, red, "\nERROR: variables in the `image` name could not be resolved!\n");
			 Write_dyn_trace(trace, blue, "\n  Please check `.gitlab-ci.yml` and update the faulty line:\n");
			 if (org_image)
			  Write_dyn_trace(trace, none,   "  \'image: %s\'\n",org_image);
			 else
			  Write_dyn_trace(trace, none,   "  \'image: ...\'\n");
			 res=oc_fail_image_check;
			} else if(image && strchr(image,'@')!=NULL)
			{
				free (image);
				image=NULL;
				Write_dyn_trace(trace, red, "\nERROR: image with <repo>@DIGEST not supported!\n");
				update_details(trace);
				res=oc_fail_image_check;
			}
			
		}
		else org_image=strdup(image);
	}
	update_details(trace);
	struct dkr_api_data_s * apidata=NULL;
	if (!res)
	{
		cJSON_AddStringToObject(env_vars, "ODAGRUN_BUILD_IMAGE",image );
		cJSON_AddStringToObject(env_vars, "ODAGRUN_BUILD_IMAGE_NICK",org_image );
	}
	char * image_sha=NULL;
	char * image_tag=NULL;
	if (!res)
	{
		dkr_api_init(&apidata, cert_file_dest); // DEFAULT_CA_BUNDLE);
		
		// image preparations ready, go and check if they can be downloaded
		if (image && strlen(image)>0 )
		{
			cJSON_ReplaceItemInObject(image_json, "name", cJSON_CreateString(image));
			Write_dyn_trace(trace, bold_yellow, "* Using docker image:\n");
			Write_dyn_trace(trace, cyan, "\t\'%s\'\n",org_image);
			
			
			debug("oc-do-job: Image Check");
			Write_dyn_trace_pad(trace, yellow ,60,"   * Image check ...");
			update_details(trace);
			
			int image_check_result=0;
			if (strcmp(image, "scratch")!=0)
			{
				set_registry_by_image(job, image,apidata);
				image_check_result=verify_image(apidata,image,trace,&image_sha, &image_tag);
				if (image_check_result!=200)
				{
					res=oc_fail_image_check;
					oc_free(&image);
					image=NULL;
				}
			}
			else
			{
				// registry set on calculation is-executer name
				Write_dyn_trace(trace, green, "[n.a.]\n");
			}
		}
	}
	if (image_sha)
	{
		alert("ODAGRUN_BUILD_IMAGE_SHA: %s\n",image_sha);
		cJSON_AddStringToObject(env_vars, "ODAGRUN_BUILD_IMAGE_SHA",image_sha );
		oc_free(&image_sha);
		image_sha=NULL;
	}
	if (image_tag)
	{
		alert("ODAGRUN_BUILD_IMAGE_TAG: %s\n",image_tag);
		cJSON_AddStringToObject(env_vars, "ODAGRUN_BUILD_IMAGE_TAG",image_tag );
		oc_free(&image_tag);
		image_tag=NULL;
	}
	char * this_executer_image=NULL;
	char * this_executer_image_name=NULL;
	char * this_executer_image_tag=NULL;
	//TODO free above items if not null
	
	if (!res && image && strlen(image)>0)
	{
		
		aspr=asprintf(&this_executer_image_name, "is-executer-image");
		if (aspr!=-1)aspr=asprintf(&this_executer_image_tag,  "%d",id);
		if (aspr!=-1)aspr=asprintf(&this_executer_image, "%s/%s/is-executer-image:%d",ImageStream_domain,oc_namespace,id);
		//res= process_executor_image_nick(&this_executer_image,ImageStream_domain,oc_namespace);
		if (aspr==-1)
		{
			res=-1;
			oc_free(&this_executer_image);
			oc_free(&this_executer_image_tag);
			oc_free(&this_executer_image_name);
			oc_free(&image);
			error("out of Memory\n");
		}
		else
		{
		debug("this executor image: %s\n",this_executer_image);
		set_registry_by_image(job, this_executer_image,apidata);
		}
	}
	
	//
	/*
	 apiVersion: image.openshift.io/v1
	 kind: ImageStream
	 metadata:
  name: is-executer-image
  namespace: odagrun
	 spec:
  lookupPolicy:
	 local: false
	 */
	
	if (org_image)oc_free(&org_image);
	char  digest_manifest[65+7];
	org_image=NULL;
	
	char  digest_config[65+7];
	int config_len=0;
	char  *config_str=NULL;
	cJSON * manifest=NULL;
	cJSON * config=NULL;
	cJSON * layer_data=NULL;
	int schemaVersion=2;
	if (ImageStream_v1Only) schemaVersion=1;
	if (!res)
	{
		Write_dyn_trace_pad(trace, yellow ,60,"   * parse layers ...");
		update_details(trace);
		int exec_upload=0;
		do
		{
			int layers_pull=0;
			if (res==-2) res=0;
			if (res==0 && image && strcmp(image, "scratch")!=0 )
			{
				
				res=append_image_2_layer_data( apidata,image,&layer_data ,&schemaVersion, "image");
			}
			if(!res)
			{
				layers_pull=parse_git_cache(apidata, job, 0);
			}
			if (!res)
			{
				cJSON * workspaces=cJSON_GetObjectItem(env_vars, "WORK_SPACES");
				if ( workspaces && cJSON_IsString(workspaces) )
				{
					cJSON * temp=yaml_sting_2_cJSON(NULL,workspaces->valuestring );
					if (temp && cJSON_IsArray(temp) )
					{
						workspaces=cJSON_GetArrayItem(temp, 0);
					}
					else
						workspaces=NULL;
				}
				else
					workspaces=NULL;
				
				if (workspaces) layers_pull=layers_pull+parse_workspaces(apidata,  job,0);
			}
			
			if(!res)
			{
				if (layers_pull)
				{
					debug("cache pull\n");
					print_json(cJSON_GetObjectItem(job, "layers_pull"));
					cJSON * item=NULL;
					cJSON_ArrayForEach(item, cJSON_GetObjectItem(job, "layers_pull"))
					{
						const char * tmp_image=cJSON_get_key(item,"image");
						const char * tmp_name=cJSON_get_key(item,"name");
						char *display_name=NULL;
						int aspr=asprintf(&display_name,"WS:%s",tmp_name);
						if (aspr!=-1 && display_name)
						{
							res=append_image_2_layer_data( apidata,tmp_image,&layer_data ,&schemaVersion,display_name);
							oc_free(&display_name);
							if (res==-1)
							{
								const char wp[]="WorkSpace ";
								const char gc[]="";
								const char * msg=NULL;
								if(strcmp(tmp_name,"git_cache")==0)
									msg=gc;
								else
									msg=wp;
								if(cJSON_safe_IsFalse(item, "mandatory"))
								{
									res=0;
								}
								else
								{
									Write_dyn_trace(trace, magenta, "\n   ERROR: Mandatory %s%s image not present\n",msg,tmp_name);
									update_details(trace);
								}
							}
						} else
						{
							//out of memory
							Write_dyn_trace(trace, magenta, "\n   ERROR: Out of Memory\n");
							update_details(trace);
						}
					}
				}
			}
			// append exec layer
			if (res==0 && image)
			{
				
				char *exec_image=NULL;
				int aspr=asprintf(&exec_image,"%s/%s/is-executer-image:latest",ImageStream_domain,oc_namespace);
				debug("append layer: %s\n",exec_image);
				if (aspr!=-1 && exec_image)
				{
					set_registry_by_image(job, exec_image,apidata);
					res=append_image_2_layer_data( apidata,exec_image,&layer_data ,&schemaVersion,"executer");
					if (res==-1)
					{
						create_imagestream(oc_api_data, job, "is-executer-image");
						// if we could not find manifest, push it, maybe it was deleted!!!
						res=image_2_ISR_int(apidata, trace, exec_image,"/bin-runner/image",ImageStream_v1Only);
						if (res==200 || res==201 || res==0)
						{
							res=append_image_2_layer_data( apidata,exec_image,&layer_data ,&schemaVersion,"executer");
						}
						exec_upload=1;
					}
					oc_free(&exec_image);
				}
			}
			
		} while (res==-2);
		if (!res)
		{
			if (exec_upload) Write_dyn_trace_pad(trace, yellow ,61,"\n     parsed layers");
			Write_dyn_trace(trace, green, "  [OK]\n");
		}
		else
		{
			if (exec_upload) Write_dyn_trace_pad(trace, yellow ,61,"\n     parsed layers");
			Write_dyn_trace(trace, red, "[FAIL]\n");
		}
		update_details(trace);
	}
	debug("\n-----------------------------------------------------------------------------------------------\nlayer_data\n");
	print_json(layer_data);
	if (layer_data)
	{
		cJSON * ws_env=cJSON_DetachItemFromObject(layer_data, "ws_environment");
		if (ws_env)
			cJSON_AddItemToObject(job, "ws_environment", ws_env);
		print_json(ws_env);
	}
	if (!res)
	{
		Write_dyn_trace(trace, yellow, "   * transfer layers\n");
		update_details(trace);
		debug("transfer blobs\n");
		// transfer blobs, transferblobs can handle layerdata, it contains schemaVersion and fsLayers or Layers
	 res=transfer_blobs(layer_data,image,this_executer_image, apidata, trace);
	}
	if ( res==0 && schemaVersion==2)
	{
		cJSON* config=NULL;
		debug("produce config\n");
		produce_config(layer_data,&config);
		if (config)
		{
			config_str=cJSON_Print(config);
			config_len=(int)strlen(config_str);
			calc_sha256_buff(config_str, config_len, digest_config);
			Write_dyn_trace_pad(trace, yellow,60, "   * Push new config");
			update_details(trace);
			res=RETRY_HTTP(dkr_api_blob_push_raw(apidata, this_executer_image, digest_config, config_str,0));
			oc_free(&config_str);
			config_str=NULL;
		}
		else res=-1;
		if (res==200||res==201||res==204)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			res=0;
		}
		else
			Write_dyn_trace(trace, red, "[FAIL]\n");
		update_details(trace);
	}
	
	if (res==0 && schemaVersion==2)
	{
		debug("produce manifest v2\n");
		produce_manifest_V2(layer_data,&manifest, digest_config,config_len);
		char * manifest_str=cJSON_Print(manifest);
		debug("pushing manifest v2\n");
		Write_dyn_trace_pad(trace, yellow,60, "   * Push new manifest V2");
		res=dkr_api_manifest_push_raw(apidata, this_executer_image, NULL, manifest_str,1);
		calc_sha256_buff(manifest_str, 0, digest_manifest);
		debug("V2 manifest %s\n",manifest_str);
		if (manifest_str) oc_free(&manifest_str);
		manifest_str=NULL;
		if (res==200||res==201||res==204)
		{
			Write_dyn_trace(trace, green,   "  [OK]\n");
			res=0;
		}
		else
			Write_dyn_trace(trace, red, "[FAIL]\n");
		update_details(trace);
	}
	
	if (res==0 && schemaVersion==1)
	{
		debug("produce manifest v1\n");
		
		produce_manifest_V1(layer_data,&manifest, this_executer_image_name, this_executer_image_tag);
		char * manifest_str=cJSON_Print(manifest);
		if (manifest_str)
		{
			calc_sha256_buff( manifest_str, 0, digest_manifest);
			oc_free(&manifest_str);
			manifest_str=NULL;
		}
		char * Signed_manifest=SignManifest(manifest);
		
		debug("Signed v1 Manifest\n--------------%s\n",Signed_manifest);
		debug("Push image as %s\n",this_executer_image);
		debug("pushing manifest V1: %s\n",digest_manifest);
		Write_dyn_trace_pad(trace, yellow,60, "   * Push new manifest V1");

		res=dkr_api_manifest_push_raw(apidata, this_executer_image, NULL, Signed_manifest,0);
		if (res==200||res==201||res==204)
		{
			Write_dyn_trace(trace, green,   "  [OK]\n");
			res=0;
			
		}
		else
			Write_dyn_trace(trace, red, "[FAIL]\n");
		update_details(trace);
		if (Signed_manifest) oc_free(&Signed_manifest);
		Signed_manifest=NULL;
	}
	
	if(layer_data) cJSON_Delete(layer_data);
	layer_data=NULL;
	
	if (config)  cJSON_Delete(config); //TODO fix double free when having this
	config=NULL;
	if (manifest)cJSON_Delete(manifest);
	manifest=NULL;
	
	
	// if something went wrong report failure and end
	if (res)
	{
		if (image) oc_free(&image);
		image=NULL;
		Write_dyn_trace(trace,bold_red,"\nJob FAILED.\n");
		set_dyn_trace_state(trace,"failed" );
		update_details(trace);
	}
	
	if (this_executer_image_name) oc_free(&this_executer_image_name);
	if (this_executer_image_tag) oc_free(&this_executer_image_tag);
	if (image)
	{
		oc_free(&image);
		image=this_executer_image;
	}
	else
		oc_free(&this_executer_image);
	this_executer_image=NULL;
	this_executer_image_name=NULL;
	this_executer_image_tag=NULL;
	
	// let's start working
	if (image)
	{
		// define api specific vars
		// result object
		cJSON *result=NULL;
		// template object
		cJSON *template=NULL;
		
		// names needed to delete when done
		char * config_map_name=NULL;
		char * pod_name=NULL;
		
		// job id for naming pods
		int job_id=0;
		
		// Delete options "{\"gracePeriodSeconds\" : 0}"
		cJSON * deleteoptions=cJSON_CreateObject();
		cJSON_AddNumberToObject(deleteoptions, "gracePeriodSeconds", 3600);
		if (!deleteoptions)
		{
			Write_dyn_trace(trace, red,"\ncan't create delete options, no point to continue!!! EXIT\n");
			update_details(trace);
			res=-1;
		}
		// let the real work begin
		if(!res)
		{
			cJSON * id_obj=cJSON_GetObjectItem(job, "id");
			if (id_obj) job_id=id_obj->valueint;
			if (!job_id)
			{
				Write_dyn_trace(trace, red,"\nNo job id!!! EXIT\n");
				update_details(trace);
				res=-1;
			}
		}
		if (!res)
		{
		 	Write_dyn_trace(trace, none,"\n");
			cJSON_AddItemToObject(job, "dispatcher", cJSON_CreateString(get_dynamic_trace(trace)));
			Write_dyn_trace_pad(trace, yellow,60,"  + oc creating job config_map ...");
			update_details(trace);
			debug("  + oc creating job config_map ...\n");
			template= make_configmap_template(job,oc_namespace,id);
			if (template)
			{
				res=oc_api(oc_api_data, "POST", template, &result,"/api/v1/namespaces/%s/configmaps",oc_namespace);
				if (!res)
				{
					Write_dyn_trace(trace, green, "  [OK]\n");
					update_details(trace);
				}
				else
				{
					Write_dyn_trace(trace, red, "[FAIL]\n");
					update_details(trace);
				}
				debug("  pushed config template map result: %d\n",res);
				cJSON_Delete(template);
				template=NULL;
			}
			else
		  		debug("  + done config template null\n");
			if (!res)
			{
				config_map_name=cJSON_get_stringvalue_dup(cJSON_GetObjectItem(result, "metadata"),"name");
				debug("configmapname %s\n",config_map_name);
			}
			if (result) cJSON_Delete(result);
			result=NULL;
			
		}
		
		if (!res)
		{
			//Write_dyn_trace(trace, none,"   digest_manifest: %s\n",digest_manifest);
			debug("manifest_digest: %s\n",digest_manifest);
			const char * podsize=cJSON_get_key(env_vars, "ODAGRUN_POD_SIZE");
			Write_dyn_trace_pad(trace, yellow,60,"  + creating Pod size: %s",podsize);
			update_details(trace);
			// create pod template
			const char * internal_ip=getenv("OKD_REGISTRY_IP_INTERNAL");
			{
				char * int_image=NULL;
				char * im_namespace=NULL;
				char * reference=NULL;
				char * registry=NULL;
				chop_image(image, &registry,&im_namespace , &reference);
				
				if (im_namespace)
				{
					int aspr=0;
					if (strstr(reference,"sha256:")==reference && strlen(reference)==71)
					{
						if (internal_ip)
							aspr=asprintf(&int_image, "%s/%s@%s",internal_ip,im_namespace,reference);
						else
							aspr=asprintf(&int_image, "%s/%s@%s",registry,im_namespace,reference);
					}
					else
					{
						if (internal_ip)
							aspr=asprintf(&int_image, "%s/%s@%s",internal_ip,im_namespace, digest_manifest);
						else
							aspr=asprintf(&int_image, "%s/%s@%s",registry,im_namespace,digest_manifest);
					}
					if (aspr==-1)
					{
						if (im_namespace) oc_free(&im_namespace);
						if (reference) oc_free(&reference);
						if (registry) oc_free(&registry);
						im_namespace=NULL;
						reference=NULL;
						registry=NULL;
						error("Failed to convert to internal registry ip, OUT OF MEMORY\n");
						res=-1;
					}
				}
				else
				{
					if (im_namespace) oc_free(&im_namespace);
					if (reference) oc_free(&reference);
					if (registry) oc_free(&registry);
					im_namespace=NULL;
					reference=NULL;
					registry=NULL;
					error("Failed to convert to internal registry ip\n");
					res=-1;
				}
				
				if (!res && int_image)
				{
					debug("int_image=%s\n",int_image);
					
					cJSON*resource_limits_namespace=NULL;
					cJSON*limits=NULL;
					int rres=0;
					int count=0;
					do{
						if (count) usleep(count*count*2000000LL);
						count++;
						if (resource_limits_namespace) cJSON_Delete(resource_limits_namespace);
						resource_limits_namespace=NULL;
						rres=oc_api(oc_api_data, "GET", NULL, &resource_limits_namespace, "/api/v1/namespaces/%s/resourcequotas/compute-resources-timebound",oc_namespace);
					} while (res!=0 && (rres<100||rres>=500) && count<5);
					if (resource_limits_namespace)
					{
						print_json(resource_limits_namespace);
						limits=Set_Pod_limits(resource_limits_namespace, podsize);
						cJSON_Delete(resource_limits_namespace);
					}
					if (!cJSON_GetObjectItem(limits, "error"))
					{
						template=make_pod_config("oc-executor", int_image, job_id,exec_uid,parameters->build_storage,&limits);
					}
					else
					{
						
						Write_dyn_trace(trace, red, "[FAIL]\n");
						
					
						cJSON* error_obj=cJSON_GetObjectItem(limits, "error");
						if (cJSON_get_key(error_obj, "memory"))
							Write_dyn_trace(trace, magenta, "\n%s\n",cJSON_get_key(error_obj, "memory"));
						if (cJSON_get_key(error_obj, "cpu"))
							Write_dyn_trace(trace, magenta, "\n%s\n",cJSON_get_key(error_obj, "cpu"));
						update_details(trace);
					}
					if (limits)
					{
						cJSON_Delete(limits);
						limits=NULL;
					}
					
				}
				if(int_image) oc_free(&int_image);
				int_image=NULL;
				if (registry) free (registry);
				if (im_namespace) oc_free(&im_namespace);
				if (reference) oc_free(&reference);
				im_namespace=NULL;
				reference=NULL;
				registry=NULL;
			}
			
		}
		if (template)
		{
			int count403=0;
			do{
				int count=0;
				do{
					count++;
					if (result) cJSON_Delete(result);
					result=NULL;
					res=oc_api(oc_api_data, "POST", template, &result,"/api/v1/namespaces/%s/pods",oc_namespace);
				}while(res!=0 && (res<100||res>=500) && count<5);
				
				if (res==403)
				{
					// {"kind":"Status","apiVersion":"v1","metadata":{},"status":"Failure","message":"pods \"oc-executor-112583644\" is forbidden: exceeded quota: compute-resources-timebound, requested: limits.cpu=500m,limits.memory=256Mi, used: limits.cpu=2,limits.memory=1Gi, limited: limits.cpu=2,limits.memory=1Gi","reason":"Forbidden","details":{"name":"oc-executor-112583644","kind":"pods"},"code":403}
					
					print_json(result);
					if(result && process403pod(result))
					{
						if (!count403){
							Write_dyn_trace_pad(trace, magenta,60,"\n   waiting for sufficiant resources Pod ...");
						    update_details(trace);
						}
						usleep(10000000LL);
					}
					else
					{
						Write_dyn_trace(trace, red, "[FAIL]\n");
						Write_dyn_trace(trace, magenta, "\n%s\n",cJSON_get_key(result, "message"));
						update_details(trace);
						res=4403;
					}
					count403++;
					if (result) cJSON_Delete(result);
					result=NULL;
				}
			} while(res==403 && count403<180);
			cJSON_Delete(template);
			template=NULL;
			if (count403>=180)
			{
				Write_dyn_trace(trace, red, "[FAIL]\n");
				Write_dyn_trace(trace, magenta, "\nTimeout waiting for resourses\n");
				update_details(trace);

			}
		}
		else res=-1;
		
		if (!res)
		{
			Write_dyn_trace(trace, green, "  [OK]\n");
			update_details(trace);
			pod_name=cJSON_get_stringvalue_dup(cJSON_GetObjectItem(result, "metadata"),"name");
			if (result) cJSON_Delete(result);
			result=NULL;
		}
		int running=0;
		
		if (!res)
		{
			update_details(trace);
			set_mark_dynamic_trace( trace);
			Write_dyn_trace(trace, cyan, "\nstatus pod '%s':  "   ,pod_name);
			
			dyn_trace_add_decrement(trace);
			update_details(trace);
			
			int terminated =0;
			int timeout_counter =1200;
			int counter=0;
			while (!terminated && timeout_counter) // add timeout
			{
				res=oc_api(oc_api_data, "GET", NULL, &result,"/api/v1/namespaces/%s/pods/%s",oc_namespace,pod_name);
				if (!res)
				{
					cJSON * status=cJSON_GetObjectItem(result, "status");
					if (status)
					{
						if (validate_key(status,"phase","Succeeded") ==0) terminated=1;
						if (validate_key(status,"phase","Running") ==0)
						{
							if (!running)
							{
								timeout_counter=3600; // TODO implement var's from job or runners.yaml
								debug("Running, timout set to 3600\n");
								running=1;
								print_json(result);
								
								if (config_map_name)
								{
									oc_api(oc_api_data, "DELETE", NULL, NULL,"/api/v1/namespaces/%s/configmaps/%s",oc_namespace,config_map_name);
								}
							}
						}
						if (validate_key(status,"phase","Failed") ==0)
						{
							terminated=1;
							res=1;
						}
						cJSON*containerStatuses=cJSON_GetObjectItem(status, "containerStatuses");
						cJSON* cnt_status=NULL;
						cJSON_ArrayForEach(cnt_status, containerStatuses)
						{
							print_json(cnt_status);
							if (cJSON_safe_IsFalse(cnt_status, "ready"))
							{
								cJSON* state=cJSON_GetObjectItem(cnt_status, "state");
								if (state)
								{
									cJSON* waiting=cJSON_GetObjectItem(state, "waiting");
									if (waiting)
									{
										if ( validate_key(waiting,"reason","ImageInspectError") ==0 ||
											 validate_key(waiting,"reason", "ImagePullBackOff") ==0 )
										{
											Write_dyn_trace(trace, red, "\nPod Failure '%s':  "   ,pod_name);
											Write_dyn_trace(trace, yellow, "%s\n",cJSON_get_key(waiting, "reason"));
											Write_dyn_trace(trace, magenta, "\n%s\n"   ,cJSON_get_key(waiting,"message" ));
											res=1;
											break;
										}
										else debug("reason=%s\n",cJSON_get_key(waiting,"reason"));
									}
								}
							}
						}
						
						alert("pod status %s\n",cJSON_get_key(status,"phase"));
					}
					else alert("no POD status\n");
				}
				else if (running && res <100)// manual delete
				{
					// pod is running, we don't want to stop it because of network issue's, so we wait for hard timeout
					// res=0 on http 200, res ==http code on curl_res==CURLE_OK or res=curl_res
					debug("wait for pod to stop: problem getting status pod: %s\n",curl_easy_strerror(res));
				}
				else
				{
					res=1;
					if (result) cJSON_Delete(result);
					result=NULL;
					break;
				}
				/*  TODO if error break
				 
				 GET https://openshift.default.svc.cluster.local:443/api/v1/namespaces/myproject/pods/oc-executor-23388
				 oc_api: return data for #404: {"kind":"Status","apiVersion":"v1","metadata":{},"status":"Failure","message":"pods \"oc-executor-23388\" not found","reason":"NotFound","details":{"name":"oc-executor-23388","kind":"pods"},"code":404}
				 */
				
				usleep(1000000);
				timeout_counter--;
				
				if ((counter % 10)==0)
				{
					if (!running && !terminated)
					{
						clear_till_mark_dynamic_trace(trace);
						Write_dyn_trace(trace, cyan, "\nstatus pod '%s':  "   ,pod_name);
						if (counter==0)
							Write_dyn_trace(trace, yellow, "pending ...        ");
						else
							Write_dyn_trace(trace, yellow, "pending for %3d sec",counter );
						dyn_trace_add_decrement(trace);
						update_details(trace);
					}
				}
				counter ++;
				if (result) cJSON_Delete(result);
				result=NULL;
			} // end while
			
			if (timeout_counter==0)
			{
				Write_dyn_trace(trace, red, "\nError: Timeout starting executer pod\n");
				update_details(trace);
				error("ERR: starting executer pod\n");
				res=1;
			}
		}
		// eeror on not been able to create delete options???
		if (pod_name)
		{
			oc_api(oc_api_data, "DELETE", deleteoptions, NULL,"/api/v1/namespaces/%s/pods/%s",oc_namespace,pod_name);
			
		}
		
		
		
		
		if (config_map_name)
		{
			oc_api(oc_api_data, "DELETE", deleteoptions, NULL,"/api/v1/namespaces/%s/configmaps/%s",oc_namespace,config_map_name);
			oc_free(&config_map_name);
			config_map_name=NULL;
		}
		
		
		// once more check if not crashed
		if ((pod_name) && oc_api(oc_api_data, "GET", NULL, &result,"/api/v1/namespaces/%s/pods/%s",oc_namespace,pod_name)==0)
		{
			oc_free(&pod_name);
			pod_name=NULL;
			if (result)
			{
				if (validate_key(cJSON_GetObjectItem(result, "status"),"phase","Failed") ==0) {res=1;}
				cJSON_Delete(result);
				result=NULL;
			}
		}
		
		if (deleteoptions)
		{
			cJSON_Delete(deleteoptions);
			deleteoptions=NULL;
		}
		// return result to gitlab
		
		if (res!=0)
		{
			alert("job failed\n");
			// get reason from result:status:containerStatuses[0]:state:terminated:{exitcode,reason}
			//or if result:kind == "Status" print result:(status,message,reason }
			Write_dyn_trace(trace,bold_red,"\nJob FAILED.\n");
			if (running)
			{
				alert("only sent state, not trace\n");
				set_dyn_trace_state(trace,"failed" );
				update_details_state(trace);
			}
			else
			{
				set_dyn_trace_state(trace,"failed" );
				error("ERROR: executer pod FAILED\n");
				update_details(trace);
			}
		}
		else debug("Executer pod succesfull created\n");
		if (result)
		{
			cJSON_Delete(result);
			result=NULL;
		}
		
		
	}
	if (image)
	{
		cJSON * result=NULL;
		debug ("delete_imagestreamtags\n");
		res=oc_api(oc_api_data, "DELETE", NULL, &result, "/oapi/v1/namespaces/%s/imagestreamtags/is-executer-image%%3A%d",oc_namespace,id);
		if (result && res)
		{
			print_json(result);
		}
		if (result)
		{
			cJSON_Delete(result);
			result=NULL;
		}
	}
	oc_api_cleanup(&oc_api_data);
	//dkr_api_manifest_delete(apidata, &image,digest_manifest); // does not work
	dkr_api_cleanup(&apidata);
	if (token) oc_free(&token);
	if (oc_namespace) oc_free(&oc_namespace);
	if (image) oc_free(&image);
	if(ImageStream_domain) oc_free(&ImageStream_domain);
	free_dynamic_trace(&trace);
	return 0;
}

/*
 apiVersion: v1
 data:
 job.json: >-
 <job json>
 kind: ConfigMap
 metadata:
 name: cfg-job-json-23268
 namespace: myproject
 */
static cJSON* make_configmap_template(const cJSON * job, const char * namespace,int id)
{
	cJSON* config_map=cJSON_CreateObject();
	cJSON * metadata=cJSON_CreateObject();
	cJSON * data=cJSON_CreateObject();
	
	cJSON_AddStringToObject(config_map, "apiVersion", "v1");
	cJSON_AddStringToObject(config_map, "kind",       "ConfigMap");
	
	cJSON_AddItemToObject  (config_map, "metadata",    metadata);
	char configmapname[256];
	snprintf(configmapname, 256, "cfg-job-json-%d",id);
	cJSON_AddStringToObject(metadata, "name",configmapname);
	cJSON_AddStringToObject(metadata, "namespace",namespace);
	cJSON_AddItemToObject  (config_map, "data",  data);
	char * job_str=cJSON_PrintUnformatted(job);
	if (job_str)
	{
		cJSON_AddStringToObject(data, JOBENVIRONMENTVAR, job_str);
		oc_free(&job_str);
	}
	else
	{
		cJSON_Delete(config_map);
		config_map=NULL;
	}
	return config_map;
}


cJSON * Set_Pod_limits(cJSON * compute_resources_timebound,const char * podsize)
{
	cJSON * limits=cJSON_CreateObject();
	
	if (podsize)
	{
		char * s=strdup(podsize);
		if (s)
		{
			lower_string(s);
			
			if (strcmp(s, "nano")==0)
			{ //nano 128M
				cJSON_AddStringToObject(limits, "cpu", "250m");
				cJSON_AddStringToObject(limits, "memory", "128Mi");
			}
			if (strcmp(s, "micro")==0)
			{ //micro 256M
				cJSON_AddStringToObject(limits, "cpu", "500m");
				cJSON_AddStringToObject(limits, "memory", "256Mi");
			}
			if (strcmp(s, "small")==0)
			{ //small 512M
				cJSON_AddStringToObject(limits, "cpu", "1");
				cJSON_AddStringToObject(limits, "memory", "512Mi");
			}
			if (strcmp(s, "medium")==0)
			{ //medium 768M
				cJSON_AddStringToObject(limits, "cpu", "1500m");
				cJSON_AddStringToObject(limits, "memory", "768Mi");
			}
			if (strcmp(s, "large")==0)
			{ //large 1Gi
				cJSON_AddStringToObject(limits, "cpu", "2");
				cJSON_AddStringToObject(limits, "memory", "1Gi");
			}
			if (strcmp(s, "xlarge")==0)
			{ //xlarge 2Gi
				cJSON_AddStringToObject(limits, "cpu", "4");
				cJSON_AddStringToObject(limits, "memory", "2Gi");
			}
			
			if (strcmp(s, "xxlarge")==0)
			{ //xxlarge 4Gi
				cJSON_AddStringToObject(limits, "cpu", "8");
				cJSON_AddStringToObject(limits, "memory", "4Gi");
			}
			if (strcmp(s, "xxxlarge")==0)
			{ //xxxlarge 8Gi
				cJSON_AddStringToObject(limits, "cpu", "8");
				cJSON_AddStringToObject(limits, "memory", "8Gi");
			}
			// nano 128M
			// micro 256
			// small 512
			// medium 768
			// large 1Gi
			// xlarge 2Gi
			// xxlarge 4Gi
			// xxxlarge 8Gi
		}
	}
	else
	{
		cJSON_AddStringToObject(limits, "cpu", "500m");
		cJSON_AddStringToObject(limits, "memory", "256Mi");
	}

	cJSON* spec=cJSON_GetObjectItem(compute_resources_timebound, "spec");
	if (spec)
	{
		cJSON * hard=cJSON_GetObjectItem(spec, "hard");
		long hard_limits_cpu=cpustring2long( cJSON_get_key(hard, "limits.cpu"));
		long limits_cpu=cpustring2long( cJSON_get_key(limits, "cpu"));
		if (hard_limits_cpu && limits_cpu && limits_cpu>hard_limits_cpu)
		{
			cJSON* error=cJSON_CreateObject();
			cJSON_AddItemToObject(limits, "error", error);
			cJSON_add_string_v(error, "cpu", "Requested cpu %s exceeds hard limit of %s",cJSON_get_key(limits, "cpu"),cJSON_get_key(hard, "limits.cpu"));
		}
		long hard_limits_memory=memorystring2long( cJSON_get_key(hard, "limits.memory"));
		long limits_memory=memorystring2long( cJSON_get_key(limits, "memory"));
		if (hard_limits_memory && limits_memory && limits_memory>hard_limits_memory)
		{
			cJSON* error=cJSON_GetObjectItem(limits, "error");
			if (!error)
			{
				error=cJSON_CreateObject();
				cJSON_AddItemToObject(limits, "error", error);
			}
			cJSON_add_string_v(error, "memory", "Requested Memory %s exceeds hard limit of %s",cJSON_get_key(limits, "memory"),cJSON_get_key(hard, "limits.memory"));
		}
	}
	return limits;
	
}


static cJSON* make_pod_config(const char* name, const char * image, int id, int exec_uid,int build_storage,cJSON ** limits)
{
	
	char configmapname[256];
	debug("Pod config_image: %s\n",image);
	snprintf(configmapname, 256, "cfg-job-json-%d",id);
	
	cJSON* pod_spec=cJSON_CreateObject();
	cJSON * metadata=cJSON_CreateObject();
	cJSON * spec=cJSON_CreateObject();
	cJSON * containers=cJSON_CreateArray();
	cJSON * container=cJSON_CreateObject();
	
	cJSON * volumeMounts=cJSON_CreateArray();
	cJSON * volumeMount=NULL;
	cJSON * volumes=cJSON_CreateArray();
	cJSON * volume=NULL;
	
	cJSON_AddStringToObject(pod_spec, "apiVersion", "v1");
	cJSON_AddStringToObject(pod_spec, "kind",       "Pod");
	/* todo add timeout according to job
	 metadata:
  annotations:
	 openshift.io/active-deadline-seconds-override: "1000"
	 */
	cJSON_AddItemToObject  (pod_spec, "metadata",    metadata);
	{
		char pod_name[256];
		snprintf(pod_name, 256, "%s-%d",name,id);
		cJSON_AddStringToObject(metadata, "name",pod_name);
		
	}
	cJSON_AddItemToObject  (pod_spec, "spec",        spec);
	
	cJSON_AddStringToObject(spec, "dnsPolicy","ClusterFirst");
	//cJSON_AddNullToObject(spec, "imagePullSecrets");
	// put pod in terminating mode
	cJSON_safe_addNumber2Obj(spec, "activeDeadlineSeconds", 3600);
	
	cJSON_AddStringToObject(spec, "restartPolicy","Never");
	//cJSON_AddStringToObject(spec, "serviceAccountName", "sa-odagrun");
	
	cJSON_AddItemToObject(spec, "containers", containers);
	
	cJSON_AddItemToArray(containers, container);
	cJSON_AddStringToObject(container, "name", name);
    cJSON_AddStringToObject(container, "image", image);
	cJSON_AddStringToObject(container, "imagePullPolicy", "IfNotPresent");
	cJSON * command=NULL;
	if (getdebug())
		command=  cJSON_CreateStringArray((const char *[]){"/oc-executer","--debug","--verbose",NULL}, 3);
	else
		command=  cJSON_CreateStringArray((const char *[]){"/oc-executer",NULL}, 1);
	
	if (command) cJSON_AddItemToObject  (container, "command",        command);
	
	
	
	
	// for terminating pods
	cJSON * resources=cJSON_CreateObject();
	cJSON * requests=cJSON_CreateObject();
	cJSON_AddStringToObject(requests, "cpu", "20m");
	cJSON_AddStringToObject(requests, "memory", "128Mi");
	
	

	if (limits && *limits)
	{
		cJSON_AddItemToObjectCS(resources, "limits", *limits);
		*limits=NULL;
	}
	cJSON_AddItemToObjectCS(resources, "requests", requests);
	cJSON_AddItemToObject(container, "resources", resources);
	
	/*
	 env:
	 - name: SPECIAL_LEVEL_KEY
	 valueFrom:
	 configMapKeyRef:
	 name: job-data
	 key: special.how
	 - name: SPECIAL_TYPE_KEY
	 valueFrom:
	 configMapKeyRef:
	 name: special-config
	 key: special.type
	 optional: true
	 envFrom:
	 - configMapRef:
	 name: env-config
	 */
	/*
	 cJSON * env=  cJSON_CreateArray();
	 cJSON_AddItemToObject  (container, "env",        env);
	 
	 cJSON * env_item=cJSON_CreateObject();
	 cJSON_AddItemToArray(env, env_item);
	 
	 cJSON_AddItemToObject  (env_item, "name",    cJSON_CreateString("JOB-DATA"));
	 cJSON * valueFrom=cJSON_CreateObject();
	 cJSON_AddItemToObject(env_item, "valueFrom", valueFrom);
	 cJSON * configMapKeyRef=cJSON_CreateObject();
	 cJSON_AddItemToObject(valueFrom, "configMapKeyRef", configMapKeyRef);
	 cJSON_AddStringToObject(configMapKeyRef, "name", "cmkr1");
	 cJSON_AddStringToObject(configMapKeyRef, "key", "job.json");
	 */
	cJSON * envFrom=  cJSON_CreateArray();
	cJSON_AddItemToObject  (container, "envFrom",   envFrom);
	cJSON * envFromItem=cJSON_CreateObject();
	cJSON * configMapRef=cJSON_CreateObject();
	cJSON_AddItemToArray(envFrom,   envFromItem);
	cJSON_AddItemToObject(envFromItem, "configMapRef", configMapRef);
	cJSON_AddStringToObject( configMapRef, "name", configmapname);
	/*
	 livenessProbe:
	 exec:
	 command:
	 - /oc-dispatcher
	 - '--version'
	 failureThreshold: 3
	 periodSeconds: 10
	 successThreshold: 1
	 timeoutSeconds: 1
	 
	 readinessProbe:
	 exec:
	 command:
	 - /oc-dispatcher
	 - '--version'
	 failureThreshold: 3
	 periodSeconds: 10
	 successThreshold: 1
	 timeoutSeconds: 1
	 */
	cJSON * probe=cJSON_CreateObject();
	command=  cJSON_CreateStringArray((const char *[]){"/oc-executer","--version",NULL}, 2);
	cJSON * exec=cJSON_CreateObject();
	cJSON_AddItemToObject(exec, "command", command);
	cJSON_AddItemToObject(probe, "exec", exec);
	cJSON_AddNumberToObject(probe, "failureThreshold", 3);
	cJSON_AddNumberToObject(probe, "periodSeconds"   , 10);
	cJSON_AddNumberToObject(probe, "successThreshold", 1);
	cJSON_AddNumberToObject(probe, "timeoutSeconds"  , 1);
	cJSON_AddItemToObject(container, "livenessProbe", cJSON_Duplicate(probe, 1));
	cJSON_AddItemToObject(container, "readinessProbe", probe);
	
	
	
	
	// add run as, if non priv
	if (exec_uid)
	{
		/*
		 user: $exec_uid
		 securityContext:
		 runAsNonRoot: true
		 runAsUser: $exec_uid
		 */
		//TODO set env
		
		//HOME /builds
		//USER nobody
		
		debug("change pod template to non priviledged");
		
		cJSON_AddNumberToObject(container, "user", exec_uid);
		cJSON_AddNumberToObject(container, "group", exec_uid);
		cJSON * securityContext=cJSON_CreateObject();
		cJSON_AddItemToObject  (container, "securityContext",   securityContext);
		{
			cJSON_AddTrueToObject  (securityContext,"runAsNonRoot");
			cJSON_AddNumberToObject  (securityContext,"runAsUser",exec_uid);
		}
	}
	
	cJSON_AddItemToObject  (container, "volumeMounts",   volumeMounts);
	if (build_storage)
	{
		volumeMount=cJSON_CreateObject();
		cJSON_AddItemToArray(volumeMounts, volumeMount);
		cJSON_AddStringToObject(volumeMount, "mountPath", "/builds");
		cJSON_AddStringToObject(volumeMount, "name", "build-storage");
	}
	/*
	 volumeMount=cJSON_CreateObject();
	 cJSON_AddItemToArray(volumeMounts, volumeMount);
	 cJSON_AddStringToObject(volumeMount, "mountPath", "/bin-runner");
	 cJSON_AddStringToObject(volumeMount, "name", "bin-runner");
	 cJSON_AddTrueToObject(volumeMount,"readOnly");
	 */
	/*
	 volumeMount=cJSON_CreateObject();
	 cJSON_AddItemToArray(volumeMounts, volumeMount);
	 cJSON_AddStringToObject(volumeMount, "mountPath", "/job-data");
	 cJSON_AddStringToObject(volumeMount, "name", "config-volume");
	 cJSON_AddFalseToObject(volumeMount,"readOnly");
	 
	 cJSON_AddItemToObject  (spec, "volumes",   volumes);
	 */
	if (build_storage)
	{
		volume=cJSON_CreateObject();
		cJSON_AddItemToArray(volumes, volume);
		
		cJSON_AddStringToObject(volume, "name", "build-storage");
		cJSON * temp=cJSON_CreateObject();
		cJSON_AddItemToObject(volume, "emptyDir",temp );
	}
	/*
	 - name: volume-79kzc
	 persistentVolumeClaim:
	 claimName: varcache
	 */
	/*
	 volume=cJSON_CreateObject();
	 cJSON_AddItemToArray(volumes, volume);
	 cJSON_AddStringToObject(volume, "name", "bin-runner");
	 cJSON * persistentVolumeClaim=cJSON_CreateObject();
	 cJSON_AddItemToObject(volume, "persistentVolumeClaim",persistentVolumeClaim );
	 cJSON_AddStringToObject(persistentVolumeClaim, "claimName", "cn-bin-runner");
	 */
	/*
	 - name: config-volume
	 configMap:
	 name: special-config
	 items:
	 - key: special.how
	 path: path/to/special-key
	 */
	/*
	 volume=cJSON_CreateObject();
	 cJSON_AddItemToArray(volumes, volume);
	 cJSON_AddStringToObject(volume, "name", "config-volume");
	 cJSON * configMap=cJSON_CreateObject();
	 cJSON_AddItemToObject(volume, "configMap",configMap );
	 
	 cJSON_AddStringToObject(configMap, "name", configmapname); // should contain job id
	 cJSON_AddNumberToObject(configMap, "defaultMode",0770);
	 */
	debug("\nPOD SPEC\n*******\n");
	print_json(pod_spec);
	return pod_spec;
}

