//
//  dyn_trace.c
//  odagrun
//
//  Created by Danny Goossen on 16/10/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "dyn_trace.h"
#include "deployd.h"

/**
 \brief array referenced with \b term_color
 */
const char * color_data[]={ANSI_BOLD_BLACK,ANSI_BOLD_RED,ANSI_BOLD_GREEN,ANSI_BOLD_YELLOW,ANSI_BOLD_BLUE,ANSI_BOLD_MAGENTA,ANSI_BOLD_CYAN,ANSI_BOLD_WHITE, \
ANSI_BLACK,ANSI_RED,ANSI_GREEN,ANSI_YELLOW,ANSI_BLUE,ANSI_MAGENTA,ANSI_CYAN,ANSI_WHITE, \
ANSI_CROSS_BLACK,ANSI_CROSS_RED,ANSI_CROSS_GREEN,ANSI_CROSS_YELLOW,ANSI_CROSS_BLUE,ANSI_CROSS_MAGENTA,ANSI_CROSS_CYAN,ANSI_CROSS_WHITE, \
ANSI_RESET};

/**
 \brief process cariage return in dyn trace buffer
 
 Gitlab trace buffer converts a '\r' to an '\n' does
 \param buf the buffer
 \param n the pointer to the strlen, is updated
 \param first_cr pointer to the first known occurange of a '\r' is updated
 \param last_nl pointer to last known pos of a newline, is updated
 */

/*
static void do_carriage_return(char * buf,size_t * n,size_t * first_cr,size_t* last_nl)
{
   int notdone=1;
   do
   {
      // go back to last \n
      char * first_car=NULL;
      char * last_newline=NULL;
      if (*last_nl)
         last_newline=buf+(*last_nl);
      else
         last_newline=buf;
      if (*first_cr)
         first_car=buf+(*first_cr);
      else
         first_car=memchr(last_newline,'\r',(*n)-(*last_nl));
      
      if (first_car)
         *first_cr=(size_t)(first_car-buf);
      else
         *first_cr=0;
      
      if (first_car && ((*first_cr)+1)!=(*n) && buf[(*first_cr)+1]!='\n' )
      {
         last_newline=memrchr(buf,'\n',*first_cr);
         if (last_newline)
         {
            *last_nl=(size_t)(last_newline-buf);
            size_t t=(size_t)(first_car+1-buf);
            memcpy(last_newline+1, first_car+1,(*n)-t);
            *n=(*n)-(size_t)((first_car)-(last_newline));
         }
         else
         {
            memcpy(buf, first_car+1,(*n)-(size_t)(first_car+1-buf));
            (*n)=(*n)-(size_t)(first_car+1-buf);
         }
         buf[*n]=0;
         first_car=memchr(buf,'\r',(*n));
         if (first_car)
            (*first_cr)=(size_t)(first_car-buf);
         else
            (*first_cr)=0;
      }
      else if (first_car && ((*first_cr)+1)!=(*n) && buf[(*first_cr)+1]=='\n' )
      {
         last_newline=buf + (*first_cr);
         *last_nl=(size_t)(last_newline-buf);
         size_t t=(size_t)(first_car+1-buf);
         memcpy(first_car, first_car+1,(*n)-t);
         (*n)--;
         buf[*n]=0;
         first_car=memchr(buf,'\r',(*n));
         if (first_car)
            (*first_cr)=(size_t)(first_car-buf);
         else
            (*first_cr)=0;
      }
      else notdone=0;
   } while ( notdone);
}
*/

/**
 \brief internal function to check if buffer needs to be increased
 */
int can_update_trace_buffer(void*userp,size_t n);

/*------------------------------------------------------------------------
* Helper function append string to dynamic trace
*------------------------------------------------------------------------*/
int init_dynamic_trace(void**userp, const char * token, const char * url, int job_id, const char * ca_bundle)
{
   struct trace_Struct **trace_p = (struct trace_Struct **)userp;
   if (!trace_p || ! token || !url) return -1;
   if (*trace_p) free_dynamic_trace(userp);
   *trace_p=NULL;
   
   (*trace_p)=calloc(1, sizeof(struct trace_Struct));
   if (*trace_p)
   {
      (*trace_p)->url=strdup(url);
      (*trace_p)->first_cr=0;
      (*trace_p)->last_nl=0;
      (*trace_p)->job_id=job_id;
      (*trace_p)->params = cJSON_CreateObject();
	   (*trace_p)->ca_bundle=ca_bundle;
      cJSON_AddStringToObject((*trace_p)->params, "state", "running");
      cJSON_AddStringToObject((*trace_p)->params, "token", token);
      cJSON * tmp=cJSON_CreateObject();
	   (*trace_p)->min_trace_buf_increment=0x4000; //increment with minimun 16K
	   (*trace_p)->trace_len=0;
	   (*trace_p)->trace_buf_size=0;
      tmp->valuestring=calloc(1,(*trace_p)->min_trace_buf_increment);
      if (tmp)
      {
         tmp->type=cJSON_String;
         cJSON_AddItemToObject((*trace_p)->params, "trace", tmp);
         (*trace_p)->trace_len=0;
		 (*trace_p)->trace_buf_size=(*trace_p)->min_trace_buf_increment;
         (*trace_p)->curl=curl_easy_init();;
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_CAINFO, (*trace_p)->ca_bundle);
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_SSL_VERIFYPEER, 1L);
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_FOLLOWLOCATION, 1L);
		  curl_easy_setopt((*trace_p)->curl, CURLOPT_USERAGENT, PACKAGE_STRING);
         (*trace_p)->tests=NULL;
         (*trace_p)->mark=0;
      }
      else
      {
         cJSON_Delete( (*trace_p)->params);
         if ((*trace_p)->url) oc_free(&(*trace_p)->url);
         (*trace_p)->url=NULL;
         free(*trace_p);
         (*trace_p)=NULL;
      }
   }
   if (*trace_p) debug("trace->params ptr %p\n",(*trace_p)->params);
   return 0;
}

size_t Write_dynamic_trace_n(const void *contents,size_t n, void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   if (contents && n>0)
   {
	   if (can_update_trace_buffer(trace, n)==-1)
	   {
			  error("Write_dyn_trace_n, no more mem");
			  return 0;
	   }
	   char* trace_str=cJSON_get_stringvalue_as_var(trace->params, "trace");
	   if (trace_str)
	   {
		   memcpy(trace_str+trace->trace_len, contents, n);
		   trace->trace_len+=n;
		   trace_str[trace->trace_len]=0;
//		   do_carriage_return(cJSON_get_stringvalue_as_var(trace->params, "trace"), &trace->trace_len, &trace->first_cr, &trace->last_nl);
		}
		else
		{
			error("Write_dyn_trace_n, no more mem");
			return 0;
		}
	}
   else
	   error("Write_dyn_trace_n, cjson trace problem\n");
   return n;
}

size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...)
{
	if (!userp || !message) return 0;
	struct trace_Struct *trace = (struct trace_Struct *)userp;
   
	char * contents=NULL;
	int realsize=0;
	size_t n=0;
	contents=CSPRINTF_L(message, &realsize);
	
	// with content and length, check if trace buffer needs to be increased
	if (realsize!=-1)
	{
		if (realsize>0)
		{
			int append_nl=0;
			n=(size_t)realsize;
			if (contents[realsize-1]=='\n')
			{
				contents[realsize-1]=0;
				append_nl=1;
			}
			// content ready, let's calculate the length
			if (color== reset)
				n+= strlen(ANSI_RESET);
			else if (color!=none)
				n+=strlen(color_data[color]) + strlen(ANSI_RESET);
			
			if (can_update_trace_buffer(trace, n)==-1)
			{
			  oc_free(&contents);
			  error("Write_dyn_trace_n, no more mem");
			  return 0;
			}
			char cr[2]={0,0};
			if (append_nl) cr[0]='\n';
			char* trace_str=cJSON_get_stringvalue_as_var(trace->params, "trace");
			if (trace_str)
			{
				size_t buff_left=trace->trace_buf_size-trace->trace_len;
				int snpr=0;
				 // append contents
				if (color== none)
					snpr=snprintf(trace_str+trace->trace_len,buff_left,"%s%s",contents,cr);
				else if (color==reset)
					snpr=snprintf(trace_str+trace->trace_len,buff_left,"%s%s%s",ANSI_RESET, contents,cr);
				else
					snpr=snprintf(trace_str+trace->trace_len,buff_left,"%s%s%s%s",color_data[color],contents,ANSI_RESET,cr);
				if (snpr>=(int)buff_left)
				{
					  error("Write_dyn_trace_pad, buffer to small\n");
					  realsize=0;
				}
				else
					trace->trace_len+=n;
		
				trace_str[trace->trace_len]=0;
			}
			else
			{
				error("Write_dyn_trace_n, cjson trace problem\n");
				realsize=0;
			}
		}
	}
	oc_free(&contents);
	return n;
}


size_t Write_dyn_trace_pad(void *userp,enum term_color color, int len2pad,const char *message, ...)
{
   if (!userp || !message) return 0;
	
   struct trace_Struct *trace = (struct trace_Struct *)userp;
	
   char * contents=NULL;
   int realsize=0;
	
   contents=CSPRINTF_L(message, &realsize);
	
   // with content and length, check if trace buffer needs to be increased
   if (realsize!=-1)
   {
      char pad[]="                                                                                                                                                                       ";
	   int pad_count=len2pad-realsize;
	   if (pad_count<0) pad_count=0;
      if (realsize<=len2pad)
		  pad[pad_count]=0;
      else
      { // trim contents
		 realsize=len2pad;
         contents[len2pad]=0;
         if (len2pad>2)
         {
            contents[len2pad-1]='.';
            contents[len2pad-2]='.';
         }
		  
		  pad[0]=0; // no padding
		  pad_count=0;
      }
      size_t n;
      // content ready, let's calculate the length
      if (color== none)
         n= realsize + pad_count;
      else if (color== reset)
         n= realsize+pad_count+strlen(ANSI_RESET);
      else n= realsize + strlen(color_data[color]) + strlen(ANSI_RESET) + pad_count;
	   
	  if (can_update_trace_buffer(trace, n)==-1)
	  {
		  oc_free(&contents);
		  error("Write_dyn_trace_n, no more mem");
		  return 0;
	  }
	   char* trace_str=cJSON_get_stringvalue_as_var(trace->params, "trace");
	   if (trace_str)
	   {
         // append contents with padding to the buffer
		  size_t buff_left=trace->trace_buf_size-trace->trace_len;
		  int snpr=0;
         if (color== none)
            snpr=snprintf(trace_str+trace->trace_len,buff_left,"%s%s",contents,pad);
         else if (color==reset)
            snpr=snprintf(trace_str+trace->trace_len,buff_left,"%s%s%s",ANSI_RESET,contents,pad);
         else
            snpr=snprintf(trace_str+trace->trace_len,buff_left,"%s%s%s%s",color_data[color],contents,ANSI_RESET,pad);
		  if (snpr>=(int)buff_left)
		  {
			  error("Write_dyn_trace_pad, buffer to small\n");
			  realsize=0;
		  }
		  else
	         trace->trace_len+=n;
         trace_str[trace->trace_len]=0;
      	}
      	else
		{
			error("Write_dyn_trace_n, cjson trace problem\n");
			realsize=0;
		}
   }
   oc_free(&contents);
   return (size_t)realsize;
}

void set_dyn_trace_state(void *userp,char * state)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   if (state)
   {
      if (cJSON_get_key(trace->params, "state"))
         cJSON_ReplaceItemInObject(trace->params, "state", cJSON_CreateString(state));
      else
         cJSON_AddItemToObject(trace->params, "state", cJSON_CreateString(state));
   }
}


/*------------------------------------------------------------------------
 * Helper function get trace string
 *------------------------------------------------------------------------*/
const char * get_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   return cJSON_get_key(trace->params, "trace");
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
size_t clear_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   cJSON * tmp=cJSON_GetObjectItem( trace->params, "trace");
   if (tmp)
   {
      tmp->valuestring[0]=0;
      trace->trace_len=0;
      trace->mark=-1;
      trace->last_nl=0;
      trace->first_cr=0;
	  trace->count=0;
   }
   else
      error("could not get trace\n");
   return 0;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void clear_till_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   if (trace->mark!=-1 && trace->trace_len>0 && trace->trace_len>(size_t)trace->mark)
   {
      cJSON * tmp=cJSON_GetObjectItem( trace->params, "trace");
      if (tmp)
      {
         tmp->valuestring[trace->mark]=0;
         trace->trace_len=(size_t)trace->mark;
         trace->last_nl=0;
         trace->first_cr=0;
      }
      else
         error("could not get trace\n");
   }
   return;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void set_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->mark=(ssize_t)trace->trace_len;
   trace->last_diff=0;
   return;
}

#define DEC_CNT 200

void dyn_trace_add_decrement(void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->count++;
   if (trace->count>DEC_CNT-1) trace->count=0;
   char dec[DEC_CNT+1];
   int i;
   for (i=0;i<(DEC_CNT-1-trace->count);i++) dec[i]=' ';
	dec[i]=0;
	Write_dyn_trace(trace, none, "%s\n",dec);
	
}

void dyn_trace_add_decrement_nl(void *userp)
{
	struct trace_Struct *trace = (struct trace_Struct *)userp;
	trace->count++;
	if (trace->count>DEC_CNT-1) trace->count=0;
	char dec[DEC_CNT+1];
	int i;
	for (i=0;i<(DEC_CNT-1-trace->count);i++) dec[i]=' ';
	dec[i]=0;
	Write_dyn_trace(trace, none, "%s",dec);
}


/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void clear_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->mark=-1;
   return;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void free_dynamic_trace(void **userp)
{
   if (*userp)
   {
      struct trace_Struct **trace_p = (struct trace_Struct **)userp;
      if ((*trace_p)->params)cJSON_Delete( (*trace_p)->params);
      (*trace_p)->params=NULL;
      if ((*trace_p)->url) oc_free(&(*trace_p)->url);
      (*trace_p)->url=NULL;
      if ((*trace_p)->curl) curl_easy_cleanup((*trace_p)->curl);
      (*trace_p)->curl=NULL;
      (*trace_p)->mark=-1;
      free (*trace_p);
      (*trace_p)=NULL;
   }
   return;
}

void print_api_error(struct trace_Struct* trace,int error)
{
	if (error!=200)
	{
		{
			if (error<90 && error>0)
			{
				Write_dyn_trace(trace, magenta, "\n\tERROR:(%d)  %s\n",error,curl_easy_strerror((CURLcode)error));
			}
			else
				switch (error) {
					case 404:
						Write_dyn_trace(trace, magenta, "\tNot Found (HTTP %d)\n",error);
						break;
					case 500:
						Write_dyn_trace(trace, magenta, "\tServer error (HTTP %d)\n",error);
					case 503:
						Write_dyn_trace(trace, magenta, "\tEndpoint not availleble (HTTP %d)\n",error);
						break;
					case 403:
						Write_dyn_trace(trace, magenta, "Forbidden (HTTP %d)\n",error);
						break;
					case 401:
						Write_dyn_trace(trace, magenta, "\tNot Authorised (HTTP %d)\n",error);
						break;
					default:
						Write_dyn_trace(trace, magenta, "\tHttp-Error : HTTP %d\n",error);
						break;
				}
		}
	}
}

int can_update_trace_buffer(void*userp,size_t n)
{
	int result=-1;
	struct trace_Struct *trace = (struct trace_Struct *)userp;
	cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
	if (item && item->valuestring)
	{
		// increase buffer if needed
		if (trace->trace_len+1+n> trace->trace_buf_size)
		{
			size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n+1);
			char * temp=realloc(item->valuestring,newsize);
			if (temp )
			{
				trace->trace_buf_size=newsize;
				if (temp!=item->valuestring)
				{
					item->valuestring=temp;
					
				}
				result=1;
			}
			
		}
		else // can fit, no need update
			result=1;
	}
	
	return result;
}

