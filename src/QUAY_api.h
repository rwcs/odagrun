/*
 Copyright (c) 2018 by Danny Goossen, Gioxa Ltd.
 
 This file is part of the odagrun
 
 MIT License
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */
/*! \file QUAY_api.h
 *  \brief api functions QUAY
 *  \author Created by Danny Goossen on 19/7/18.
 *  \copyright Copyright (c) 2018 Danny Goossen. All rights reserved.
 *
 */


#ifndef __odagrun__QUAY_api__
#define __odagrun__QUAY_api__

#include <stdio.h>
#include "common.h"

/**
 \brief api_data the docker api data structure
 \todo add mutex for registry updates in the database for parallel actions(pulling multiple layers in parallel)
 */
struct QUAY_api_data_s {
	CURL * curl;              /**< CURL connection */
	char * credential;       /**< the database of the registries */
	char * default_ca_bundle; /**< the default ca_cert bundle */
	char * token;
	int retries;
	char * errormsg;
	//  add mutext for registries
	// individual request data
};




/**
 * \brief init the docker api
 * \param api_data the data struture is created and initialized and returned
 * \param ca_cert_bundle the default ca_bundle to use
 * \return 0 on success
 * \note apidata should be NULL
 */
int QUAY_api_init(struct QUAY_api_data_s ** api_data, const char *ca_cert_bundle ,const char * credentials);

/**
 * \brief cleanup the api data
 * \param api_data is *api_data will be NULL when returned
 */
void QUAY_api_cleanup(struct QUAY_api_data_s ** api_data);

/**
 \brief api to delete repository on QUAY_api
 */
int QUAY_api_QUAY_delete_repository(struct QUAY_api_data_s * api_data, const char *image, cJSON **result);
/**
 \brief api to delete tag on QUAY_api
 */
int QUAY_api_QUAY_delete_tag(struct QUAY_api_data_s * api_data, const char *image,const char * tag, cJSON **result);

/**
 \brief api to set description on QUAY_api for a repository
 */
int QUAY_api_QUAY_set_description(struct QUAY_api_data_s * api_data, const char *image,const char * description, cJSON**result);

/**
 \brief api to set visibility on QUAY_api for a repository
 */
int QUAY_api_QUAY_set_visibility(struct QUAY_api_data_s * api_data, const char *image,const char * visibility,cJSON**result);

/**
 \brief api createn on QUAY_api for a repository
 */
int QUAY_api_QUAY_create_repository(struct QUAY_api_data_s * api_data, const char *image, const char * description,const char *visibility,cJSON ** result);

/**
 \brief api QUAY read repository, there is no HEAD
 */
int QUAY_api_QUAY_read_repository(struct QUAY_api_data_s * api_data, const char *image, cJSON ** result);



#endif /* defined(__odagrun__QUAY_api__) */
