//
//  c-ares.h
//  odagrun
//
//  Created by Danny Goossen on 27/1/18.
//  Copyright (c) 2018 Danny Goossen. All rights reserved.
//

#ifndef __odagrun__c_ares__
#define __odagrun__c_ares__

#include <stdio.h>

char * getip(const char * hostname);
char * gethost(const char * hostip);

#endif /* defined(__odagrun__c_ares__) */
